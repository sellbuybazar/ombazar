$(function() {
    altair_tour.init();

    $('#restartTour').click(function() {
        altair_tour.init();
    })

});

altair_tour = {
    init: function() {

        // This tour guide is based on EnjoyHint plugin
        // for more info/documentation please check https://github.com/xbsoftware/enjoyhint

        // initialize instance
        var enjoyhint_instance = new EnjoyHint({

        });

        // config
        var enjoyhint_script_steps = [
            {
                "next #help_button": 'Need help? Follow the link',
                "skipButton" : {text: "Got it"},
                "nextButton" : {text: "Cancel"}
            }

        ];

        // set script config
        enjoyhint_instance.set(enjoyhint_script_steps);

        // run Enjoyhint script
        enjoyhint_instance.run();

        $('.enjoyhint_skip_btn').click(function() {
            // run Enjoyhint script
            $.ajax({
                type: 'post',
                url: path+'ajax/tour_close.php',
                success: function(data,stat) {
                    return true;
                }
            });
        })


    }
};
