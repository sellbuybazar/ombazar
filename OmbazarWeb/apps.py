from django.apps import AppConfig


class OmbazarwebConfig(AppConfig):
    name = 'OmbazarWeb'
