from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from .retrieve import Retrieve
from .modify import Modify
from .passed import Passed
from .common import Common
from django.utils.encoding import smart_str
Retrieve = Retrieve()
Common = Common()
Passed = Passed()
Modify = Modify()

# Create your views here.


def index(request):
    # full_url = "http://download.thinkbroadband.com/10MB.zip"
    # file_name = "10MB.zip"
    # response = HttpResponse(content_type='application/force-download')
    # response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
    # response['X-Sendfile'] = smart_str(full_url)
    # return response
    if request.session:
        return render(request, "OmbazarBackEnd/pages/home.html")


def add_admin(request):
    return render(request, "OmbazarBackEnd/pages/add-admin.html")


def install(request):
    return render(request, "OmbazarBackEnd/pages/install.html")


def request(request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        if request.method == "POST":
            try:
                result = ''
                class_name = ''
                method_name = request.POST['request_name']
                request_type = request.POST['request_type']

                if request_type == 'get':
                    class_name = "Retrieve"
                elif request_type == 'update':
                    class_name = "Modify"
                elif request_type == "post":
                    class_name = "Passed"
                elif request_type == "delete":
                    class_name = "Delete"
                elif request_type == "asset":
                    class_name = "Common"

                method = class_name+'.'+method_name
                result = eval(method)
                if not result:
                    not_found_object = {"not_found": "Data not found", "length": 0}
                    result = not_found_object
                # elif 'download' in result:
                #     # file_info = result['download']
                #     # file_url = file_info['view_path']
                #     # file_name = "10MB.zip"
                #     # full_url = "http://download.thinkbroadband.com/10MB.zip"
                #     # response = HttpResponse(content_type='application/force-download')
                #     # response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
                #     # response['X-Sendfile'] = smart_str(full_url)
                #     # Common.preview(response)
                #     response = HttpResponse(content_type='application/http://download.thinkbroadband.com/10MB.zip')
                #     response['Content-Disposition'] = 'attachment; filename="foo.xls"'
                #     return response

                elif 'status' in result:
                    if not result['status']:
                        result['form_data'] = request.POST

            except Exception as e:
                error_message = "Method not found"
                return_object['status'] = 0
                errors['error'] = error_message
                errors['error_info'] = str(Common.error_info())
                errors['error_details'] = str(e)
                return_object['post_data'] = request.POST
                return_object['method'] = method
                return_object['result'] = str(result)
            else:
                return JsonResponse(result)
        else:
            error_message = "Post/Form data empty"
            errors['error'] = error_message
            return_object['status'] = 0
        return JsonResponse(return_object)


