from django.urls import path, re_path
from . import views
from .helper import *

urlpatterns = [
    path('', views.index, name="web_index"),
    path('home/', views.index, name="web_index"),
    path('request/', views.request, name="web_request"),
    re_path(r'^(?!base)(?!media)(?!control).*?$', views.index, name="all_page"),
]

