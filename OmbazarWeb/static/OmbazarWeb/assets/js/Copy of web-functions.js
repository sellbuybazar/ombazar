let OmbazarFn = {};
OmbazarFn.countries = [];
OmbazarFn.categories = [];
OmbazarFn.access_menus = [];
OmbazarFn.selected_country = {};
OmbazarFn.logger_id = Cookies.getJSON('usr_id');
OmbazarFn.logger_user_name = Cookies.getJSON('usr_name');
OmbazarFn.logged_user = {};
OmbazarFn.id_types = {};

OmbazarFn.bar_codes = [];
OmbazarFn.currency_symbol = "$";
OmbazarFn.custom_filter = function(obj,key_name,value,list=false,negetive=false){
    let find_item=obj.filter(function(item){
            if (negetive) {
                return item[key_name]!==value;
            }
            else{
                return item[key_name]===value;
            }

        });
    if(list){
        return find_item;
    }
    else{
        for(let x in find_item){
            return find_item[x];
        }
    }
};


OmbazarFn.not_found_message = function(message="Not found"){
    var html="<div class='md-card'>";
        html+="<div class='md-card-content'>";
        html+="<h2 class='not-found-message'>"+message+"</h2>";
        html+="</div>";
        html+="</div>";
    return html;

};
OmbazarFn.common_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Common request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"asset"
        };
    }
    return extra_data;

};
OmbazarFn.retrive_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Retrieve request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"get"
        };
    }
    return extra_data;

};

OmbazarFn.passed_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Passed request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"post"
        };
    }
    return extra_data;

};
OmbazarFn.modify_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Modify request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"update"
        };
    }
    return extra_data;

};


//processing image on
OmbazarFn.loading_on = function(){
    altair_helpers.content_preloader_show('regular');
};
OmbazarFn.loading_off = function (){
    altair_helpers.content_preloader_hide();

};

OmbazarFn.clean = function(form_id) {
    document.getElementById(form_id).reset();
};
OmbazarFn.modal_close = function () {
    $(".uk-modal-close").click();
};

OmbazarFn.redirect = function (page_name) {
    window.location=path+page_name;
};
OmbazarFn.notify = function (message='Changed has been saved',pos="top-center",timeout="5000") {
    UIkit.notify({
        message: message,
        status:  '',
        timeout: timeout,
        group: null,
        pos: pos,
        onClose: function() {

        }
    });

};

OmbazarFn.have = function (element){
    if($(element).length>0){
        return true;
    }
    else{
        return false;
    }
};
OmbazarFn.response = function (data) {
    UIkit.modal.info(data);
};
OmbazarFn.get_form_data = function (form_name) {
    var form_data=new FormData($(form_name)[0]);
    return form_data;

};


// using jQuery
OmbazarFn.getCookie = function (name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
OmbazarFn.csrftoken = OmbazarFn.getCookie('csrftoken');


OmbazarFn.request = function (form_name,event,loading=true,extra_data=[],extra_images=[],fancy_tree=[]){
    var form_data=OmbazarFn.get_form_data(form_name);
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){    $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);

        }
        form_data=OmbazarFn.get_form_data(form_name);
    }
    for(item in extra_data){
        var field_name=extra_data[item].name;
        var value=extra_data[item].value;
        form_data.append(field_name,value);

    }
    let token = form_data.get("csrfmiddlewaretoken");
    if (!token){
        if(form_name === ""){
            form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
        }
        else {
            // form_data.append("csrfmiddlewaretoken",document.getElementsByName('csrfmiddlewaretoken')[0].value);
        }

    }

    if(loading){
       OmbazarFn.loading_on();
       }

    $.ajax({
        url:"/request/",
        data:form_data,
        type:"POST",
        processData: false,
        contentType: false,
        // dataType:"json",
        success:function(data,status){
            if (loading) {
                OmbazarFn.loading_off();
            }
            event(data,status);
        },
        complete:function (xhr,status) {
            if(status != "success"){
                let error=xhr.responseText;
                console.log(error);
            }
        }

    });
};

OmbazarFn.form_data_maker = function (form_element_name,extra_data={},extra_images=[],fancy_tree=[]) {
    // form name format like ("element")/ like jquery selector
    var form_data=OmbazarFn.get_form_data(form_element_name);

    // Image an array []
    //format is array = [file_input_id_name]
    //example <input type='file' id='image'> array key is (image)
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }

    // tree make follow the fancytree rules
    //array format [{},{}]
    // tree = [
    //      {
    //         element_name:".categories.fancytree_radio" ,
    //         selected_name:"category" ,
    //         active_name:"parent"
    //      }
    //  ];
    //
    //
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){
            $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);
        }
        form_data=OmbazarFn.get_form_data(form_element_name);
    }
    // extra data followed by object = {name:"xxxx",age:"xxx",other:"xxxx"}
    for(var item in extra_data){
        var field_name= item;
        var value=extra_data[item];
        form_data.append(field_name,value);
    }
    //csrf token for python security
    // form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
    // for(x of form_data.values()){
    //     console.log(x);
    // }
    return form_data;

};

//// angular js support
OmbazarFn.active_button = function (){
    return $("button[active='true']").attr('data-button-name');
};

OmbazarFn.add_action = function(response,$location,url){
     status = response.status;
    if (status){
        ///First notify a message top bar
        let message = response.message;
            message = OmbazarFn.notify_message(message);
        let active_button = OmbazarFn.active_button();
        // If button name save_and_new form data reset/clean
        if(active_button === 'save_and_new'){
            OmbazarFn.clean("form");
            OmbazarFn.update_inputs();
        }
        // If button name save, page redirect to profile page
        else{
            $location.path(url);
        }
    }
};

OmbazarFn.informer = function(data){
    if(data.status !== undefined) {
        // if (!data.status) {
            let messages = data.message;
            let errors = data.error;
            let m_length = Object.keys(messages).length;
            let e_length = Object.keys(errors).length;
            let info_html = "";
            // If message available
            if(!data.status){
                if (m_length) {
                    let message_html = "<h3>Message</h3>";
                    for (let message in messages) {
                        message_html += "<p>" + messages[message] + "</p>";
                    }
                    info_html += message_html;
                }
            }
            // If error available
            if (e_length) {
                let error_html = "<h3>Error</h3>";
                for (let error in errors) {
                    error_html += "<p>" + errors[error] + "</p>";
                }
                info_html += error_html;
                console.log(data)
            }
            if(info_html){
                OmbazarFn.response(info_html);
            }


        // }

    }
};


OmbazarFn.$http = function($http,data,event,loading = true){
    if(loading){
        OmbazarFn.loading_on();
    }
    // When any form submit first disabled all button in form element
    $("form button").addClass("uk-disabled");
    $http({
        method : "POST",
        url : "/request/",
        data : data,
        headers: { 'Content-Type': undefined}
    }).then(
        function (response) {
            let data = response.data;
            let status = response.status;
            if (loading){
                OmbazarFn.loading_off();
            }

            OmbazarFn.informer(data);
            event(data,status);
            $("form button").removeClass("uk-disabled");
        },
        function(response) {
            if (loading){
                OmbazarFn.loading_off();
            }
              console.log(response);
            $("form button").removeClass("uk-disabled");
        })
};

OmbazarFn.notify_message = function(messages){
    let m_length = Object.keys(messages).length;
    let info_html = "";
    // If message available
    if(m_length){
        let message_html = '';
        for (let message in messages) {
            message_html += "<p>"+messages[message]+"</p>";
        }
        info_html += message_html;
        OmbazarFn.notify(info_html,"top-center",5000)
    }
};
OmbazarFn.update_inputs = function(){
    $("input,.md-input").each(function () {
        altair_md.update_input($(this));
    });

};
OmbazarFn.init_requirement = function(){
    altair_md.inputs();
    altair_forms.switches();
    altair_md.checkbox_radio();
    altair_md.fab_toolbar();
    OmbazarFn.update_inputs();
    OmbazarFn.dropify();
};

OmbazarFn.dropify = function(options={}){
  $('.dropify').dropify(options);
};



OmbazarFn.drug_drop_upload = function(params,event) {
    var progressbar = $("#file_upload-progressbar"),
        bar         = progressbar.find('.uk-progress-bar'),
        settings    = {
            action: '/request/', // Target url for the upload
            allow : '*.(json)', // File filter  format like jpg|jpeg|gif|png
            params : params,
            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");

            },
            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },
            allcomplete: function(response,xhr) {

                bar.css("width", "100%").text("100%");
                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);
                setTimeout(function() {
                    UIkit.notify({
                        message: "Upload Completed",
                        pos: 'top-right'
                    });
                },280);

                var data = JSON.parse(response);
                OmbazarFn.informer(data);
                event(data);
            }
        };

    var select = UIkit.uploadSelect($("#file_upload-select"), settings),
        drop   = UIkit.uploadDrop($("#file_upload-drop"), settings);
};

OmbazarFn.find_index = function (docs,key,value) {
  let index_number=docs.findIndex(x => x[key] === value);
  return index_number;
};

OmbazarFn.menu_maker = function (parent_id,main_menus){
    let menu_list = [];
    let menus = OmbazarFn.custom_filter(main_menus,"parent_id",parent_id,true);
    for (let x in menus){
        let item = menus[x];
        let menu_id = item.id;
        item['menus'] = OmbazarFn.menu_maker(menu_id,main_menus);
        menu_list.push(item);
    }
    return menu_list;
};

OmbazarFn.category_maker = function (parent_id,main_categories){
    let category_list = [];
    let categories = OmbazarFn.custom_filter(main_categories,"parent_id",parent_id,true);
    for (let x in categories){
        let item = categories[x];
        let category_id = item.category_id;
        item['categories'] = OmbazarFn.category_maker(category_id,main_categories);
        category_list.push(item);
    }
    return category_list;
};



OmbazarFn.tags_maker = function(element,options = {}){
    $(element).not("input.selectized,.multi.tags").each(function () {
        let default_options = {
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    });

            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
        };
        $.extend(default_options,options);
        $(this).selectize(default_options);
    });
};
OmbazarFn.custom_selectize = function(element,options = {},element_type=false /* just name or html object*/){
    if(element_type){
        var available=OmbazarFn.have(element,true);
    }
    else{
        var available=OmbazarFn.have(element);
    }
    if(available){
        if(element_type){
            var this_elem=element;
        }
        else{
            var this_elem=$(element);
        }
        this_elem.not("select.selectized,.plugin-remove_button").each(function () {
            let default_options = {
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                valueField: 'value',
                labelField: 'label',
                searchField: ['label','value','mobile'],
                hideSelected: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({'margin-top':'0'})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });

                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({'margin-top':''})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },

            };
            $.extend(default_options,options);
            $(this).selectize(default_options);
        });
    }
    else{
        console.log("Not exist select element!");
    }
};



//for add item category an spcifications manage

OmbazarFn.get_access_category = function ($http,event) {
    if(OmbazarFn.categories.length){
        let categories = OmbazarFn.categories;
        event(categories);
    }
    else{
        OmbazarFn.get_categories($http,function (categories) {
            event(categories);
        })
    }
};

OmbazarFn.get_countries = function ($http,event) {
    if(OmbazarFn.countries.length){
        let categories = OmbazarFn.countries;
        event(categories);
    }
    else{
        let function_name = "countries()";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);

        OmbazarFn.$http($http,form_data,function (response) {
            event(response.find_data);
        })
    }
};


OmbazarFn.get_categories = function ($http,event) {
    let function_name = "categories()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let categories = response.find_data;
       event(categories);
    });
};



OmbazarFn.data_load = function ($http, $scope, $rootScope, $timeout,event) {
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);
        $rootScope.rows = [];
    let skip = ($scope.page-1)*$scope.limit;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "";
            if ($scope.custom_function !== undefined){
                method = $scope.custom_function.function_name;
                let params = $scope.custom_function.parameteres;
                let params_join = params.join();
                if (params.length){
                    params_join += ",";
                }
                method +=  "("+params_join+"'"+$scope.limit+"','"+skip+"')";
            }
            else{
                method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + $scope.limit + "','" + skip + "')";
            }
        let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
        let form_data = OmbazarFn.form_data_maker("", extra_data);
        OmbazarFn.$http($http, form_data, function (response, status) {
            console.log(response);
            let finds = response.find_data;
            $rootScope.rows = finds;
            $timeout(function () {
                if (finds.length) {
                    $scope.filtered = $scope.limit * $scope.page;
                    $scope.start_row = $scope.filtered - $scope.limit;
                    altair_md.checkbox_radio();
                    $rootScope.require();
                    if (!$("#ts_pager_filter[role]").length) {
                        altair_tablesorter.pager_filter_example();
                        OmbazarFn.custom_selectize(".ts_gotoPage",{
                            create: function(input) {
                                return {
                                    value: input,
                                    label: input
                                }
                            },
                        });
                        OmbazarFn.custom_selectize(".pagesize");
                    }
                    $("#ts_pager_filter").trigger("update");

                    let page_instance = $(".ts_gotoPage").get(0).selectize;
                    page_instance.setValue($scope.page);
                    page_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.page = value;
                            // $scope.change();
                        })
                    });
                    let limit_instance = $(".pagesize").get(0).selectize;
                    limit_instance.setValue($scope.limit);
                    limit_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.limit = value;
                            // $scope.change();
                        })
                    });
                }

            });
            event(response, status);
        });
    }
    else if(skip <= 0){
        $scope.prev_switch = 0;
        OmbazarFn.custom_selectize(".ts_gotoPage");
        OmbazarFn.custom_selectize(".pagesize");
    }
    else if(skip >= $scope.total){
        $scope.next_switch = 0;
        OmbazarFn.custom_selectize(".ts_gotoPage");
        OmbazarFn.custom_selectize(".pagesize");
    }

};
OmbazarFn.total_counter = function($http, $scope,event){
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
        query = JSON.stringify(query);
    let method = "";
            if ($scope.custom_function !== undefined){
                method = $scope.custom_function.function_name;
                let params = $scope.custom_function.parameteres;
                let params_join = params.join();
                if (params.length){
                    params_join += ",";
                }
                method +=  "("+params_join+"'0','0',True)";
            }
            else{
                method = "documents_counter('"+collection_name+"',"+query+")";
            }

    let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(response);
        event(response,status);
    });
};

OmbazarFn.request_data_load = function ($http, $scope, $rootScope, $timeout,event) {

    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);
    let skip = $scope.skip;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "";
        if ($scope.custom_function !== undefined) {
            method = $scope.custom_function.function_name;
            let params = $scope.custom_function.parameteres;
            let params_join = params.join();
            if (params.length) {
                params_join += ",";
            }
            method += "(" + params_join + "'" + $scope.limit + "','" + skip + "')";
        }
        else{
            method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + $scope.limit + "','" + skip + "')";
        }
        let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
        let form_data = OmbazarFn.form_data_maker("", extra_data);
        OmbazarFn.$http($http, form_data, function (response, status) {
            let finds = response.find_data;
            if (finds.length) {
                for (let x in finds) {
                    let item = finds[x];
                    $scope.rows.push(item);
                }
                $scope.skip = $scope.skip+finds.length;
            }
            $timeout(function () {
                event(response, status);
            });

        });
    }
};

OmbazarFn.get_basic_info = function ($rootScope,service,event=function () {}) {
    let basic_info = {};
    if(Object.keys($rootScope.basic_info).length){
        basic_info = $rootScope.basic_info;
        event(basic_info);
    }
    else{
        $rootScope.$on("set_basic_info",function () {
            basic_info = service.get_basic_info();
            event(basic_info);
        });

    }

};

OmbazarFn.retrieve_data = function ($http, collection_name, query,event,option={},function_name=false,parameteres = {}) {
    query = JSON.stringify(query);
    option = JSON.stringify(option);
    let method = "";
    if (function_name) {
        method = collection_name;
        let params = parameteres;
        let params_join = params.join();
        method += "(" + params_join + ")";
    }
    else{
        method = "request_collection_data('" + collection_name + "'," + query + "," + option + ")";
    }
    let extra_data = OmbazarFn.retrive_request_object(method);
        extra_data['query'] = query;
    let form_data = OmbazarFn.form_data_maker("", extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        event(response,status);
    })
};


OmbazarFn.add_to_cart = function ($rootScope,$scope, store_item_id, amount = 1,replace = false) {
    let item = Cookies.getJSON(store_item_id);
        amount = Number(amount);
    if (item){
        let new_amount = 0;
        if (replace){
            new_amount = amount;
        }
        else{
            new_amount = amount+item.quantity;
        }

        Cookies.set(store_item_id,{
            "quantity": new_amount
        });
        let index = OmbazarFn.index_number($scope.cart,"store_item_id",store_item_id);
        $scope.cart[index].quantity = new_amount;
        let before_info = $scope.cart[index];
        let point = OmbazarFn.point_maker($rootScope,before_info);
        $scope.cart[index].point = point;
    }
    else{
        let item_info = OmbazarFn.custom_filter($scope.products,"store_item_id",store_item_id);
            // item_info.quantity = amount;
        Cookies.set(store_item_id,{
            "quantity": amount
        });
        let new_object = {
            "price": item_info.price,
            "sale": item_info.sale,
            "quantity": amount,
            "purchase": item_info.purchase,
            "item_id": item_info.item_id,
            "store_id": item_info.store_id,
            "store_item_id": item_info.store_item_id,
            "vat": item_info.vat,
            "vendor_id": item_info.vendor_id,
            "category_id": item_info.category_id,
            "description": item_info.description,
            "images": item_info.images,
            "short_description": item_info.short_description,
            "item_name": item_info.item_name
        };
        let point = OmbazarFn.point_maker($rootScope,new_object);
        new_object.point = point;
        $scope.cart.push(new_object);
    }
    $scope.calculate();

    // console.log(index);

};

OmbazarFn.merge = function (target_object, new_object) {
    return Object.assign(target_object, new_object);
};
OmbazarFn.index_number = function(obj,key_name,value){
    return obj.map(function(e) { return e[key_name]; }).indexOf(value);
};
OmbazarFn.order_total_info = function (items, extra_discount = 0) {

    let sub_total = 0;
    let total = 0;
    let vat = 0;

    let discounts = [];

    let grand_total = 0;
    let total_item = items.length;
    for (let x in items){
        let item = items[x];
        let price = item.price;
        let sale = item.sale;
        let vat = item.vat;
        let quantity = item.quantity;

        //for gather rice grand total
        let line_total = 0;
        if (item.sale){
            line_total = item.sale*item.quantity;
        }
        else{
            line_total = item.price*item.quantity;
        }
        let line_vat = line_total*(vat/100);
        vat += line_vat;
        sub_total += line_total;

        if (item.sale){
            let price_line_total = item.price*item.quantity;
            let price_line_vat = price_line_total*(vat/100);
            let this_discount = (price_line_total+price_line_vat)-(line_total+line_vat);
            let item_in_ob = {
                "item_name": item.item_name,
                "discount": this_discount,
            };
            discounts.push(item_in_ob);
        }
    }
    total = sub_total + vat - (extra_discount);
    let object = {
        grand_total: total,
        extra_discount: extra_discount,
        sub_total: sub_total,
        vat: vat,
    };
    let total_item_discount = 0;
    for (let x in discounts){
        total_item_discount += discounts[x].discount;
    }
    object.total_discount = total_item_discount + extra_discount;
    object.total_item = total_item;
    object.discounts = discounts;

    return object;
};
OmbazarFn.order_status_selectize = function (default_value=0,disabled=0, event=function () {}) {
    let order_status_list = [

        {
            label: "Processing",
            value: "Processing",
        },
        {
            label: "Completed",
            value: "Completed",
        },
        {
            label: "Pending",
            value: "Pending",
        },
        {
            label: "Cancelled",
            value: "Cancelled",
        },

        ];
    if (disabled){
        $(".order-status").addClass("uk-disabled");
    }

    OmbazarFn.custom_selectize(".order-status",{
        options: order_status_list,
        onInitialize:function () {
            if (default_value){
                this.setValue(default_value);
            }
            event(this);
        }
    });
};
OmbazarFn.order_info_filter = function (order) {
    let order_items = order.order_items;
    let items = [];
    for(let x in order_items){
        let order_item_info = order_items[x].item_info;
        let item_id = order_item_info.item_id;
        let item_info = OmbazarFn.custom_filter(order.items,"item_id",item_id);
        let merge = OmbazarFn.merge(item_info,order_item_info);
        items.push(merge)
    }
    order.order_items = items;
    // console.log($scope.order_items);
    order.total_info = OmbazarFn.order_total_info(items);
    return order;
};
OmbazarFn.route_info = function ($rootScope,$location) {
    $rootScope.path = $location.path();
    let path = $rootScope.path;
    let path_text = path.substr(1, path.length);
    let first_char = path_text.substr(0,1).toUpperCase();
    let path_name = first_char + path_text.substr(1, path_text.length);
        path_name = path_name.replace(/-/g," ");
    let info = [
        {
            name: "Home",
            url: "",
            anchor: true
        },
        {
            name: path_name,
            url: path_text,
            anchor: true
        }
    ];
    return info;
};

OmbazarFn.countries_init = function ($http,event=function () {}) {
    OmbazarFn.get_countries($http,function (countries) {
        OmbazarFn.countries = countries;
        OmbazarFn.custom_selectize(".country-code-selector",{
            onInitialize:function () {
                for (let x in countries){
                    let item = countries[x];
                    let country_code = item.callingCodes[0];
                    let label = item.alpha2Code+" ("+country_code+")";
                    let value = country_code;
                    let opt_ob = {label: label, value: value};
                    this.addOption(opt_ob);
                    countries[x].calling_code = country_code;
                }
                event(this);
            },
            onChange: function (item) {
                let country = OmbazarFn.custom_filter(countries,'calling_code',item);
                let country_name_field = $(".own-country-name");
                let nationality = {
                    country_name: country.name,
                    calling_code: country.calling_code,
                    region: country.region,
                    flag: country.flag
                };
                OmbazarFn.selected_country = nationality;
                country_name_field.val(country.name);
                altair_md.update_input(country_name_field);
            }

        });
    });

};
OmbazarFn.get_basic_info = function ($rootScope,service,event=function () {}) {
    let basic_info = {};
    if(Object.keys($rootScope.basic_info).length){
        basic_info = $rootScope.basic_info;
        event(basic_info);
    }
    else{
        $rootScope.$on("set_basic_info",function () {
            basic_info = service.get_basic_info();
            event(basic_info);
        });

    }

};
//this for add category parent category choser
OmbazarFn.access_category_init = function ($http, $scope, $timeout, event=function () {}) {
    OmbazarFn.get_access_category($http,function (categories) {
        OmbazarFn.categories = categories;
        let parent_categories = OmbazarFn.custom_filter(categories,"parent_id",'0',true);
        let category_list = [];
        for (let x in parent_categories){
            let item = parent_categories[x];
            let category_id = item.category_id;
            let new_categories = OmbazarFn.category_maker(category_id,categories);
            // if(new_categories.length){
                item['categories'] = new_categories;
                category_list.push(item);
            // }
        }
        // OmbazarFn.categories = categories;
        $scope.categories = category_list;
        // $timeout(function () {
        //     altair_main_sidebar.init();
        // });
    });
};
OmbazarFn.tree_maker = function(user_id, tree_data){
    let parent_tree_data = OmbazarFn.custom_filter(tree_data,"referer",user_id,true);
    let tree_data_list = [];
    for (let x in parent_tree_data){
        let item = parent_tree_data[x];
        let user_id = item.user_id;
        let new_tree = OmbazarFn.tree_maker(user_id,tree_data);
        // if(new_categories.length){
            item['tree'] = new_tree;
            tree_data_list.push(item);
        // }
    }
    return tree_data_list;
};


OmbazarFn.set_tree_position = function ($http,$rootScope,data_object,event) {
    let function_name = "set_tree_position(request)";
    let extra_data = OmbazarFn.passed_request_object(function_name);
        extra_data = OmbazarFn.merge(extra_data,data_object);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
         console.log(response);
        if (response.status){

        }
    });
};

OmbazarFn.tree = function (element,options={}) {
    let defaults = {
        'add_option': false,
        'edit_option': false,
        'delete_option': false,
        'confirm_before_delete': false,
        'animate_option': false,
        'fullwidth_option': false,
        'align_option': 'center',
        'drag_disabled': false,
    };
    $.extend(defaults,options);
    return $(element).tree_structure(defaults);
};