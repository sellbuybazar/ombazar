    <?php
        $per_page=20;
        $data=array();

    ?>
    <div id="page_content">
        <div id="page_content_inner">
            <h4>Members</h4>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-margin-bottom">
                        <a href="#" class=" " id="printTable" data-uk-tooltip="{pos:'top'}" title="Print">
                            <i class="material-icons md-icon">print</i>
                        </a>
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                            <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                            <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                            </div>
                        </div>
                        <a href="<?php echo SITE_URL?>views/add-member" class="" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}" title="Add member"><i class="material-icons md-icon md-color-green-300">add_circle_outline</i></a>


                    </div>
                    <div class="uk-overflow-container uk-margin-bottom">
                        <table class="uk-table uk-table-align-vertical uk-table-nowrap tablesorter tablesorter-altair" id="ts_pager_filter">
                            <thead>
                            <tr>
                                <th data-name="Select" data-priority="5"><input type="checkbox" class="ts_checkbox_all"></th>
                                <th class="filter-false remove sorter-false" data-priority="6">Image</th>
                                <th data-priority="critical">Name</th>
                                <th data-priority="critical">Active</th>
                                <th data-priority="1">Phone</th>
                                <th data-priority="2">Email</th>
                                <th data-priority="3">Member ID</th>
                                <th data-priority="4">Entry date</th>
                                <th class="filter-false remove sorter-false uk-text-center" data-priority="1">Manage</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Active</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Member ID</th>
                                    <th>Entry date</th>
                                    <th class="uk-text-center">Manage</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php foreach($data as $result){?>
                            <tr id="row_id_<?php echo $result['row_id'];?>" data-id="<?php echo $result['row_id'];?>">
                                <td><input type="checkbox" data-md-icheck class="ts_checkbox"></td>
                                <td><img class="md-user-image" src="<?php echo SITE_URL?>assets/img/images/<?php echo $result['image']?>" alt=""/></td>
                                <td>
                                <?php
                                    echo $result['name'];
                                    $power=$retrive->team_member_add_power($result['user_id']);
                                    if($power){
                                        echo " (admin)";
                                    }

                                ?>
                                </td>
                                <td class="active-colum">
                                <?php $active=$result['active']?>
                                <?php if($active==1):?>
                                <span class="uk-badge uk-badge-success">Active</span>
                                <?php else: ?>
                                <span class="uk-badge uk-badge-danger">Deactive</span>
                                <?php endif ?>
                                </td>
                                <td><?php echo $result['phone']?></td>
                                <td><?php echo $result['email']?></td>
                                <td><?php echo $result['member_id']?></td>
                                <td>
                                    <?php echo $result['date']?>

                                </td>
                                <td class="uk-text-center icon-relative">
                                    <div class="uk-button-dropdown" data-uk-dropdown="{pos:'left-top',mode:'click'}">
                                        <i class="md-icon material-icons">&#xE5D4;</i>
                                        <div class="uk-dropdown uk-dropdown-small">

                                            <?php
                                                $user_menu_access=$retrive->user_menu_access($result['user_id']);
                                                $count=count($user_menu_access);
                                            ?>

                                            <ul class="uk-nav uk-text-left">
<!--                                                <li><a href="#" class="uk-text-success"><i class="material-icons">pageview</i> View profile</a></li>-->
                                                <li><a href="<?php echo SITE_URL;?>views/edit-member/?team_member=<?php echo $result['user_id']?>" class="uk-text-info"><i class="material-icons">create</i> Edit</a></li>

                                                <?php if($count>0){ ?>
                                                <?php if($active){?>
                                                <li><a href="#"  class="un_active" data-table="controller_short_info" data-id="<?php echo $result['row_id'];?>"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Deactive </a></li>
                                                <?php } else{?>
                                                <li><a href="#"  class="active" data-table="controller_short_info" data-id="<?php echo $result['row_id'];?>"   class="uk-text-danger"><i class="material-icons ">settings_power</i> Active</a></li>
                                                <?php } }?>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <ul class="uk-pagination ts_pager">
                        <li data-uk-tooltip title="Select Page">
                            <select class="ts_gotoPage ts_selectize"></select>
                        </li>
                        <li class="first"><a href="javascript:void(0)"><i class="uk-icon-angle-double-left"></i></a></li>
                        <li class="prev"><a href="javascript:void(0)"><i class="uk-icon-angle-left"></i></a></li>
                        <li><span class="pagedisplay"></span></li>
                        <li class="next"><a href="javascript:void(0)"><i class="uk-icon-angle-right"></i></a></li>
                        <li class="last"><a href="javascript:void(0)"><i class="uk-icon-angle-double-right"></i></a></li>
                        <li data-uk-tooltip title="Page Size">
                            <select class="pagesize ts_selectize">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </li>
                    </ul>

                    <button class="more_team_members md-btn md-btn-primary md-btn-mini md-btn-wave-light md-btn-icon waves-effect waves-button waves-light">Load <?php echo $per_page;?> more </button>

                </div>
            </div>
        </div>
    </div>
