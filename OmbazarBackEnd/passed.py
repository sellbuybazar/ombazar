import re
import json
import random
from .common import Common
from .retrieve import Retrieve
from .document_actions import *
from .models import *
from .helper import *
from openpyxl import load_workbook
from io import BytesIO


class Passed:
    def __init__(self):
        self.common = Common()
        self.retrieve = Retrieve()
        self.db = self.common.db

    def add_controller(self, request, controller_type):
        common = self.common
        retrieve = self.retrieve
        model = Controller()
        controller_action = ControllerAction(request)
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "message": messages,
            "error": errors
        }
        try:
            user_id = user_name = id_maker = user_name_maker = request_data = vendor_id = ''
            tree_model = tree_collection = tree_data = ''
            collection = model.collection_name()

            # Check vendor info when activity of employee
            if controller_type == "employee":
                try:
                    logged_user_id = request.session['logged_user_id']
                except:
                    messages['login_required'] = "Login first"
                else:
                    id_type_name = common.id_type_name(logged_user_id)
                    if id_type_name == "admin":
                        try:
                            activate_vendor_id = request.session['vendor_id']
                            vendor_id = activate_vendor_id
                        except:
                            messages['undefined_vendor'] = "Select vendor first"
                            return return_object
                    elif id_type_name != "vendor":
                        messages['access_error'] = "Your are not permitted this work"
                    else:
                        vendor_id = logged_user_id

            # Checking the generated user name
            try:
                user_name_maker = common.UserNameMaker(request.POST['name'])
                user_name = user_name_maker.user_name
                if not user_name:
                    errors['user_name_maker_errors'] = user_name_maker.error()
            except Exception as e:
                errors['user_name_generate'] = "user name generate problem"
                errors['user_name_exception'] = str(e)

            # Checking the generated user id is to be generate
            option = ""
            if vendor_id:
                option = vendor_id
            try:
                id_maker = common.IdMaker(controller_type, option)
                user_id = id_maker.user_id
                if not user_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception:
                errors['id_generate'] = "An error in id maker (add_controller)"
            else:
                # Checking existence this user id
                query = {"user_id": user_id}
                exist = common.exist('controllers', query)
                if exist:
                    errors['duplicate'] = 'Duplicate user id'

            # Data validation
            validation = controller_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['user_id'] = user_id
                request_data['user_name'] = user_name
                if controller_type == "vendor":
                    request_data['stores'] = []
                    request_data['point_balance'] = 0
                    request_data['account_balance'] = 0
                compare = controller_action.compare(request_data)
                errors.update(compare['errors'])

                # if controller type user then store this id in tree
                if controller_type == "user":
                    tree_model = Tree()
                    tree_collection = tree_model.collection_name()
                    tree_data = {
                        "user_id": user_id,
                        "level": 0,
                        "referer": "",
                        "up_line_referer": '',
                        "childes": [],
                        "position": "",
                        "account_balance": 0,
                        "point_balance": 0,
                        "status": "Free",
                        "time": time,
                        "expire": time,
                        "active_expire": time,
                        "distributed_point": 0,
                        "distribute_status": 0,
                        "last_distribute_time": time
                    }

                    if 'referer_mobile' in request_data:
                        referer_mobile = request_data['referer_mobile']
                        # find the referer exist or not
                        referer = common.collection_data_one("controllers", {"mobile": {"$regex": str(referer_mobile)[-10:]}}, {"user_id": 1, "mobile": 1})
                        if referer:
                            referer_user_id = referer['user_id']
                            tree_data['referer'] = referer_user_id

                            # compare first tree data with model
                            compare = tree_model.compare(tree_data)
                            if not compare:
                                errors['tree_compare'] = "Tree data compare invalid"

            else:
                errors['request_data'] = "Request data not generated!"
            # First check any error available else update
            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

                # First update total amount or controller username keyword. Because its safe for id generate.
                # (if insert fail but generate user name not duplicate anyway)(if last update fail and insert new data,
                # generate user name duplicate possible.)
                return_object['user_name'] = user_name_maker.keyword
                try:
                    keyword_update = user_name_maker.update()

                    status = keyword_update.status
                    if not status:
                        errors['user_name_keyword_update_errors'] = keyword_update.error()

                except Exception as e:
                    errors['user_name_keyword_update_error'] = "Username keyword update " + controller_type + " fail!"
                    errors['user_name_keyword_update_exception'] = str(e)

        except Exception as e:
            errors['controller_add_exception'] = str(e)
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:

                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload

                # If any error not exit then data has been sent
                try:
                    send_data = collection.insert_one(request_data)
                    send_id = str(send_data.inserted_id)
                    # if referer user available send this data in tree collection
                    if tree_data:
                        tree_collection.insert_one(tree_data)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['user_id'] = user_id
                    messages['registration_message'] = 'Registration successful'
                    return_object['user_name'] = user_name
                    del request_data['_id']
                    return_object['new_user'] = request_data
        return return_object

    def upload_collection(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        url = data = collection_name = collection_data = ''
        try:
            #  Get this file and try to read and convert json format
            data = request.FILES['files[]']
            result = data.read()
            result = json.loads(result)
            if 'name' not in result:
                messages['invalid format_name'] = "Invalid format (name) key missing"
            else:
                collection_name = result['name']

            if 'data' not in result:
                messages['invalid_format_data'] = "Invalid format (data) key missing"
            else:
                collection_data = result['data']
                count = len(collection_data)
                if not count:
                    messages['empty'] = "Empty data"
        except Exception as e:
            errors['data_reding'] = "Data reading problem"
            errors['open_exception'] = str(e)
            messages['unknown_error'] = "Invalid format or something else"
        else:
            try:
                # get collection name and and collection into json then send
                collection = self.db[collection_name]
                delete_before_data = collection.delete_many({})
                insert = collection.insert_many(collection_data)
                insert_ids = str(insert.inserted_ids)
            except Exception as e:
                errors['data_send'] = "Data not send!"
                errors['data_send_exception'] = str(e)
            else:
                if not return_object['error'] and not return_object['message']:
                    try:
                        return_object['status'] = 1
                        messages['saved'] = "Data has been saved."

                        # Its time to update/add record this upload information data in installed_collections
                        first_document = collection_data[0]
                        collection_keys = ', '.join(first_document.keys())
                        installed_data = {
                            "collection_name": collection_name,
                            "total_document": count,
                            "collection_keys": collection_keys,
                            "time": time
                        }
                        update_installed_collection = common.assign_install_collection(installed_data)
                        if update_installed_collection['status']:
                            return_object['update_installed_collection'] = 1
                            return_object['new_collection'] = installed_data
                        else:
                            return_object['update_installed_collection'] = 0
                            errors['collection_info_update'] = "Installed collection update"
                            return_object['update_installed_collection_data'] = update_installed_collection
                    except Exception as e:
                        errors['update_exception'] = str(e)
        return return_object

    def add_store(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = ""
        try:
            store_id = vendor_id = request_data = ''
            common = self.common
            model = Store()
            collection = model.collection_name()
            store_action = StoreAction(request)
            try:
                logged_user_id = request.session['logged_user_id']
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "admin":
                    try:
                        vendor_id = request.session['vendor_id']
                    except Exception:
                        messages['undefined_vendor'] = "Select a vendor first"
                elif id_type_name == "vendor":
                    vendor_id = logged_user_id
                else:
                    messages['not_access_add_store'] = "You are not permitted to add store"
            except Exception as e:
                # errors['vendor_undefined'] = "Vendor not selected"
                messages['login_undefined'] = "Login first"
                errors['login_exist_exception'] = str(e)
            try:
                id_maker = common.IdMaker("store", vendor_id)
                store_id = id_maker.user_id
                if not store_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['store_id_making'] = "Store id not generated"
                errors['store_id_exception'] = str(e)
            # validation data
            validation = store_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['store_id'] = store_id
                request_data['vendor_id'] = vendor_id
                compare = store_action.compare(request_data)
                errors.update(compare['errors'])
            else:
                errors['request_data'] = "Request data generate problem"

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['add_store_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload

                # If any error not exit then data has been sent
                try:
                    # Update stores this vendor
                    controller_model = Controller()
                    cont_collection = controller_model.collection_name()
                    find = common.collection_data_one("controllers", {"user_id": vendor_id})
                    if not find:
                        errors['vendor_find'] = "Vendor not found"
                        return return_object
                    else:
                        stores = find['stores']
                        stores.append(store_id)
                        update = cont_collection.update_one({"user_id": vendor_id}, {"$set": {"stores": stores}})
                        if update:
                            send_data = collection.insert_one(request_data)
                        else:
                            messages['error_at_update_stores'] = "vendor stores not updated"

                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['store_id'] = store_id
                    messages['store_add_message'] = 'Store added successfully'
        return return_object

    def add_category(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = user_id = category_id = request_data = ""
        try:
            common = self.common
            model = Category()
            collection = model.collection_name()
            category_action = CategoryAction(request)

            try:
                id_maker = common.IdMaker("category")
                category_id = id_maker.user_id
                if not category_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['category_id_making'] = "Category id not generated"
                errors['category_id_exception'] = str(e)

            # validation
            validation = category_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['category_id'] = category_id
                compare = category_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])
            else:
                errors['request_data'] = "Request data generate problem"

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['add_category_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'image')
                if upload:
                    request_data['image'] = upload

                # If any error not exit then data has been sent
                try:
                    send_data = collection.insert_one(request_data)
                    send_id = str(send_data.inserted_id)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['category_id'] = category_id
                    messages['category_add_message'] = 'Category added successfully'
        return return_object

    def add_item(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = user_id = item_id = request_data = vendor_id = store_id = ""
        try:
            common = self.common
            model = Item()
            collection = model.collection_name()
            item_action = ItemAction(request)

            try:
                id_maker = common.IdMaker("item")
                item_id = id_maker.user_id
                if not item_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['item_id_making'] = "Item id not generated"
                errors['item_id_exception'] = str(e)

            # validation
            validation = item_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['item_id'] = item_id
                compare = item_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

            # if vendor id activated

            try:
                logged_user_id = request.session['logged_user_id']
            except:
                pass
            else:
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "admin":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        pass
                elif id_type_name == "vendor":
                    vendor_id = logged_user_id
            if vendor_id:
                try:
                    store_id = request.session['store_id']
                except:
                    messages['store_undefined'] = "Set a store first"
                    return return_object

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

                # barcode generator
                try:
                    bar_code = request_data['bar_code']
                except Exception:
                    pass
                else:
                    if bar_code:
                        existence = common.collection_data_one("items", {"bar_code.code": bar_code})
                        if not existence:
                            bar_code_maker = common.barcode_generator(bar_code)
                            bar_code_image = bar_code_maker['bar_code_image']
                            if bar_code_image:
                                request_data['bar_code'] = {
                                    "code": bar_code,
                                    "image": bar_code_image
                                }
                            else:
                                errors['bar_code_error'] = bar_code_maker

                        else:
                            messages['bar_code_exist'] = "This bar code is already exist"

        except Exception as e:
            errors['add_item_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'images[]', True)
                if upload:
                    request_data['images'] = upload

                # If any error not exit then data has been sent
                try:
                    send_data = collection.insert_one(request_data)
                    send_id = str(send_data.inserted_id)

                    # uploader is vendor or vendor id active
                    if send_data:
                        if store_id:
                            request.POST = request.POST.copy()
                            request.POST['purchase'] = request_data['purchase']
                            request.POST['price'] = request_data['price']
                            request.POST['sale'] = request_data['sale']
                            request.POST['quantity'] = request_data['quantity']
                            request.POST['item_id'] = request_data['item_id']

                            store_item_add = self.add_to_store(request)
                            if not store_item_add['status']:
                                messages['store_item'] = "Item stored fail"
                                errors['store_item_add_data'] = store_item_add

                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['item_id'] = item_id
                    messages['item_add_message'] = 'Item added successfully'
        return return_object

    def coupon_generator(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = ""
        try:
            common = self.common
            model = Coupon()
            collection = model.collection_name()
            this_request = request.POST

            expected_amount = this_request['amount']
            coupon_type = this_request['type']

            t = 'D'
            if coupon_type != 'Print' and coupon_type != 'Digital':
                messages['invalid type'] = "Select a valid type"
            else:
                if coupon_type == 'Print':
                    t = "P"

            try:
                expected_amount = int(expected_amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid amount"
            option = "OM"+t
            last_coupon_info = common.LastUpdate("coupon", option)

            batch_amount = last_coupon_info.amount
            batch = option+str(batch_amount)
            # Make a object for coupon generate record
            coupon_batch_model = CouponBatch()
            coupon_batch_collection = coupon_batch_model.collection_name()

            request_data = {
                "batch": batch,
                "amount": expected_amount,
                "time": time,
                "type": coupon_type,
            }
            compare = coupon_batch_model.compare(request_data)
            if not compare:
                errors['compare'] = "Compare not matched"

            coupon_obj_list = []
            if not return_object['error'] and not return_object['message']:
                rands = random.sample(range(expected_amount), expected_amount)
                for n in rands:
                    coupon = str(n).zfill(6)
                    coupon = common.shuffle(coupon)
                    coupon = batch+"-"+coupon
                    obj = {
                        "coupon_number": coupon,
                        "status": 0,
                        "time": time,
                        "type": coupon_type,
                        "batch": batch,
                    }
                    coupon_obj_list.append(obj)

                # errors['datas'] = coupon_obj_list

                if not return_object['error'] and not return_object['message']:
                    # Update user id information
                    update = last_coupon_info.update()
                    status = update.status
                    if not status:
                        errors['update_error'] = update.error()

        except Exception as e:
            errors['coupon_generate_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # If any error not exit then data has been sent
                try:
                    send_coupon_batch = coupon_batch_collection.insert_one(request_data)
                    send_data = collection.insert_many(coupon_obj_list)
                    # pass
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['coupon_generate'] = 'Coupon generate successfully'
        return return_object

    def round_generator(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = round_id = ""
        try:
            common = self.common
            model = Round()
            collection = model.collection_name()

            try:
                id_maker = common.IdMaker("round")
                round_id = id_maker.user_id
                if not round_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['round_id_making'] = "Category id not generated"
                errors['round_id_exception'] = str(e)

            this_request = request.POST
            round_name = this_request['round_name']
            amount = this_request['amount']
            block_amount = 5

            try:
                amount = int(amount)
                if not amount:
                    messages['too_short'] = "amount must greater than 0"
                try:
                    div = amount % block_amount
                    if div:
                        messages['unmatched'] = "Enter a match number divided by "+str(block_amount)
                except Exception as e:
                    errors['matched_error'] = str(e)
            except ValueError:
                messages['invalid_amount'] = "Enter a numeric amount"

            # round in request data
            request_data = {
                "round_id": round_id,
                "round_name": round_name,
                "amount": amount,
                "active": 0,
                "archive": 0,
                "time": time,
            }

            common_prize = common.collection_data_one("prizes")
            if not common_prize:
                messages['default_prize'] = "Add a prize first"
                return return_object

            # Round block id making
            blocks = []
            for x in range(amount):
                block_id = round_id+str(x)
                estimated_ids = []
                lucky_id = random.randrange(block_amount)
                for n in range(block_amount):
                    prize_id = ""
                    if n == lucky_id:
                        prize_id = common_prize['prize_id']
                    es_ob = {
                        "id": n,
                        "prize_id": prize_id,
                        "status": 0
                    }
                    estimated_ids.append(es_ob)

                block = {
                    "block_id": block_id,
                    "round_id": round_id,
                    "estimated_ids": estimated_ids,
                    "active": 1,
                    "time": time,
                }
                blocks.append(block)

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['round_generate_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:

                # If any error not exit then data has been sent
                try:
                    send_round_data = collection.insert_one(request_data)

                    round_block_model = RoundBlock()
                    collection = round_block_model.collection_name()
                    send_data = collection.insert_many(blocks)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['round_id'] = round_id
                    messages['round_add_message'] = 'Round added successfully'
        return return_object

    def add_prize(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = ""
        try:
            prize_id = request_data = ''
            common = self.common
            model = Prize()
            collection = model.collection_name()
            prize_action = PrizeAction(request)
            try:
                vendor_id = request.session['logged_user_id']
            except Exception as e:
                errors['user_undefined'] = "User not selected"
                messages['user_undefined'] = "Login first"
                errors['user_exist_exception'] = str(e)
            try:
                id_maker = common.IdMaker("prize")
                prize_id = id_maker.user_id
                if not prize_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['prize_id_making'] = "Prize id not generated"
                errors['prize_id_exception'] = str(e)
            # validation data
            validation = prize_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['prize_id'] = prize_id
                compare = prize_action.compare(request_data)
                errors.update(compare['errors'])
            else:
                errors['request_data'] = "Request data generate problem"

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['add_prize_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:

                # If any error not exit then data has been sent
                try:
                    send_data = collection.insert_one(request_data)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['prize_id'] = prize_id
                    messages['prize_add_message'] = 'Prize added successfully'
        return return_object

    def add_corporate(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = corporate_id = request_data = ""
        try:
            common = self.common
            model = Corporate()
            collection = model.collection_name()
            corporate_action = CorporateAction(request)

            try:
                id_maker = common.IdMaker("corporate")
                corporate_id = id_maker.user_id
                if not corporate_id:
                    errors['id_generate_errors'] = id_maker.error()
            except Exception as e:
                errors['item_id_making'] = "Item id not generated"
                errors['item_id_exception'] = str(e)

            # validation
            validation = corporate_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['corporate_id'] = corporate_id
                compare = corporate_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['add_corporate_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Upload image. If image upload fail a default image will be upload
                upload = common.file_upload(request, 'images[]', True)
                if upload:
                    request_data['images'] = upload

                # If any error not exit then data has been sent
                try:
                    send_data = collection.insert_one(request_data)
                    send_id = str(send_data.inserted_id)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['corporate_id'] = corporate_id
                    messages['corporate_add_message'] = 'Corporate added successfully'
        return return_object

    def add_to_store(self, request):

        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            common = self.common
            model = StoreItem()
            store_item_action = StoreItemAction(request)
            collection = model.collection_name()
            store_id = vendor_id = store_item_id = id_maker = request_data = ""
            item_point = 0
            # Store id available
            try:
                store_id = request.session['store_id']
            except Exception:
                messages['undefined_store'] = "Select a store first"
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_undefined'] = "Login first"
            else:
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name == "vendor":
                    vendor_id = logged_user_id
                elif id_type_name == "admin" or id_type_name == "employee":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        errors['undefined_vendor'] = "Undefined vendor"

            # Make id for this new item
            try:
                id_maker = common.IdMaker("store_item", store_id)
                # messages['total'] = id_maker.total_controller
                # return return_object
                store_item_id = id_maker.user_id
                if not store_item_id:
                    errors['store_item_id'] = id_maker.error()
            except Exception as e:
                errors['store_item_id_exception'] = str(e)

            # validation
            validation = store_item_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['store_item_id'] = store_item_id
                # item point for charge point balance
                item_point = request_data['point']
                if vendor_id:
                    vendor_query = {
                        "user_id": vendor_id,
                        "point_balance": {
                            "$gte": item_point
                        }
                    }
                    vendor_info = common.collection_data_one("controllers", vendor_query)
                    if not vendor_info:
                        messages['low_balance'] = "Insufficient balance"
                        return return_object
                else:
                    messages['undefined_vendor'] = "Vendor undefined"
                    return return_object

                compare = store_item_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

            if not return_object['error'] and not return_object['message']:
                # Update user id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)

        except Exception as e:
            errors['add_to_store_exception'] = str(e)

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    send = collection.insert_one(request_data)
                    if not send:
                        errors['store_item_data_send'] = "Data not send"
                    else:
                        return_object['status'] = 1
                        return_object['store_item_id'] = store_item_id
                        del request_data["_id"]

                        return_object['item_info'] = request_data
                except Exception as e:
                    errors['store_item_exception'] = str(e)
        return return_object

    def order(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        try:
            order_id = id_maker = request_data = order_code = ""
            store_id = vendor_id = ""

            model = Order()
            order_items_model = OrderItems()
            order_items_collection = order_items_model.collection_name()
            this_request = request.POST
            order_action = OrderAction(request)
            collection = model.collection_name()

            # Store id available

            try:
                logged_user_id = request.session['logged_user_id']
            except:
                pass
                # errors['login_required'] = "Login first"
            else:
                id_type_name = common.id_type_name(logged_user_id)
                if id_type_name != "user":
                    try:
                        store_id = request.session['store_id']
                    except Exception:
                        messages['undefined_store'] = "Select a store first"
                elif id_type_name != "vendor" and id_type_name != "user":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        messages['undefined_vendor'] = "Unknown vendor"
                        return return_object
                elif id_type_name == "vendor":
                    vendor_id = logged_user_id

            # Make id for this new item
            try:
                id_maker = common.IdMaker("order")
                order_id = id_maker.user_id
                if not order_id:
                    errors['order_id'] = id_maker.error()
            except Exception as e:
                errors['order_id_exception'] = str(e)

            # Item code making
            try:
                last_update = common.LastUpdate("order")
                amount = last_update.amount
            except Exception:
                errors['last_order_amount'] = "Last order amount not found"
            else:
                order_code = "Order #" + str(amount)

            # all values from form
            try:
                data = this_request['data']
            except Exception:
                errors['data_undefined'] = "Data not found"
            else:
                data = json.loads(data)
            # validation
            validation = order_action.data_validation()
            errors.update(validation['errors'])
            messages.update(validation['messages'])
            if "request_data" in validation:
                request_data = validation['request_data']
                request_data['order_id'] = order_id
                request_data['order_code'] = order_code
                compare = order_action.compare(request_data)
                errors.update(compare['errors'])
                messages.update(compare['messages'])

            # Data or orders item

            order_items = []
            for item in data:
                order_item = {}
                new_item = {
                    "purchase": item['purchase'],
                    "price": item['price'],
                    "sale": item['sale'],
                    "vat": item['vat'],
                    "item_id": item['item_id'],
                    "quantity": item['quantity'],
                    "item_name": item['item_name'],
                    # "point": item['point'],
                }
                try:
                    point = common.point_maker(new_item)
                except:
                    errors['point_error'] = str(common.error_info())
                try:
                    line_total = common.line_total(new_item)
                except:
                    errors['line_total_error'] = str(common.error_info())
                new_item['line_total'] = line_total
                new_item['point'] = point
                order_item['status'] = "Pending"
                order_item['time'] = time
                order_item['order_id'] = order_id
                order_item['store_id'] = item['store_id']
                order_item['store_item_id'] = item['store_item_id']
                order_item['item_info'] = new_item
                order_items.append(order_item)

            # Order total information
            try:
                order_total_info = common.order_total_info(order_items)
            except:
                errors['order_total_error'] = str(common.error_info())
            else:
                request_data['total_info'] = order_total_info
            if not order_items:
                messages['order_item_empty'] = "Order item empty"
            else:
                first_order_data = order_items[0]
                compare = order_items_model.compare(first_order_data)
                if not compare:
                    errors['order_item_compare'] = "Order items not matched"

            if not return_object['error'] and not return_object['message']:
                # Update order id information
                update_error = common.user_id_update(return_object, id_maker)
                errors.update(update_error)
            else:
                return return_object

            customer_id = request_data['customer_id']
            grand_total = request_data['total_info']['grand_total']
            point_total = request_data['total_info']['point']
            order_type = request_data['order_type']
            if order_type == "web":
                customer_query = {
                    "user_id": customer_id,
                    "account_balance": {
                        "$gte": grand_total
                    }
                }
                customer_info = common.collection_data_one("tree", customer_query)
                if not customer_info:
                    messages['insufficient'] = "Insufficient balance"
                else:
                    tree_model = Tree()
                    tree_collection = tree_model.collection_name()
                    tree_where = {
                        "user_id": customer_id
                    }
                    negative_tree_balance = grand_total - (grand_total + grand_total)
                    tree_which = {
                        "$inc": {
                            "account_balance": negative_tree_balance
                        }
                    }
                    try:
                        tree_collection.update_one(tree_where, tree_which)
                    except:
                        errors['user_balance'] = "User balance not updated"
                        errors['error'] = str(common.error_info())
                    else:
                        # make a transaction for record
                        transaction_maker = common.transaction_maker(request, "product_buy", {
                            "user_id": customer_id,
                            "amount": grand_total,
                            "description": "Buy products",
                            "status": 1
                        })
                        if not transaction_maker:
                            errors['transaction'] = "Transaction record failed"
        except Exception as e:
            errors['order_exception'] = str(common.error_info())

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    send = collection.insert_one(request_data)
                    send_items = order_items_collection.insert_many(order_items)

                    # point update
                    point_update = common.user_point_add(customer_id, grand_total)
                    if not point_update:
                        errors['user_point_add'] = "User point not added"

                    # Expire date update
                    expire_date_manage = common.user_expire_manager(customer_id, grand_total)
                    if not expire_date_manage:
                        errors['expire_date'] = "User expire date manage fail"

                    # collect amount of every store
                    store_points = {}

                    # Stock update
                    requests = []
                    store_item_model = StoreItem()
                    store_item_collection = store_item_model.collection_name()

                    for item in order_items:
                        store_id = item['store_id']
                        point = item['item_info']['point']
                        try:
                            store_point = store_points[store_id]
                        except:
                            store_points[store_id] = point
                        else:
                            store_points[store_id] = store_point + point

                        store_item_id = item['store_item_id']
                        quantity = item['item_info']['quantity']
                        negative_quantity = quantity - (quantity + quantity)
                        requests.append(UpdateOne(
                            {
                                "store_item_id": store_item_id
                            },
                            {
                                "$inc": {
                                    "quantity": negative_quantity
                                }
                            }
                        ))
                    stock_update = store_item_collection.bulk_write(requests)

                    # update store point
                    controller_model = Controller()
                    controller_collection = controller_model.collection_name()

                    transaction_model = Transaction()
                    transaction_collection = transaction_model.collection_name()
                    point_requests = []

                    last_transaction_amount = common.total_amount("transaction") + 1
                    transaction_requests = []
                    for store in store_points:
                        point = store_points[store]
                        where = {
                            "stores": {
                                "$in": [store]
                            }
                        }
                        negative_point = point - (point + point)
                        which = {
                            "$inc": {
                                "point_balance": negative_point
                            }
                        }
                        point_requests.append(UpdateOne(where, which))
                        # transaction record
                        transaction_id = common.new_user_id(last_transaction_amount, "transaction")
                        vendor_id = common.id_info(store, "option") + "000000000000000000"
                        data_object = {
                            "transaction_id": transaction_id,
                            "user_id": vendor_id,
                            "status": 1,
                            "time": time,
                            "type": "product_sale",
                            "amount": point,
                            "description": "Update stock and information"
                        }
                        transaction_requests.append(data_object)
                        last_transaction_amount += 1
                    controller_collection.bulk_write(point_requests)
                    transaction_collection.insert_many(transaction_requests)

                    if not send:
                        errors['order_data_send'] = "Data not send"
                    else:
                        return_object['status'] = 1
                        return_object['order_id'] = order_id
                    if not send_items:
                        errors['order_item_send'] = "Order item not send"
                except Exception as e:
                    errors['order_exception'] = str(e)
        return return_object

    def set_tree_position(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }

        try:
            referer_update_object = {}
            common = self.common
            model = Tree()
            collection = model.collection_name()
            this_request = request.POST
            referer = this_request['referer']
            new_child = this_request['new_child']
            up_line_referer = this_request['up_line_referer']
            position = this_request['position']
            level = position_cost= 0
            try:
                position_cost_info = common.last_update("position_cost")
                if not position_cost_info:
                    messages['position_cost'] = "Position cost not set"
                    return return_object
                else:
                    position_cost = position_cost_info['amount']
                find_referer = common.collection_data_one("tree", {"user_id": referer})
                if find_referer:
                    account_balance = find_referer['account_balance']
                    referer_position = find_referer['position']
                    charge_applicable = True
                    if referer_position == "Top":
                        charge_applicable = False
                    if charge_applicable:
                        if account_balance < position_cost:
                            messages['balance_finish'] = "Insufficient balance"
                            return return_object
                        else:
                            referer_new_balance = account_balance - position_cost
                            referer_update_object = {
                                "account_balance": referer_new_balance
                            }
                    if not return_object['error'] and not return_object['message']:
                        find_levels = common.last_update("total_level")
                        if not find_levels:
                            level = 1
                            assign_update = common.assign_last_update("total_level", "", {
                                "value": [level]
                            })
                            if not assign_update:
                                errors['assign_level'] = "Level not assigned"
                        else:
                            up_line_info = common.collection_data_one("tree", {"user_id": up_line_referer})
                            if not up_line_info:
                                messages['unknown_up_line'] = "Parent info not found"
                            else:
                                parent_level = up_line_info['level']
                                level = parent_level+1
                                if level not in find_levels['value']:
                                    last_update_model = LastUpdate()
                                    last_update_collection = last_update_model.collection_name()
                                    level_where = {
                                        "type": "total_level",
                                        "option": ""
                                    }
                                    level_which = {
                                        "$push": {
                                            "value": level
                                        }
                                    }
                                    level_update = last_update_collection.update_one(level_where, level_which)
                                    if not level_update:
                                        errors['level_update'] = "Level not updated"

            except Exception:
                messages['unknown_error'] = "Something went wrong!"

        except Exception as e:
            errors['set_position_exception'] = str(e)

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # set up line referer and position
                    requests = []
                    which = {
                        "$set": {
                            "up_line_referer": up_line_referer,
                            "position": position,
                            "status": "Paid",
                            "level": level
                        }
                    }
                    where = {
                        "user_id": new_child
                    }
                    requests.append(UpdateOne(where, which))

                    # set referer account balance
                    if referer_update_object:
                        where = {
                            "user_id": referer,
                        }
                        which = {
                            "$set": referer_update_object
                        }
                        requests.append(UpdateOne(where, which))
                    # set childes
                    where = {
                        "user_id": up_line_referer
                    }
                    which = {
                        "$push": {
                            "childes": new_child
                        }
                    }
                    requests.append(UpdateOne(where, which))
                    collection.bulk_write(requests)
                except:
                    errors['tree_position'] = "Position set failed"
                    errors['error_info'] = str(common.error_info())
                else:
                    return_object['status'] = 1

        return return_object

    def point_exchange(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = logged_user_id = request_data = ""
        user_new_account_balance = user_new_point_balance = 0
        try:
            referer_update_object = {}
            common = self.common
            this_request = request.POST
            amount = this_request['amount']
            description = this_request['description']
            tr_type = "point_exchange"
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            # Check amount data validation
            try:
                amount = int(amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid numeric amount"

            # Check point rule assigned or not
            point_exchange_rule = common.collection_data_one("point_rules", {"id": "2"})
            common.preview(point_exchange_rule)
            if not point_exchange_rule:
                messages['point_rule_undefined'] = "Point exchange rule not set"
                return return_object
            else:
                rule_amount = point_exchange_rule['amount']
                request_data = {
                    "amount": amount,
                    "description": description,
                    "status": 1
                }
                user_info = common.collection_data_one("tree", {"user_id": logged_user_id})
                if not user_info:
                    messages['undefined_user'] = "Unknown user"
                else:
                    user_current_point = user_info['point_balance']
                    if user_current_point:
                        user_account_balance = user_info['account_balance']
                        user_point_balance = user_info['point_balance']
                        converted_amount = amount * rule_amount
                        user_new_account_balance = user_account_balance + converted_amount
                        user_new_point_balance = user_point_balance - amount
                    else:
                        messages['empty_balance'] = "Insufficient point balance"
        except Exception as e:
            errors['point_exchange_exception'] = str(e)

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    send_transaction_data = common.transaction_maker(request, tr_type, request_data)
                    if send_transaction_data:
                        transaction_last_update = send_transaction_data.update()
                        if transaction_last_update.status:
                            if send_transaction_data:
                                tree_model = Tree()
                                tree_collection = tree_model.collection_name()
                                where = {
                                    "user_id": logged_user_id
                                }
                                which = {
                                    "$set": {
                                        "point_balance": user_new_point_balance,
                                        "account_balance": user_new_account_balance
                                    }
                                }
                                balance_update = tree_collection.update_one(where, which)
                                if not balance_update:
                                    errors['balance_update'] = "Balance not updated"
                        else:
                            errors['transaction_last_update'] = "Transaction last update fail"
                    else:
                        errors['transaction'] = "Transaction fail"
                except:
                    errors['point_exchange_exception'] = "Point exchange fail"
                    errors['error_info'] = str(common.error_info())
                else:
                    return_object['status'] = 1
                    return_object['account_balance'] = user_new_account_balance
                    return_object['point_balance'] = user_new_point_balance

        return return_object

    def point_distributor(self):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        retrieve = self.retrieve
        try:
            tree_model = Tree()
            tree_collection = tree_model.collection_name()
            levels = common.last_update("total_level")
            if levels:
                levels = levels['value']
                levels.reverse()

                # while all levels and get self level type trees
                distributed_point_slabs = retrieve.distributed_point_slabs()
                if not distributed_point_slabs:
                    messages['undefined_point_slabs'] = "Distribited point slabs not set"
                    return return_object
                else:
                    slab_data = distributed_point_slabs['value']
                    slabs = []
                    for x in slab_data:
                        slab_amount = x['amount']
                        slabs.append(slab_amount)
                    min_slab_amount = min(slabs)

                for x in levels:
                    level = x
                    # if data has not finish loop is continuous
                    i = 0
                    while i < 1:
                        limit = 50000
                        pipeline = [
                            {
                                "$lookup": {
                                    "from": "tree",
                                    "localField": "childes",
                                    "foreignField": "user_id",
                                    "as": "childes"
                                }
                            },
                            {
                                "$match": {
                                    "level": level,
                                    "status": "Paid",
                                    "up_line_referer": {
                                        "$ne": ""
                                    },
                                }
                            },
                            {
                                "$addFields": {
                                    "total_child": {
                                        "$size": {
                                            "$filter": {
                                                "input": "$childes",
                                                "cond": {
                                                    "$gte": ["$$this.distributed_point", min_slab_amount]
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                "$project": {
                                    "up_line_referer": 1,
                                    "point_balance": 1,
                                    "distributed_point": 1,
                                    "distribute_status": 1,
                                    "last_distribute_time": 1,
                                    "user_id": 1,
                                    "_id": 0,
                                    "childes": {
                                        "up_line_referer": 1,
                                        "point_balance": 1,
                                        "distributed_point": 1,
                                        "distribute_status": 1,
                                        "last_distribute_time": 1,
                                        "user_id": 1,

                                    },
                                    "total_child": 1

                                }
                            },
                            {
                                "$match": {
                                    "total_child": 2,

                                }
                            }

                        ]
                        data = common.aggregate_collection_data("tree", pipeline, 100)
                        # return_object['data'+str(x)] = data
                        if data:
                            find_data = data['find_data']
                            if find_data:
                                # initiate  this bulk request list
                                requests = []
                                for item in find_data:
                                    childes = item['childes']
                                    # get points list
                                    distributed_points = []
                                    for child in childes:
                                        distributed_point = child['distributed_point']
                                        distributed_points.append(distributed_point)
                                    # get minimum point
                                    min_point = min(distributed_points)
                                    # what is the minimum point index for 0 value this tree distributed point
                                    min_point_index = distributed_points.index(min_point)
                                    next_child_index_finder = {
                                        "0": 1,
                                        "1": 0
                                    }
                                    # zero affected child
                                    match_child = childes[min_point_index]
                                    # next child
                                    next_child_index = next_child_index_finder[str(min_point_index)]
                                    next_child = childes[next_child_index]
                                    # what is the distributed point get after loop in distributed point slabs
                                    match_point = min_point

                                    for s_point in sorted(slabs):
                                        if s_point <= min_point:
                                            match_point = s_point
                                        else:
                                            break
                                    # update up line referer
                                    user_id = item['user_id']
                                    requests.append(UpdateOne(
                                        {
                                            "user_id": user_id
                                        },
                                        {
                                            "$inc": {
                                                "point_balance": match_point,
                                                "distributed_point": match_point
                                            }
                                        }
                                    ))

                                    # match_child update
                                    match_child_user_id = match_child['user_id']
                                    requests.append(UpdateOne(
                                        {
                                            "user_id": match_child_user_id
                                        },
                                        {
                                            "$set": {
                                                "distributed_point": 0
                                            }
                                        }
                                    ))

                                    # next child update
                                    next_child_user_id = next_child['user_id']
                                    negative_match_point = match_point - (match_point + match_point)
                                    requests.append(UpdateOne(
                                        {
                                            "user_id": next_child_user_id
                                        },
                                        {
                                            "$inc": {
                                                "distributed_point": negative_match_point
                                            }
                                        }
                                    ))

                                tree_collection.bulk_write(requests)
                            else:
                                break
                        else:
                            break
                        i -= 1

                # return_object['levels'] = levels
        except:
            errors['error_info'] = str(common.error_info())
            print("Error")
        else:
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1
                print("Done")
            else:
                print(return_object)
        finally:
            try:
                id_maker = common.IdMaker("cron")
                cron_id = id_maker.user_id
                cron_model = Cron()
                cron_collection = cron_model.collection_name()
                cron_object = {
                    "cron_id": cron_id,
                    "type": 'Point distribution',
                    "description": 'Every night Point distribution',
                    "status": return_object['status'],
                    "time": time,
                    "more_info": return_object,
                }
                cron_collection.insert_one(cron_object)
            except:
                pass
        return return_object

    def import_user(self, request):
        common = self.common
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        url = data = collection_name = collection_data = ''
        try:
            #  Get this file and try to read and convert json format
            data = request.FILES['files[]'].read()
            wb = load_workbook(filename=BytesIO(data))
            sheets = wb.sheetnames
            data_info_pattern = [
                {
                    "key_name": "user_name",
                    "required": 1,
                    "validation": "",
                },
                {
                    "key_name": "mobile",
                    "required": 1,
                    "validation": "int",
                },
                {
                    "key_name": "position",
                    "required": 1,
                    "validation": "position",
                },
                {
                    "key_name": "account_balance",
                    "required": 1,
                    "validation": "int",
                },
                {
                    "key_name": "point_balance",
                    "required": 1,
                    "validation": "int",
                },
                {
                    "key_name": "referer",
                    "required": 0,
                    "validation": "referer",
                },
                {
                    "key_name": "up_line_mobile",
                    "required": 0,
                    "validation": "up_line_referer",
                },
                {
                    "key_name": "status",
                    "required": 1,
                    "validation": "status",
                },
                {
                    "key_name": "expire_date",
                    "required": 0,
                    "validation": "time",
                },
                {
                    "key_name": "active_expire_date",
                    "required": 0,
                    "validation": "time",
                },
                {
                    "key_name": "post_code",
                    "required": 1,
                    "validation": "int",
                },
                {
                    "key_name": "email",
                    "required": 0,
                    "validation": "email",
                },
                {
                    "key_name": "address_line_1",
                    "required": 1,
                    "validation": "address",
                },
                {
                    "key_name": "address_line_2",
                    "required": 0,
                    "validation": "",
                },
                {
                    "key_name": "password",
                    "required": 0,
                    "validation": "",
                }
            ]
            number_of_field_in_pattern = len(data_info_pattern)

            # Last total user created amount in last update document
            total_user = 1
            try:
                total_user = common.total_amount("user") + 1
            except:
                errors['total_amount'] = "Total amount not found"

            users = []
            mobiles = []
            user_ids = {}
            childes = {}
            sheet_users = []
            total_sheet_row = 0
            # we have done this work on 2 loop. first store all mobile store data store in mobiles and last check
            # all validation have done or not
            process = 0
            while process <= 1:
                data_errors = []
                sheet_sl = 1
                for sheet in wb:
                    # sheet wise users
                    if process == 1:
                        sheet_users = []
                        users.append(sheet_users)
                    sheet_errors = []
                    data_errors.append(
                        {
                            "sheet": sheet_sl,
                            "errors": sheet_errors
                        }
                    )

                    row_sl = 0
                    for row in sheet.rows:
                        if row_sl != 0:
                            number_of_field_in_row = len(row)
                            if number_of_field_in_row != number_of_field_in_pattern:
                                sheet_errors.append(
                                    {
                                        "format": "Data missing in " + str(row_sl) + "line"
                                    }
                                )

                            user = {}
                            cell_sl = 1
                            for cell in row:
                                cell_pattern = data_info_pattern[cell_sl - 1]
                                cell_data = cell.value
                                if cell_data is None:
                                    cell_data = ""
                                # if process 2 then start validation
                                if process == 1:
                                    user[cell_pattern['key_name']] = cell_data
                                    # store this mobile number in mobiles list for duplicate check

                                    if cell_pattern['key_name'] == "mobile":
                                        count_mobile = mobiles.count(cell_data)
                                        if count_mobile > 1:
                                            sheet_errors.append(
                                                {
                                                    "empty": str(row_sl) + " no. row duplicate mobile number"
                                                }
                                            )

                                    if cell_pattern['key_name'] == "up_line_mobile":
                                        # make childs for this up line referer
                                        before_childes = []
                                        if cell_data in childes:
                                            before_childes = childes[cell_data]
                                            childes[cell_data] = before_childes.append(user['mobile'])
                                        else:
                                            childes[cell_data] = [user['mobile']]

                                    required = common.required(cell_pattern['required'], cell_data)
                                    validation = common.validation(cell_pattern['validation'], cell_data, mobiles)
                                    if not required:
                                        sheet_errors.append(
                                            {
                                                "empty": str(row_sl)+" no. row, " + str(cell_sl) + " no. cell: data empty"
                                            }
                                        )
                                    elif validation:
                                        sheet_errors.append(
                                            {
                                                "validation": str(row_sl)+" no. row, " + str(cell_sl)+" no. cell: " + validation['message']
                                            }
                                        )
                                else:
                                    if cell_pattern['key_name'] == "mobile":
                                        mobiles.append(cell_data)
                                cell_sl += 1
                            if process == 1:
                                try:
                                    user_id = common.new_user_id(total_user)
                                    user['user_id'] = user_id
                                except:
                                    errors['new_user_id'] = "New user maker failed"
                                else:
                                    total_user += 1
                                    user_mobile = user['mobile']
                                    user_ids[user_mobile] = user_id
                                    total_sheet_row += 1
                                sheet_users.append(user)
                        row_sl += 1
                    sheet_sl += 1

                # count processed error
                total_error_in_process = 0
                for sheet_error in data_errors:
                    total_error_in_process += len(sheet_error['errors'])
                if total_error_in_process:
                    return_object['import_errors'] = data_errors
                    return return_object
                process += 1
            if total_sheet_row != len(user_ids):
                errors['row_and_data'] = "Total sheet row and user ids not matched"
            # errors['e'] = users
            ready_list = []
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                # sorted as user and tree list

                for sheet_users in users:
                    tree_data = []
                    account_data = []
                    ready_list.append({
                        "tree": tree_data,
                        "account": account_data
                    })
                    for user in sheet_users:
                        # common_data
                        calling_code = "880"
                        # user data
                        mobile = user['mobile']
                        password = user['password']
                        name = user['user_name']
                        email = user['email']
                        address_line_1 = user['address_line_1']
                        address_line_2 = user['address_line_2']
                        post_code = user['post_code']
                        referer_mobile = user['referer']
                        up_line_referer_mobile = user['up_line_mobile']
                        user_id = user['user_id']
                        user_name = common.id_info(user_id, "type")
                        user_name += common.id_info(user_id, "sl")
                        account_object = {
                            "calling_code": calling_code,
                            "mobile": str(mobile),
                            "password": password,
                            "active": 1,
                            "archive": 0,
                            "time": time,
                            "name": name,
                            "email": email,
                            "address_line_1": address_line_1,
                            "address_line_2": address_line_2,
                            "image": "/media/default/profile.png",
                            "nationality": {
                                "country_name": "Bangladesh",
                                "calling_code": "880",
                                "region": "Asia",
                                "flag": "https://restcountries.eu/data/bgd.svg"
                            },
                            "post_code": post_code,
                            "referer_mobile": str(referer_mobile),
                            "user_id": user_id,
                            "user_name": user_name,
                        }
                        account_data.append(account_object)

                        # common info
                        level = 0
                        position = user['position']
                        account_balance = user['account_balance']
                        point_balance = user['point_balance']
                        status = user['status']
                        expire = user['expire_date']
                        if not expire:
                            expire = time
                        active_expire = user['active_expire_date']
                        if not active_expire:
                            active_expire = time
                        distributed_point = 0
                        distribute_status = 1
                        last_distribute_time = time
                        user_childes = []
                        try:
                            user_childes = childes[mobile]
                            user_childes_user_id = []
                            for child in user_childes:
                                user_childes_user_id.append(user_ids[child])
                            user_childes = user_childes_user_id
                        except:
                            pass

                        user_referer = ""
                        try:
                            user_referer = user_ids[referer_mobile]
                        except:
                            pass
                        up_line_referer = ""
                        try:
                            up_line_referer = user_ids[up_line_referer_mobile]
                        except:
                            pass

                        tree_object = {
                            "user_id": user_id,
                            "level": level,
                            "referer": user_referer,
                            "up_line_referer": up_line_referer,
                            "childes": user_childes,
                            "position": position,
                            "account_balance": account_balance,
                            "point_balance": point_balance,
                            "status": status,
                            "expire": expire,
                            "active_expire": active_expire,
                            "distributed_point": distributed_point,
                            "distribute_status": distribute_status,
                            "last_distribute_time": last_distribute_time,
                        }
                        tree_data.append(tree_object)
            # return_object['ready'] = ready_list
            # First check any error available then update total user assign
            if not return_object['error'] and not return_object['message']:
                try:
                    assign_total_user = common.assign_last_update("user", "", {"amount": total_user})
                except:
                    errors['last_update'] = "Last update not assigned"
        except Exception as e:
            errors['open_exception'] = str(common.error_info())
        else:
            try:
                # First check any error available
                if not return_object['error'] and not return_object['message']:
                    sheet_number = 1
                    for sheet in ready_list:
                        try:
                            accounts = sheet['account']
                            trees = sheet['tree']
                            controller_model = Controller()
                            controller_collection = controller_model.collection_name()
                            tree_model = Tree()
                            tree_collection = tree_model.collection_name()
                            # First send account data
                            if accounts:
                                controller_collection.insert_many(accounts)
                            # Last send tree data
                            if trees:
                                tree_collection.insert_many(trees)
                        except:
                            errors['data_error_sheet' + str(sheet_number)] = "sheet " + str(sheet_number) + " data not send"
                            errors['error_send_info'] = str(common.error_info())
                        sheet_number += 1
                # get collection name and and collection into json then send
                pass
            except Exception as e:
                errors['data_send'] = "Data not send!"
                errors['data_send_exception'] = str(e)
            else:
                total_user = self.retrieve.imported_user_info()
                return_object['status'] = 1
                return_object['total_user'] = total_user['total']

        return return_object

    def send_code_forgot_user(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        user_id = mobile = email = ""
        try:
            try:
                email = request.POST['email']
            except:
                errors['email_undefined'] = "Email not found"
            try:
                user_id = request.POST['user_id']
            except:
                errors['user_id_undefined'] = "User id not found"
            try:
                mobile = request.POST['mobile']
            except:
                errors['mobile_undefined'] = "Mobile number not found"

            confirmation_number = random.randrange(100000, 999999)
            conf_object = {
                "number": confirmation_number,
                "expire": after_time(0, 0, 5),
                "user_id": user_id,
            }
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                request.session['confirmation'] = conf_object

        except:
            errors['error_info'] = str(common.error_info())
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                query = {
                    "user_id": user_id,
                    "mobile": mobile,
                    "email": email
                }
                validation_this_user = common.collection_data_one("controllers", query, {"user_id": 1})
                if not validation_this_user:
                    messages['invalid_user'] = "Invalid user"
                    return return_object
                else:
                    from_email = "info@noyonmondol.com"
                    forgot_system_email = common.last_update("forgot_email")
                    if not forgot_system_email:
                        messages['email_undefined'] = "Deliver mail not set"
                        return return_object
                    else:
                        from_email = forgot_system_email['value']
                        if not from_email:
                            messages['email_undefined'] = "Deliver mail not set"
                            return return_object

                    to_email = email
                    subject = "New password confirmation code"
                    messages = "<p>Your password recovery confirmation code is below</p>"
                    messages += "<h2>"+str(confirmation_number)+"</h2>"
                    send_mail = common.send_mail(from_email, to_email, subject, messages)
                    if send_mail.status_code == 202:
                        return_object['status'] = 1
                    else:
                        messages['not_send'] = "Code send failed !"
                        return_object['status'] = 0
                    return_object['confirmation_code'] = confirmation_number
        return return_object

    def confirm_code(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        common = self.common
        confirmation_code = ""
        confirmation_object = {}
        try:
            try:
                confirmation_code = request.POST['confirmation_code']
                confirmation_code = int(confirmation_code)
            except:
                messages['confirmation_error'] = "Confirmation code empty"
            try:
                confirmation_object = request.session['confirmation']
            except:
                errors['conf_object'] = "We don't have any secret code from you"


        except:
            errors['error_info'] = str(common.error_info())
        else:
            # First check any error available
            try:
                if not return_object['error'] and not return_object['message']:
                    conf_expire_time = confirmation_object['expire']
                    conf_code = confirmation_object['number']
                    if conf_expire_time < time:
                        messages['expire'] = "Token expire"
                    elif conf_code != confirmation_code:
                        messages['conf_code'] = "Confirmation code not matched!"
                    else:
                        return_object['status'] = 1
            except:
                errors['error_info'] = str(common.error_info())
        return return_object

    def top_up(self, request, tr_type="top_up"):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = logged_user_id = request_data = transaction_id = ""
        common = self.common
        try:
            this_request = request.POST
            amount = this_request['amount']
            description = this_request['description']
            transaction_number = this_request['transaction_number']
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            # Check amount data validation
            try:
                amount = int(amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid numeric amount"

        except Exception as e:
            errors['top_up_exception'] = str(common.error_info())

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    request_data = {
                        "amount": amount,
                        "description": description,
                        "more_info": {
                            "transaction_number": transaction_number
                        },
                    }
                    id_type = common.id_type_name(logged_user_id)
                    if id_type == "employee":
                        try:
                            vendor_id = request.session['vendor_id']
                        except:
                            errors['undefined_vendor'] = "Undefined vendor"
                        else:
                            request_data['user_id'] = vendor_id
                    send_transaction_data = common.transaction_maker(request, tr_type, request_data)
                    if send_transaction_data:
                        transaction_last_update = send_transaction_data.update()
                        if not transaction_last_update.status:
                            errors['transaction_last'] = "Transaction last update fail"
                    else:
                        errors['request_fail'] = "Request fail"
                except:
                    errors['top_up_exception'] = "Top up fail"
                    errors['error_info'] = str(common.error_info())
                else:
                    return_object['status'] = 1
        return return_object

    def withdraw(self, request, withdraw_type):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        id_maker = logged_user_id = request_data = transaction_id = ""
        common = self.common
        try:
            this_request = request.POST
            amount = this_request['amount']
            tr_type = withdraw_type
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            # Check amount data validation
            try:
                amount = int(amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid numeric amount"

        except Exception as e:
            errors['top_up_exception'] = str(common.error_info())

        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    request_data = {
                        "amount": amount,
                        "description": "",
                        "more_info": {
                            "transaction_number": ""
                        },
                    }
                    send_transaction_data = common.transaction_maker(request, tr_type, request_data)
                    if send_transaction_data:
                        transaction_last_update = send_transaction_data.update()
                        if not transaction_last_update.status:
                            errors['transaction_last'] = "Transaction last update fail"
                    else:
                        errors['request_fail'] = "Request fail"
                except:
                    errors['withdraw_exception'] = "Withdraw fail"
                    errors['error_info'] = str(common.error_info())
                else:
                    return_object['status'] = 1
        return return_object

    def buy_coupon(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_data = user_id = ""
        my_list = []
        try:
            common = self.common
            model = MyCoupon()
            collection = model.collection_name()
            this_request = request.POST
            amount = this_request['amount']
            try:
                user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            try:
                id_type_name = common.id_type_name(user_id)
                if id_type_name != "user":
                    messages['not_permitted'] = "You are not permitted for this service"
                    return return_object
            except:
                pass

            try:
                amount = int(amount)
            except ValueError:
                messages['invalid_amount'] = "Enter a valid numeric amount"
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                pipeline = [
                    {
                        "$lookup": {
                            "from": "my_coupons",
                            "localField": "coupon_number",
                            "foreignField": "coupon_number",
                            "as": "exist_item",
                        }
                    },
                    {
                        "$match": {
                            "type": "Digital",
                            "exist_item": []
                        }
                    },
                    {
                        "$project": {
                            "_id": 0,
                            "exist_item._id": 0
                        }
                    }
                ]
                coupon_data = common.aggregate_collection_data("coupons", pipeline, amount)
                find_data = coupon_data['find_data']
                return_object['data'] = find_data
                if not find_data:
                    messages['finish'] = "Coupon has been finish!"
                else:
                    for coupon in find_data:
                        list_object = {
                            "coupon_number": coupon['coupon_number'],
                            "status": 0,
                            "time": time,
                            "user_id": user_id
                        }
                        my_list.append(list_object)

        except Exception as e:
            errors['buy_coupon_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    # pass
                    if my_list:
                        # Charge of coupons
                        cost_amount_info = common.last_update("cost_per_coupon")
                        if not cost_amount_info:
                            messages['coupon_cost'] = "Coupon cost not set"
                            return return_object
                        else:
                            cost_per_coupon = cost_amount_info['amount']
                            total_cost = cost_per_coupon * amount
                            tree_model = Tree()
                            tree_collection = tree_model.collection_name()
                            check_query = {"user_id": user_id, "account_balance": {"$gte": total_cost}}
                            check_user_balance = common.collection_data_one("tree", check_query)
                            if not check_user_balance:
                                messages['low_balance'] = "Insufficient balance"
                                return return_object
                            try:
                                # update user balance
                                negative_balance = total_cost - (total_cost + total_cost)
                                where = {
                                    "user_id": user_id
                                }
                                which = {
                                    "$inc": {
                                        "account_balance": negative_balance
                                    }
                                }
                                tree_collection.update_one(where, which)
                            except:
                                errors['balance_update'] = "User account balance not updated"
                                errors['error_info'] = str(common.error_info())
                            else:
                                send_data = collection.insert_many(my_list)

                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['buy_coupon_message'] = 'Coupon purchase successfully'
        return return_object

    def add_brand(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        request_object = {}
        try:
            common = self.common
            model = Brand()
            collection = model.collection_name()
            this_request = request.POST
            store_id = this_request['store_id']
            if not store_id:
                messages['empty'] = "Select a store first"
            else:
                request_object = {
                    "store_id": store_id,
                    "active": 1,
                    "time": time
                }
                compare = model.compare(request_object)
                if not compare:
                    errors['compare'] = "Compare failed"
        except Exception as e:
            errors['add_brand_exception'] = str(e)
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                try:
                    insert = collection.insert_one(request_object)
                except Exception as e:
                    errors['data_insert'] = "Data not sent. Estimated cause database connection error"
                    errors['date_insert_exception'] = str(e)
                else:
                    return_object['status'] = 1
                    messages['brand_add_message'] = 'Brand added'
        return return_object