let OmbazarFn = {};
OmbazarFn.countries = [];
OmbazarFn.categories = [];
OmbazarFn.access_menus = [];
OmbazarFn.selected_country = {};
OmbazarFn.logged_user = {};
OmbazarFn.id_types = {};

OmbazarFn.currency_symbol = "$";
OmbazarFn.custom_filter = function(obj,key_name,value,list=false,negetive=false){
    let find_item=obj.filter(function(item){
            if (negetive) {
                return item[key_name]!==value;
            }
            else{
                return item[key_name]===value;
            }

        });
    if(list){
        return find_item;
    }
    else{
        for(let x in find_item){
            return find_item[x];
        }
    }
};




OmbazarFn.not_found_message = function(message="Not found"){
    var html="<div class='md-card'>";
        html+="<div class='md-card-content'>";
        html+="<h2 class='not-found-message'>"+message+"</h2>";
        html+="</div>";
        html+="</div>";
    return html;

};
OmbazarFn.common_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Common request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"asset"
        };
    }
    return extra_data;

};
OmbazarFn.retrive_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Retrieve request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"get"
        };
    }
    return extra_data;

};

OmbazarFn.passed_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Passed request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"post"
        };
    }
    return extra_data;

};
OmbazarFn.modify_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Modify request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"update"
        };
    }
    return extra_data;

};


//processing image on
OmbazarFn.loading_on = function(){
    altair_helpers.content_preloader_show('regular');
};
OmbazarFn.loading_off = function (){
    altair_helpers.content_preloader_hide();
};
OmbazarFn.small_loading = function(elem,this_switch = 1,result="",size=24){
    if (this_switch){
        let html = "";
        html = "<img src='/base/OmbazarBackEnd/static/OmbazarBackEnd/assets/img/spinners/spinner_small.gif' width='"+size+"' height='"+size+"'>";
        $(elem).html(html).addClass("uk-disabled")
    }
    else{
        $(elem).html(result).removeClass("uk-disabled");
    }
};

OmbazarFn.clean = function(form_id) {
    document.getElementById(form_id).reset();
};
OmbazarFn.modal_close = function () {
    $(".uk-modal-close").click();
};

OmbazarFn.redirect = function (page_name) {
    window.location=path+page_name;
};
OmbazarFn.notify = function (message='Changed has been saved',pos="top-center",timeout="5000") {
    UIkit.notify({
        message: message,
        status:  '',
        timeout: timeout,
        group: null,
        pos: pos,
        onClose: function() {

        }
    });

};

OmbazarFn.have = function (element){
    if($(element).length>0){
        return true;
    }
    else{
        return false;
    }
};
OmbazarFn.response = function (data) {
    UIkit.modal.info(data);
};
OmbazarFn.get_form_data = function (form_name) {
    var form_data=new FormData($(form_name)[0]);
    return form_data;

};


// using jQuery
OmbazarFn.getCookie = function (name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
OmbazarFn.csrftoken = OmbazarFn.getCookie('csrftoken');

OmbazarFn.request = function (form_name,event,loading=true,extra_data=[],extra_images=[],fancy_tree=[]){
    var form_data=OmbazarFn.get_form_data(form_name);
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){    $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);

        }
        form_data=OmbazarFn.get_form_data(form_name);
    }
    for(item in extra_data){
        var field_name=extra_data[item].name;
        var value=extra_data[item].value;
        form_data.append(field_name,value);

    }
    let token = form_data.get("csrfmiddlewaretoken");
    if (!token){
        if(form_name === ""){
            form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
        }
        else {
            // form_data.append("csrfmiddlewaretoken",document.getElementsByName('csrfmiddlewaretoken')[0].value);
        }

    }

    if(loading){
       OmbazarFn.loading_on();
       }

    $.ajax({
        url:"/control/request/",
        data:form_data,
        type:"POST",
        processData: false,
        contentType: false,
        // dataType:"json",
        success:function(data,status){
            if (loading) {
                OmbazarFn.loading_off();
            }
            event(data,status);
        },
        complete:function (xhr,status) {
            if(status != "success"){
                let error=xhr.responseText;
                console.log(error);
            }
        }

    });
};

OmbazarFn.form_data_maker = function (form_element_name,extra_data={},extra_images=[],fancy_tree=[]) {
    // form name format like ("element")/ like jquery selector
    var form_data=OmbazarFn.get_form_data(form_element_name);

    // Image an array []
    //format is array = [file_input_id_name]
    //example <input type='file' id='image'> array key is (image)
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }

    // tree make follow the fancytree rules
    //array format [{},{}]
    // tree = [
    //      {
    //         element_name:".categories.fancytree_radio" ,
    //         selected_name:"category" ,
    //         active_name:"parent"
    //      }
    //  ];
    //
    //
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){
            $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);
        }
        form_data=OmbazarFn.get_form_data(form_element_name);
    }
    // extra data followed by object = {name:"xxxx",age:"xxx",other:"xxxx"}
    for(var item in extra_data){
        var field_name= item;
        var value=extra_data[item];
        form_data.append(field_name,value);
    }
    //csrf token for python security
    // form_data.append("csrfmiddlewaretoken",OmbazarFn.csrftoken);
    // for(x of form_data.values()){
    //     console.log(x);
    // }
    return form_data;

};

//// angular js support
OmbazarFn.active_button = function (){
    return $("button[active='true']").attr('data-button-name');
};

OmbazarFn.add_action = function(response,$location,url=""){
     status = response.status;
    if (status){
        ///First notify a message top bar
        let message = response.message;
            message = OmbazarFn.notify_message(message);
        let active_button = OmbazarFn.active_button();
        // If button name save_and_new form data reset/clean

        if(active_button === 'save_and_new'){
            OmbazarFn.clean("form");
            OmbazarFn.update_inputs();
        }
        // If button name save, page redirect to profile page
        else{
            if (url !== ""){
                $location.path(url);
            }
            else{
                OmbazarFn.clean("form");
                OmbazarFn.update_inputs();
            }

        }
    }
};

OmbazarFn.informer = function(data){
    if(data.status !== undefined) {
        // if (!data.status) {
            let messages = data.message;
            let errors = data.error;
            let m_length = Object.keys(messages).length;
            let e_length = Object.keys(errors).length;
            let info_html = "";
            // If message available
            if(!data.status){
                if (m_length) {
                    let message_html = "<h3>Message</h3>";
                    for (let message in messages) {
                        message_html += "<p>" + messages[message] + "</p>";
                    }
                    info_html += message_html;
                }
            }
            // If error available
            if (e_length) {
                let error_html = "<h3>Error</h3>";
                for (let error in errors) {
                    error_html += "<p>" + errors[error] + "</p>";
                }
                info_html += error_html;
                console.log(data)
            }
            if(info_html){
                OmbazarFn.response(info_html);
            }


        // }

    }
};


OmbazarFn.$http = function($http,data,event,loading = true){
    if(loading){
        OmbazarFn.loading_on();
    }
    // When any form submit first disabled all button in form element
    $("form button").addClass("uk-disabled");
    $http({
        method : "POST",
        url : "/control/request/",
        data : data,
        headers: { 'Content-Type': undefined}
    }).then(
        function (response) {
            let data = response.data;
            let status = response.status;
            if (loading){
                OmbazarFn.loading_off();
            }

            OmbazarFn.informer(data);
            event(data,status);
            $("form button").removeClass("uk-disabled");
        },
        function(response) {
            if (loading){
                OmbazarFn.loading_off();
            }
              console.log(response);
            $("form button").removeClass("uk-disabled");
        })
};

OmbazarFn.notify_message = function(messages){
    let m_length = Object.keys(messages).length;
    let info_html = "";
    // If message available
    if(m_length){
        let message_html = '';
        for (let message in messages) {
            message_html += "<p>"+messages[message]+"</p>";
        }
        info_html += message_html;
        OmbazarFn.notify(info_html,"top-center",5000)
    }
};
OmbazarFn.update_inputs = function(){
    $("input,.md-input").each(function () {
        altair_md.update_input($(this));
    });

};
OmbazarFn.init_requirement = function(){
    altair_md.inputs();
    altair_forms.switches();
    altair_md.checkbox_radio();
    altair_md.fab_toolbar();
    OmbazarFn.update_inputs();
    OmbazarFn.dropify();
};

OmbazarFn.dropify = function(options={}){
  $('.dropify').dropify(options);
};



OmbazarFn.drug_drop_upload = function(params,event,options={}) {
    var progressbar = $("#file_upload-progressbar"),
        bar         = progressbar.find('.uk-progress-bar'),
        settings    = {
            action: '/control/request/', // Target url for the upload
            allow : '*.(json)', // File filter  format like jpg|jpeg|gif|png
            params : params,
            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");

            },
            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },
            allcomplete: function(response,xhr) {

                bar.css("width", "100%").text("100%");
                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);
                setTimeout(function() {
                    UIkit.notify({
                        message: "Upload Completed",
                        pos: 'top-right'
                    });
                },280);
                var data = JSON.parse(response);
                OmbazarFn.informer(data);
                event(data);
            }
        };
        $.extend(settings,options);

    var select = UIkit.uploadSelect($("#file_upload-select"), settings),
        drop   = UIkit.uploadDrop($("#file_upload-drop"), settings);
};

OmbazarFn.find_index = function (docs,key,value) {
  let index_number=docs.findIndex(x => x[key] === value);
  return index_number;
};

OmbazarFn.menu_maker = function (parent_id,main_menus){
    let menu_list = [];
    let menus = OmbazarFn.custom_filter(main_menus,"parent_id",parent_id,true);
    for (let x in menus){
        let item = menus[x];
        let menu_id = item.id;
        item['menus'] = OmbazarFn.menu_maker(menu_id,main_menus);
        menu_list.push(item);
    }
    return menu_list;
};

OmbazarFn.category_maker = function (parent_id,main_categories){
    let category_list = [];
    let categories = OmbazarFn.custom_filter(main_categories,"parent_id",parent_id,true);
    for (let x in categories){
        let item = categories[x];
        let category_id = item.category_id;
        item['categories'] = OmbazarFn.category_maker(category_id,main_categories);
        category_list.push(item);
    }
    return category_list;
};



OmbazarFn.tags_maker = function(element,options = {}){
    $(element).not("input.selectized,.multi.tags").each(function () {
        let default_options = {
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    });

            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
        };
        $.extend(default_options,options);
        $(this).selectize(default_options);
    });
};
OmbazarFn.custom_selectize = function(element,options = {},element_type=false /* just name or html object*/){
    if(element_type){
        var available=OmbazarFn.have(element,true);
    }
    else{
        var available=OmbazarFn.have(element);
    }
    if(available){
        if(element_type){
            var this_elem=element;
        }
        else{
            var this_elem=$(element);
        }
        this_elem.not("select.selectized,.plugin-remove_button").each(function () {
            let default_options = {
                plugins: {
                    'remove_button': {
                        label: ''
                    }
                },
                valueField: 'value',
                labelField: 'label',
                searchField: ['label','value','mobile'],
                hideSelected: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({'margin-top':'0'})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });

                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({'margin-top':''})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },

            };
            $.extend(default_options,options);
            $(this).selectize(default_options);
        });
    }
    else{
        console.log("Not exist select element!");
    }
};



//for add item category an spcifications manage

OmbazarFn.get_access_category = function ($http,event) {
    if(OmbazarFn.categories.length){
        let categories = OmbazarFn.categories;
        event(categories);
    }
    else{
        OmbazarFn.get_categories($http,function (categories) {
            event(categories);
        })
    }
};
//this for add category parent category choser
OmbazarFn.access_category_init = function ($http, $scope, $timeout, event=function () {}) {
    OmbazarFn.get_access_category($http,function (categories) {
        OmbazarFn.categories = categories;
        let parent_categories = OmbazarFn.custom_filter(categories,"parent_id",'0',true);
        let category_list = [];
        for (let x in parent_categories){
            let item = parent_categories[x];
            let category_id = item.category_id;
            let new_categories = OmbazarFn.category_maker(category_id,categories);
            // if(new_categories.length){
                item['categories'] = new_categories;
                category_list.push(item);
            // }
        }
        // OmbazarFn.categories = categories;
        $scope.categories = category_list;
        $timeout(function () {
            altair_tree.tree_radio({
                init: function () {
                    event(this);
                }
            });
        });
    });
};

OmbazarFn.get_countries = function ($http,event) {
    if(OmbazarFn.countries.length){
        let categories = OmbazarFn.countries;
        event(categories);
    }
    else{
        let function_name = "countries()";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);

        OmbazarFn.$http($http,form_data,function (response) {
            event(response.find_data);
        })
    }
};

OmbazarFn.countries_init = function ($http,event=function () {}) {
    OmbazarFn.get_countries($http,function (countries) {
        OmbazarFn.countries = countries;
        OmbazarFn.custom_selectize(".country-code-selector",{
            onInitialize:function () {
                for (let x in countries){
                    let item = countries[x];
                    let country_code = item.callingCodes[0];
                    let label = item.alpha2Code+" ("+country_code+")";
                    let value = country_code;
                    let opt_ob = {label: label, value: value};
                    this.addOption(opt_ob);
                    countries[x].calling_code = country_code;
                }
                event(this);
            },
            onChange: function (item) {
                let country = OmbazarFn.custom_filter(countries,'calling_code',item);
                let country_name_field = $(".own-country-name");
                let nationality = {
                    country_name: country.name,
                    calling_code: country.calling_code,
                    region: country.region,
                    flag: country.flag
                };
                OmbazarFn.selected_country = nationality;
                country_name_field.val(country.name);
                altair_md.update_input(country_name_field);
            }

        });
    });

};

OmbazarFn.get_access_menus = function($http,$routeParams,event=function () {}){
    if(OmbazarFn.access_menus.length){
        let access_menus = OmbazarFn.access_menus;
        event(access_menus);
    }
    else{
        let controller_type = $routeParams.controller_type;
        let function_name = "access_menus('"+controller_type+"')";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);

        OmbazarFn.$http($http,form_data,function (response) {
            event(response.find_data);
        })
    }
};

OmbazarFn.access_menu_init = function($http,$routeParams,$scope,$timeout,event=function () {}){
    OmbazarFn.get_access_menus($http,$routeParams,function (access_menus) {
        // OmbazarFn.access_menus = access_menus;
        $scope.list = access_menus;
        $timeout(function () {
            altair_tree.tree_checkbox({
                init: function () {
                    event(this);
                }
            });

        });
    });

};

OmbazarFn.sp_selectize_init = function(list=[],service){
    $(".tags").each(function () {
        OmbazarFn.tags_maker(this,{
            create:false
        });
    });
    OmbazarFn.custom_selectize(".specification_selectize",{
        placeholder:"Select specification",
        onInitialize: function () {
            for (let x in list){
                this.addOption(list[x]);
            }
        },
        onChange: function (item) {
            let categories = OmbazarFn.categories;
            let elem = $(this)[0].$input;
            let cat_id = $(".category-selector").val();
            let specifications = OmbazarFn.custom_filter(categories,"category_id",cat_id).specifications;
            let values = specifications[item];
            let tag = elem.closest("#d_form_row").find("input.tags");
            let new_instance = tag.get(0).selectize;
                new_instance.clearOptions();
            let tag_list = values.split(",");

            for(let x in tag_list){
                let ob = {
                    text:tag_list[x],
                    value:tag_list[x]
                };
                new_instance.addOption(ob);
            }
        }
    });
};

OmbazarFn.item_category_init = function($http,event=function () {}, sp = true){
  OmbazarFn.get_categories($http,function (categories) {

      OmbazarFn.categories = categories;
      if (sp){
          OmbazarFn.sp_selectize_init();
      }

        OmbazarFn.custom_selectize(".category-selector",{
            onInitialize: function () {
                for (let x in categories){
                    let item = categories[x];
                    let label = item.category_name;
                    let value = item.category_id;
                    let opt_ob = {label: label, value: value};
                    this.addOption(opt_ob)
                }
                event(this);
            },
            onChange: function (item) {
                if (sp){
                    let sp_selectize = $(".specification_selectize").get(0).selectize;
                    sp_selectize.clearOptions();
                    $(".btnSectionRemove").closest("#d_form_row").remove();
                    let specifications = OmbazarFn.custom_filter(categories,"category_id",item).specifications;
                    let list = [];
                    for (let sp in specifications){
                        let ob = {
                            label:sp,
                            value:sp
                        };
                        sp_selectize.addOption(ob);
                        list.push(ob);
                    }
                    OmbazarFn.sp_list = OmbazarFn.get_specifications(item);
                }

            }
        });
        altair_forms.sp_dynamic_fields();
  })
};
OmbazarFn.get_specifications = function(category_id){
    let categories = OmbazarFn.categories;
    let specifications = OmbazarFn.custom_filter(categories,"category_id",category_id).specifications;
    let list = [];
    for (let sp in specifications){
        let ob = {
            label:sp,
            value:sp
        };
        list.push(ob);

    }
    OmbazarFn.sp_list = list;
    return list;
};
//this for barcode generator
OmbazarFn.bar_code_selectize_init = function (barcodes,$scope) {
    OmbazarFn.custom_selectize(".bar-code-selectize",{
        onInitialize: function () {
            for (let x in barcodes){
                let item = barcodes[x];
                let label = item.name;
                let value = item.label;
                let opt_ob = {label: label, value: value};
                this.addOption(opt_ob)
            }
        },
        onChange: function (item) {
            let info = OmbazarFn.custom_filter(barcodes,"label",item);
            let details = info.description;
                details += "\n Support:"+info.support;
                details += "\n Length:"+info.length;
                $scope.$apply(function () {
                    $scope.details = details;
                });

        }
    });
    OmbazarFn.bar_codes = barcodes;
};
OmbazarFn.get_categories = function ($http,event) {
    let function_name = "categories()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        let categories = response.find_data;
       event(categories);
    });
};


OmbazarFn.data_load = function ($http, $scope, $rootScope, $timeout,event) {
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);
        $rootScope.rows = [];

    let skip = ($scope.page-1)*$scope.limit;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "";
            if ($scope.custom_function !== undefined){
                method = $scope.custom_function.function_name;
                let params = $scope.custom_function.parameteres;
                let params_join = params.join();
                if (params.length){
                    params_join += ",";
                }
                method +=  "("+params_join+"'"+$scope.limit+"','"+skip+"')";
            }
            else{
                method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + $scope.limit + "','" + skip + "')";
            }

        let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
        let form_data = OmbazarFn.form_data_maker("", extra_data);
        OmbazarFn.$http($http, form_data, function (response, status) {
            console.log(response);
            let finds = response.find_data;
            $rootScope.rows = finds;
            $timeout(function () {
                if (finds.length) {
                    $scope.filtered = $scope.limit * $scope.page;
                    $scope.start_row = $scope.filtered - $scope.limit;
                    altair_md.checkbox_radio();
                    $rootScope.require();
                    if (!$("#ts_pager_filter[role]").length) {
                        altair_tablesorter.pager_filter_example();
                        OmbazarFn.custom_selectize(".ts_gotoPage",{
                            create: function(input) {
                                return {
                                    value: input,
                                    label: input
                                }
                            },
                        });
                        OmbazarFn.custom_selectize(".pagesize");
                    }
                    $("#ts_pager_filter").trigger("update");

                    let page_instance = $(".ts_gotoPage").get(0).selectize;
                    page_instance.setValue($scope.page);
                    page_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.page = value;
                            // $scope.change();
                        })
                    });
                    let limit_instance = $(".pagesize").get(0).selectize;
                    limit_instance.setValue($scope.limit);
                    limit_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.limit = value;
                            // $scope.change();
                        })
                    });
                }

            });
            event(response, status);
        });
    }
    else if(skip <= 0){
        $scope.prev_switch = 0;
        OmbazarFn.custom_selectize(".ts_gotoPage");
        OmbazarFn.custom_selectize(".pagesize");
    }
    else if(skip >= $scope.total){
        $scope.next_switch = 0;
        OmbazarFn.custom_selectize(".ts_gotoPage");
        OmbazarFn.custom_selectize(".pagesize");
    }

};

OmbazarFn.export_data = function ($http, $scope, $rootScope, $timeout,event,limit=0,skip=0) {
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);

    let export_method = "";
    if ($scope.custom_function !== undefined){
        export_method = $scope.custom_function.function_name;
        let params = $scope.custom_function.parameteres;
        let params_join = params.join();
        if (params.length){
            params_join += ",";
        }
        export_method +=  "("+params_join+"'"+limit+"','"+skip+"')";
    }
    else{
        export_method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + limit + "','" + skip + "')";
    }
    let method = "export_excel(request)";
    let extra_data = OmbazarFn.retrive_request_object(method);
        extra_data['query'] = query;
        extra_data['export_method'] = export_method;
    let form_data = OmbazarFn.form_data_maker("", extra_data);

    OmbazarFn.$http($http,form_data,function (response,status) {
        event(response,status);
        if (response.status){
            let download_info = response.download;

            let pathoffile = download_info.view_path;
            let filename = download_info.file_name;
            $http.get(pathoffile, {
                responseType: "arraybuffer"
            }).then(function (response) {
                $scope.filedata = response.data;
                var headers = response.headers();
                headers['Content-Disposition'] = "attachment";
                var blob = new Blob([response.data], { type: "octet/stream" });
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = filename;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            });
        }


    })
};


OmbazarFn.total_counter = function($http, $scope,event){
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
        query = JSON.stringify(query);
    let method = "";
            if ($scope.custom_function !== undefined){
                method = $scope.custom_function.function_name;
                let params = $scope.custom_function.parameteres;
                let params_join = params.join();
                if (params.length){
                    params_join += ",";
                }
                method +=  "("+params_join+"'0','0',True)";
            }
            else{
                method = "documents_counter('"+collection_name+"',"+query+")";
            }

    let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(response);
        event(response,status);
    });
};
OmbazarFn.activate = function ($http,$rootScope,activate_id,event = function () {}) {
    let function_name = "activate(request,'"+activate_id+"')";
    let extra_data = OmbazarFn.common_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    
    OmbazarFn.$http($http,form_data,function (response,status) {
        event(response,status);
    });
};

OmbazarFn.request_data_load = function ($http, $scope, $rootScope, $timeout,event) {

    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);
    let skip = $scope.skip;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "";
        if ($scope.custom_function !== undefined) {
            method = $scope.custom_function.function_name;
            let params = $scope.custom_function.parameteres;
            let params_join = params.join();
            if (params.length) {
                params_join += ",";
            }
            method += "(" + params_join + "'" + $scope.limit + "','" + skip + "')";
        }
        else{
            method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + $scope.limit + "','" + skip + "')";
        }
        let extra_data = OmbazarFn.retrive_request_object(method);
            extra_data['query'] = query;
            extra_data['option'] = option;
        let form_data = OmbazarFn.form_data_maker("", extra_data);
        OmbazarFn.$http($http, form_data, function (response, status) {
            let finds = response.find_data;
            if (finds.length) {
                for (let x in finds) {
                    let item = finds[x];
                    $scope.rows.push(item);
                }
                $scope.skip = $scope.skip+finds.length;
            }
            $timeout(function () {
                event(response, status);
            });

        });
    }
};

OmbazarFn.get_basic_info = function ($rootScope,service,event=function () {}) {
    let basic_info = {};
    if(Object.keys($rootScope.basic_info).length){
        basic_info = $rootScope.basic_info;
        event(basic_info);
    }
    else{
        $rootScope.$on("set_basic_info",function () {
            basic_info = service.get_basic_info();
            event(basic_info);
        });

    }

};

OmbazarFn.retrieve_data = function ($http, collection_name, query,event,option={},function_name=false,parameteres = []) {
    query = JSON.stringify(query);
    option = JSON.stringify(option);
    let method = "";
    if (function_name) {
        method = collection_name;
        let params = parameteres;
        let params_join = params.join();
        method += "(" + params_join + ")";
    }
    else{
        method = "request_collection_data('" + collection_name + "'," + query + "," + option + ")";
    }
    // console.log(method);
    let extra_data = OmbazarFn.retrive_request_object(method);
        extra_data['query'] = query;
    let form_data = OmbazarFn.form_data_maker("", extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        event(response,status);
    })
};



OmbazarFn.add_to_cart = function ($rootScope,$scope, store_item_id, amount = 1,replace = false) {
    let item = Cookies.getJSON(store_item_id);
        amount = Number(amount);
    if (item){
        let new_amount = 0;
        if (replace){
            new_amount = amount;
        }
        else{
            new_amount = amount+item.quantity;
        }

        Cookies.set(store_item_id,{
            "quantity": new_amount
        });
        let index = OmbazarFn.index_number($scope.cart,"store_item_id",store_item_id);
        $scope.cart[index].quantity = new_amount;
        let before_info = $scope.cart[index];
        let point = OmbazarFn.point_maker($rootScope,before_info);
        $scope.cart[index].point = point;
    }
    else{
        let item_info = OmbazarFn.custom_filter($scope.rows,"store_item_id",store_item_id);
            // item_info.quantity = amount;
        Cookies.set(store_item_id,{
            "quantity": amount
        });
        let new_object = {
            "price": item_info.price,
            "sale": item_info.sale,
            "quantity": amount,
            "purchase": item_info.purchase,
            "item_id": item_info.item_id,
            "store_id": item_info.store_id,
            "store_item_id": item_info.store_item_id,
            "vat": item_info.vat,
            "vendor_id": item_info.vendor_id,
            "category_id": item_info.category_id,
            "description": item_info.description,
            "images": item_info.images,
            "short_description": item_info.short_description,
            "item_name": item_info.item_name
        };
        let point = OmbazarFn.point_maker($rootScope,new_object);
        new_object.point = point;
        $scope.cart.push(new_object);
    }
    $scope.calculate();

    // console.log(index);

};

OmbazarFn.merge = function (target_object, new_object) {
    return Object.assign(target_object, new_object);
};
OmbazarFn.index_number = function(obj,key_name,value){
    return obj.map(function(e) { return e[key_name]; }).indexOf(value);
};
OmbazarFn.order_info_filter = function (order,$rootScope) {
    let order_items = order.order_items;
    let items = [];
    for(let x in order_items){
        let order_item_info = order_items[x].item_info;
            order_item_info.store_item_id = order_items[x].store_item_id;
        let item_id = order_item_info.item_id;
        let item_info = OmbazarFn.custom_filter(order.items,"item_id",item_id);
        let merge = OmbazarFn.merge(item_info,order_item_info);
        items.push(merge)
    }
    order.order_items = items;
    let order_type = order.order_type;
    if (order_type !== "pos") {
        let activate_store_id = $rootScope.basic_info.store_id;
        let store_total_info = order.total_info.stores_total[activate_store_id];
        if (store_total_info!== undefined){
            order.total_info = store_total_info;
        }

    }
    return order;
};

OmbazarFn.order_total_info = function (items, extra_discount = 0) {
    let sub_total = 0;
    let total = 0;
    let vat = 0;
    let point = 0;
    let discounts = [];

    let grand_total = 0;
    let total_item = items.length;
    for (let x in items){
        let item = items[x];
        let price = item.price;
        let sale = item.sale;
        let this_vat = item.vat;
        let line_point = item.point;
        let quantity = item.quantity;

        //for gather rice grand total
        let line_total = 0;
        if (item.sale){
            line_total = item.sale*item.quantity;
        }
        else{
            line_total = item.price*item.quantity;
        }
        point += quantity*line_point;
        let line_vat = line_total*(this_vat/100);
        vat += line_vat;
        sub_total += line_total;

        if (item.sale){
            let price_line_total = item.price*item.quantity;
            let price_line_vat = price_line_total*(this_vat/100);
            let this_discount = (price_line_total+price_line_vat)-(line_total+line_vat);
            let item_in_ob = {
                "item_name": item.item_name,
                "discount": this_discount,
            };
            discounts.push(item_in_ob);
        }
    }
    total = sub_total + vat - (extra_discount);
    let object = {
        grand_total: total,
        extra_discount: extra_discount,
        sub_total: sub_total,
        vat: vat,
        point: point,
    };
    let total_item_discount = 0;
    for (let x in discounts){
        total_item_discount += discounts[x].discount;
    }
    object.total_discount = total_item_discount + extra_discount;
    object.total_item = total_item;
    object.discounts = discounts;

    return object;
};
OmbazarFn.order_status_selectize = function (default_value=0,disabled=0, event=function () {}) {
    let order_status_list = [

        // {
        //     label: "Processing",
        //     value: "Processing",
        // },
        {
            label: "Completed",
            value: "Completed",
        },
        {
            label: "Pending",
            value: "Pending",
        },
        // {
        //     label: "Cancelled",
        //     value: "Cancelled",
        // },

        ];
    if (disabled){
        $(".order-status").addClass("uk-disabled");
    }

    OmbazarFn.custom_selectize(".order-status",{
        options: order_status_list,
        onInitialize:function () {
            if (default_value){
                this.setValue(default_value);
            }
            event(this);
        }
    });
};


OmbazarFn.altair_init = function () {
    // page onload functions
        altair_page_onload.init();

        // main header
        altair_main_header.init();

        // main sidebar
        // altair_main_sidebar.init(true);
        // secondary sidebar
        altair_secondary_sidebar.init();

        // top bar
        altair_top_bar.init();

        // page heading
        altair_page_heading.init();

        // material design
        altair_md.init();

        // forms
        altair_forms.init();

        // truncate text helper
        altair_helpers.truncate_text($('.truncate-text'));

        // full screen
        altair_helpers.full_screen();

        // table check
        altair_helpers.table_check();

        // print page
        altair_helpers.print_page();
};
OmbazarFn.id_info = function (user_id, info_name) {
    if (user_id !== undefined){
        let type = user_id.substr(0,3);
        let sl = user_id.substr(3,15);
        let option = user_id.substr(18,18);
        let arr = {"type": type, "sl": sl, "option": option};
        let result = arr[info_name];
        return result;
    }
    else{
        console.log("undefined user id for get id info")
    }
};

OmbazarFn.id_type_name = function (user_id) {
    let id_type = OmbazarFn.id_info(user_id, 'type');
    let reverse_id_types = OmbazarFn.object_reverse(OmbazarFn.id_types);
    let type_name = reverse_id_types[id_type];
    return type_name;
};
OmbazarFn.object_reverse = function (object) {
    let reverse_object = {};
    for( let x in object){
        let key = x;
        let value = object[x];
        reverse_object[value] = key;
    }
    return reverse_object;
};

OmbazarFn.point_maker = function($rootScope,object,quantity=0) {

      let point = 0;
      let item = object;
      let sale = item.sale;
      let price = item.price;
      let purchase = item.purchase;
      let rules = $rootScope.basic_info.point_rules;
      let sale_rule = OmbazarFn.custom_filter(rules,"id","1");

      if (sale_rule !== undefined){
            let rule_point = sale_rule.amount;
            let exact_price = 0;
            if (sale){
                exact_price = sale;
            }
            else{
                exact_price = price;
            }
            if (quantity){
                quantity  = 1;
            }
            else{
                quantity = item.quantity;
            }
            let profit = exact_price*quantity - purchase*quantity;
            point = profit * rule_point;
      }
      else{
          point = 0
      }
      return point;

  };
OmbazarFn.modal = function ($interpolate,$scope,element,event) {
    let content = $(element).html();
        let result = $interpolate(content)($scope);
        OmbazarFn.response(result);
        event();
};
OmbazarFn.form_submit = function (form_name,event) {
  $(document).on("submit",form_name,function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      event(this,e);
  });
};

OmbazarFn.document_update = function ($http,collection_name,where,which,event) {
  let function_name = "document_update(request)";
    let extra_data = OmbazarFn.modify_request_object(function_name);
    let update_object = {
        collection_name: collection_name,
        which:which,
        where:where
    };
    extra_data['update_document'] = JSON.stringify(update_object);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        event(response,status);
    });
};
// OmbazarFn.sale_point_rule = function ($rootScope) {
//     let amount = 0
//     let point_rules = $rootScope.basic_info.point_rules;
//     if (point_rules !== undefined){
//         let sale_point_rule = OmbazarFn.custom_filter()
//     }
//     return amount;
// };

OmbazarFn.vendor_id = function ($rootScope) {
    let vendor_id = 0;
    let logged_user_id = $rootScope.basic_info.logged_user_id;
    if (logged_user_id !== undefined){
        let id_type = OmbazarFn.id_type_name(logged_user_id);
        if (id_type === "admin" || id_type === "employee"){
            let active_vendor_id = $rootScope.basic_info.vendor_id;
            if(active_vendor_id !== undefined){
                vendor_id = active_vendor_id;
            }
        }
        else if(id_type === "vendor"){
            vendor_id = logged_user_id;
        }

    }
    return vendor_id;
};
OmbazarFn.logged_user_type = function ($rootScope) {
    let user_type = null;
    let logged_user_id = $rootScope.basic_info.logged_user_id;
    if (logged_user_id !== undefined && logged_user_id){
        let id_type = OmbazarFn.id_type_name(logged_user_id);
        user_type = id_type;
    }
    return user_type;
};

