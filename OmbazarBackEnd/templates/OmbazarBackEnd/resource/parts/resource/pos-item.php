<?php require_once "support.php"?>
{{#each rows}}
<div data-product-name="{{item_name}}" class="row_id_{{id}} pos-item" data-id="{{id}}" data-item-id="{{item_id}}" data-object="">
    <div class="md-card md-card-hover-img">
        <div class="md-card-head uk-text-center uk-position-relative">
            <div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">{{sale}}</div>
            <img class="md-card-head-img" src="{{image_path}}" alt=""/>
        </div>
        <div class="md-card-content">
            <h4 class="heading_c uk-margin-bottom">
                {{item_name}}
                <span class="sub-heading">Price:
                    {{#if sale}}
                    <del>{{price}} {{currency_symbol}}</del> {{sale}} {{currency_symbol}}
                    {{else}}
                    {{price}} {{currency_symbol}}
                    {{/if}}
                </span>
            </h4>
        </div>
    </div>
</div>
{{/each}}