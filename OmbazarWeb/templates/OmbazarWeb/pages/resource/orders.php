
    <div id="page_content">
        <div id="top_bar">
            <ul id="breadcrumbs">
                <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
                <li><a href="<?php echo SITE_URL;?>orders" class="no_load">Orders</a></li>
                <li><span>List</span></li>
            </ul>
        </div>
        <div id="page_content_inner" class="order-page">
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-margin-bottom">
                        <a href="#" class=" " id="printTable" data-uk-tooltip="{pos:'top'}" title="Print">
                            <i class="material-icons md-icon">print</i>
                        </a>
                        <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" style="margin-top:-8px">
                            <a class="" href="#" data-uk-tooltip="{pos:'top'}" title="Colum select"><i class="material-icons md-icon ">reorder</i></a>
                            <div class="uk-dropdown">
                                <ul class="uk-nav uk-nav-dropdown" id="columnSelector"></ul>
                            </div>
                        </div>


                    </div>
                    <div class="uk-overflow-container uk-margin-bottom">
                        <table class="uk-table uk-table-align-vertical uk-table-nowrap user-uk-table" data-type="user-order" limit="3">
                            <thead>
                            <tr>
                                <th data-priority="critical">Order id</th>
                                <th data-priority="critical">Status</th>
                                <th data-priority="4">Entry date</th>
                                <th class="filter-false remove sorter-false uk-text-center" data-priority="1">Manage</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <ul class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                        <li class=""><a href="#" class="previous-button"><i class="uk-icon-angle-double-left"></i> Previous</a> </li>

                        <li class="uk-text-center"><form class="page-go-form"><label>Page number</label> <input class=" md-input uk-form-width-small uk-text-center selectize-input go-page-number" type="text" value="<?php if(isset($_REQUEST['page_number'])){echo $_REQUEST['page_number'];} else{echo "1";};?>" name="page_number"> </form> </li>

                        <li class=""><a href="#" class="next-button">Next <i class="uk-icon-angle-double-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
