$(function() {
    // init invoices
    offline_altair_invoices.init();
});

// variables
var $offline_invoice_card = $('#invoice'),
    $offline_invoice_preview = $('#invoice_preview'),
    $offline_invoice_form = $('#invoice_form'),
    $offline_invoices_list_main = $('#invoices_list'),
    offline_invoice_list_class = '.invoices_list', // main/sidebar list
    $offline_invoice_add_btn = $('#offline_doc_add'),
    $offline_invoice_edit_btn = $('#offline_invoice_edit');
function doc_name(){
    var doc_name=$(".offline-doc").attr("data-default-document");
    return doc_name;
}
function doc_type(){
    var doc_type=".offline-"+doc_name()+" ";
    return doc_type;
}
offline_altair_invoices  = {
    init: function() {
        // copy list to sidebar
        offline_altair_invoices.copy_list_sidebar();
        // add new invoice
        offline_altair_invoices.add_new();
        // open invoice
        offline_altair_invoices.open_invoice();
        offline_altair_invoices.edit_invoice();
//        // print invoice btn
        offline_altair_invoices.print_invoice();
    },
    add_new: function() {
        if($offline_invoice_add_btn) {

            var insert_form = function() {

                var $offline_invoice_form_template = $(doc_type()+'#offline_invoice_form_template'),
                    card_height = $offline_invoice_card.height(),
                    content_height = $offline_invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $offline_invoice_form_template.html();
                    template_compiled = Handlebars.compile(invoice_form_template_content);

                    var docs=localStorage.getItem(doc_name());
                    if(docs==null){
                        var sl=0;
                    }
                    else{
                        var docs_object=JSON.parse(docs);
                        var sl=docs_object.length;
                    }
                    var context = {
                        "doc_sl": sl+1
                    };
                    compiled_html=template_compiled(context);
                // remove "uk-active" class form invoices list
//                $(offline_invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $offline_invoice_card.height(card_height);

                $offline_invoice_form
                    .hide()
                    // add form to card
                    .html(compiled_html)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_service = function() {

                    var $offline_invoice_form_template_services = $(doc_type()+'#offline_invoice_form_template_services'),
                        $invoice_services = $offline_invoice_form.find('#offline_form_invoice_services');

                    var template = $offline_invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id
                        },
                        theCompiledHtml = template_compiled(context);
                    $invoice_services.append(theCompiledHtml);
                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                    if(doc_name()!="reciept"){
                        //if the document type is invoice or bill
                        var d;
                        function user_data(){
                            return items_obj;
                        }

                        var data=user_data();

                        var options = {
                            sourceData: function(search, success) {
                                dataResponse = [];
                                data.forEach(function(el, i) {
                                    dataResponse.push({
                                        label: el.label,
                                        value: el.value
                                    });
                                });
                                success(dataResponse);
                            },
                            minChar: 3,
                            onClick: function(item,element) {
                                var this_element=$(element).attr("data-item-number");

                                this_item=$(".service-item[data-service-number='"+this_element+"']");
                                var data_id=item.value;

                                var item=custom_filter(items_obj,"value",data_id);
                                var name=item.label;
                                var purchase_price=item.purchase_price;
                                var selling_price=item.selling_price;
                                var vat=item.vat;
                                var image=item.image;

                                this_item.find(".item_name").val(name);
                                if(doc_name()=="invoice"){
                                    this_item.find(".service_price").val(selling_price);
                                }
                                else if(doc_name()=="bill"){
                                    this_item.find(".service_price").val(purchase_price);
                                }

                                this_item.find(".service_vat").val(vat);
                                this_item.find(".item_id").val(data_id);

                                var image_path=path+"assets/img/images/"+image;
                                this_item.find(".invoice-service-thumb").attr("src",image_path);
                                this_item.find(".fancy-box-item").attr("href",image_path);
                                altair_md.update_input($(this_item.find("input")));


                            },
                            onPressEnter: function(item,element) {
                                var this_element=$(element).attr("data-item-number");

                                this_item=$(".service-item[data-service-number='"+this_element+"']");
                                var data_id=item.value;

                                var item=custom_filter(items_obj,"value",data_id);
                                var name=item.label;
                                var purchase_price=item.purchase_price;
                                var selling_price=item.selling_price;
                                var vat=item.vat;
                                var image=item.image;

                                this_item.find(".item_name").val(name);
                                if(doc_name()=="invoice"){
                                    this_item.find(".service_price").val(selling_price);
                                }
                                else if(doc_name()=="bill"){
                                    this_item.find(".service_price").val(purchase_price);
                                }

                                this_item.find(".service_vat").val(vat);
                                this_item.find(".item_id").val(data_id);

                                var image_path=path+"assets/img/images/"+image;
                                this_item.find(".invoice-service-thumb").attr("src",image_path);
                                this_item.find(".fancy-box-item").attr("href",image_path);
                                altair_md.update_input($(this_item.find("input")));

                            }

                        };

                        $('.add_item_field').lightAutocomplete(options);
                        ///end of items auto complete for bill invoice
                    }

                };

                // append first service to invoice form on init
                append_service();

                $('#offline_invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    append_service();
//                    $('.modal_product_items').lightAutocomplete(options)
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show invoice form on animation complete
            var show_form = function() {
                $offline_invoice_card.css({
                    'height': ''
                });
                $offline_invoice_form.show();
                $offline_invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $offline_invoice_add_btn.on('click', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($offline_invoice_card,insert_form,show_form,undefined);
                if(doc_name()!="reciept"){
                    var d;
                    if(doc_name()=="invoice"){
                        var user_type="customer";
                    }
                    else if(doc_name()=="bill"){
                        var user_type="vendor";
                    }
                    function user_data(){
                        var data=custom_object_filter(users_obj,"user_type",user_type);
                        return data;
                    }

                    var data=user_data();
                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item) {
                            $('.to_info').val(item.label);
                            var user_id=item.value;

                            var reciver=custom_filter(data,"row_id",user_id);
                            $(".reciver").val(reciver.user_id);
                            $("#invoice_to_address_1").val(reciver.address_line_1);
                            $("#invoice_to_address_2").val(reciver.address_line_2);
                            $("#to_email").val(reciver.email);
                            $("#to_phone").val(reciver.phone);
                            altair_md.update_input($("#invoice_to_address_1"));
                            altair_md.update_input($("#invoice_to_address_2"));
                            altair_md.update_input($("#to_email"));
                            altair_md.update_input($("#to_phone"));

                        },
                        onPressEnter: function(item) {
                            $('.to_info').val(item.label);
                            var user_id=item.value;

                            var reciver=custom_filter(data,"row_id",user_id);
                            $(".reciver").val(reciver.user_id);
                            $("#invoice_to_address_1").val(reciver.address_line_1);
                            $("#invoice_to_address_2").val(reciver.address_line_2);
                            $("#to_email").val(reciver.email);
                            $("#to_phone").val(reciver.phone);
                            altair_md.update_input($("#invoice_to_address_1"));
                            altair_md.update_input($("#invoice_to_address_2"));
                            altair_md.update_input($("#to_email"));
                            altair_md.update_input($("#to_phone"));

                        }


                    };

                    $('.to_info').lightAutocomplete(options);


                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //invoice form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#offline_invoice_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end invoice form discount control by discount type
                }else{
                    altair_forms.select_elements();
                    altair_form_file_dropify.init();

                    $categories=$(".income_expense_category").selectize({
                        valueField: 'user_id',
                        labelField: 'category',
                        onDropdownOpen: function($dropdown) {
                            $dropdown
                                .hide()
                                .velocity('slideDown', {
                                begin: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top':'0'})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            })
                        },
                        onDropdownClose: function($dropdown) {
                            $dropdown
                                .show()
                                .velocity('slideUp', {
                                complete: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top': ''})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            });
                        }
                    });

                    //income expense category changes
                    $(".income-expense-category input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var type=event.target.value;

                        var control=$categories[0].selectize;
                        control.clearOptions();

                        var data=custom_object_filter(income_expense_obj,"type",type);
                        for(item in data){
                            control.addOption(data[item]);

                        }
                    });
                    altair_form_validation.init();
                }
            })

        }
    },
    edit_invoice: function() {
        if($offline_invoice_edit_btn) {

            var insert_form = function() {

                var $offline_invoice_form_template = $(doc_type()+'#offline_invoice_edit_form_template'),
                    card_height = $offline_invoice_card.height(),
                    content_height = $offline_invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $offline_invoice_form_template.html(),
                    template_compiled = Handlebars.compile(invoice_form_template_content);

                    // ajax function to get invoices
                    var invoice_id = parseInt($("#offline_invoice_edit").attr('offline-data-invoice-id'));
                    var docs=localStorage.getItem(default_doc_name());
                    if(docs!==null){
                        var docs_object=JSON.parse(docs);
                        var data_object=document_object_filter(docs_object,"int_number",invoice_id);
                        var response={};
                        for(x in data_object){
                            var row_data=data_object[x];

                            if(default_doc_name()!="reciept"){
                                var invoice_services=[];
                                var services=row_data.services;
                                var total_amount=0;
                                for(x in services){
                                        var vat=Number(services[x].vat);
                                        var rate=Number(services[x].rate);
                                        var quantity=Number(services[x].quantity);
                                        var persent_amount=(vat/100)*rate*quantity;
                                        var service_total=rate*quantity+persent_amount;
                                        total_amount+=service_total;
                                    var obj={
                                        invoice:row_data.int_number,
                                        service_description:services[x].description,
                                        service_name:services[x].name,
                                        service_quantity:services[x].quantity,
                                        service_rate:services[x].rate,
                                        service_total:service_total,
                                        service_vat:services[x].vat,
                                        service_item_id:services[x].item_id,
                                    };
                                    invoice_services.push(obj);
                                }

                                var discount=row_data.discount;
                                if(row_data.discount_type=="persent"){
                                    discount=total_amount*Number(discount)/100;
                                }
                                var due_date=next_date(row_data.issue_date,row_data.due_date);
                                var fixed=0;
                                var persent=0;
                                if(row_data.discount_type=="fixed"){
                                    fixed=1;
                                }
                                else if(row_data.discount_type=="persent"){
                                    persent=1;
                                }
                                var new_object={
                                    discount:row_data.discount,
                                    fixed:fixed,
                                    persent:persent,
                                    invoice_date:row_data.issue_date,
                                    invoice_due_date:due_date,
                                    invoice_due_day:row_data.due_date,
                                    invoice_from_address_1:company_obj.address_line_1,
                                    invoice_from_address_2:company_obj.address_line_2,
                                    invoice_from_company:company_obj.name,
                                    invoice_id:row_data.int_number,
                                    invoice_number:row_data.int_number,
                                    invoice_services:invoice_services,
                                    invoice_to_address_1:row_data.address_line_1,
                                    invoice_to_address_2:row_data.address_line_2,
                                    invoice_to_company:row_data.name,
                                    invoice_to_email:row_data.email,
                                    invoice_to_phone:row_data.phone,
                                    invoice_status:"Unpaid",
                                    invoice_status_color:"danger",
                                    invoice_total_value:total_amount,
                                    invoice_type:document_name(),
                                    invoice_vat_value:discount,
                                    note:row_data.note,
                                    reciver:row_data.reciver,
                                };

                            }
                            else{
                                var expense_status=0;
                                var income_status=0;
                                if(row_data.type=="income"){
                                    income_status=1;
                                }
                                else{
                                    expense_status=1;
                                }

                                var this_category=custom_filter(income_expense_obj,"user_id",row_data.category);
                                var category_name=this_category.category;
                                var services=row_data.services;
                                var invoice_services=[];
                                var total_amount=0;
                                var sl=1;
                                var x;
                                for(x in services){
                                        var vat=services[x].description;
                                        var service_total=Number(services[x].amount);
                                        total_amount+=service_total;
                                    var obj={
                                        service_id:row_data.int_number,
                                        service_description:services[x].description,
                                        service_sl:sl,
                                        service_total:service_total,
                                    };
                                    invoice_services.push(obj);
                                    sl++;
                                }
                                var this_team_member=custom_filter(team_members_obj,"user_id",row_data.team_member);

                                var new_object={
                                    type:row_data.type,
                                    category:row_data.category,
                                    category_name:category_name,
                                    expense_status:expense_status,
                                    income_status:income_status,
                                    invoice_date:row_data.date,
                                    invoice_id:row_data.int_number,
                                    invoice_number:row_data.int_number,
                                    invoice_services:invoice_services,
                                    invoice_to_company:company_obj.name,
                                    invoice_total_value:total_amount,
                                    note:row_data.note,
                                    team_member:row_data.team_member,
                                    team_member_name:this_team_member.name,
                                };
                            }
                            var response={
                                invoice:new_object
                            };

                        }
                        services=response.invoice.invoice_services;
                        theCompiledHtml = template_compiled(response);
                    }

                // remove "uk-active" class form invoices list
                $(offline_invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $offline_invoice_card.height(card_height);

                $offline_invoice_form
                    .hide()
                    // add form to card
                    .html(theCompiledHtml)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_int_service = function() {

                    var $offline_invoice_form_template_services = $(doc_type()+'#offline_invoice_edit_form_template_services'),
                        $invoice_services = $offline_invoice_form.find('#offline_form_invoice_edit_services');

                    var template = $offline_invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    var d;
                    if(doc_name()!="reciept"){
                        function user_data(){
                            return items_obj;
                        }

                        var data=user_data();

                        var options = {
                            sourceData: function(search, success) {
                                dataResponse = [];
                                data.forEach(function(el, i) {
                                    dataResponse.push({
                                        label: el.label,
                                        value: el.value
                                    });
                                });
                                success(dataResponse);
                            },
                            minChar: 3,
                            onClick: function(item,element) {
                                var this_element=$(element).attr("data-item-number");

                                this_item=$(".service-item[data-service-number='"+this_element+"']");
                                var data_id=item.value;

                                var item=custom_filter(items_obj,"value",data_id);
                                var name=item.label;
                                var purchase_price=item.purchase_price;
                                var selling_price=item.selling_price;
                                var vat=item.vat;
                                var image=item.image;

                                this_item.find(".item_name").val(name);
                                if(doc_name()=="invoice"){
                                    this_item.find(".service_price").val(selling_price);
                                }
                                else if(doc_name()=="bill"){
                                    this_item.find(".service_price").val(purchase_price);
                                }

                                this_item.find(".service_vat").val(vat);
                                this_item.find(".item_id").val(data_id);

                                var image_path=path+"assets/img/images/"+image;
                                this_item.find(".invoice-service-thumb").attr("src",image_path);
                                this_item.find(".fancy-box-item").attr("href",image_path);
                                altair_md.update_input($(this_item.find("input")));

                            },
                            onPressEnter: function(item,element) {
                                var this_element=$(element).attr("data-item-number");

                                this_item=$(".service-item[data-service-number='"+this_element+"']");
                                var data_id=item.value;

                                var item=custom_filter(items_obj,"value",data_id);
                                var name=item.label;
                                var purchase_price=item.purchase_price;
                                var selling_price=item.selling_price;
                                var vat=item.vat;
                                var image=item.image;

                                this_item.find(".item_name").val(name);
                                if(doc_name()=="invoice"){
                                    this_item.find(".service_price").val(selling_price);
                                }
                                else if(doc_name()=="bill"){
                                    this_item.find(".service_price").val(purchase_price);
                                }

                                this_item.find(".service_vat").val(vat);
                                this_item.find(".item_id").val(data_id);

                                var image_path=path+"assets/img/images/"+image;
                                this_item.find(".invoice-service-thumb").attr("src",image_path);
                                this_item.find(".fancy-box-item").attr("href",image_path);
                                altair_md.update_input($(this_item.find("input")));

                            }
                        };

                        $('.add_item_field').lightAutocomplete(options);
                    }
                };

                // append first service to invoice form on init
                append_int_service();

                // append services to invoice form
                var append_service = function() {
                    var $offline_invoice_form_template_services = $(doc_type()+'#offline_invoice_blank_form_template_services'),
                        $invoice_services = $offline_invoice_form.find('#offline_form_invoice_edit_services');

                    var template = $offline_invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);
                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                    var d;
                    function user_data(){
                        return items_obj;
                    }

                    var data=user_data();

                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;

                            var item=custom_filter(items_obj,"value",data_id);
                            var name=item.label;
                            var purchase_price=item.purchase_price;
                            var selling_price=item.selling_price;
                            var vat=item.vat;
                            var image=item.image;

                            this_item.find(".item_name").val(name);
                            if(doc_name()=="invoice"){
                                this_item.find(".service_price").val(selling_price);
                            }
                            else if(doc_name()=="bill"){
                                this_item.find(".service_price").val(purchase_price);
                            }

                            this_item.find(".service_vat").val(vat);
                            this_item.find(".item_id").val(data_id);

                            var image_path=path+"assets/img/images/"+image;
                            this_item.find(".invoice-service-thumb").attr("src",image_path);
                            this_item.find(".fancy-box-item").attr("href",image_path);
                            altair_md.update_input($(this_item.find("input")));


                        },
                        onPressEnter: function(item,element) {
                            var this_element=$(element).attr("data-item-number");
                            this_item=$(".service-item[data-service-number='"+this_element+"']");
                            var data_id=item.value;

                            var item=custom_filter(items_obj,"value",data_id);
                            var name=item.label;
                            var purchase_price=item.purchase_price;
                            var selling_price=item.selling_price;
                            var vat=item.vat;
                            var image=item.image;

                            this_item.find(".item_name").val(name);
                            if(doc_name()=="invoice"){
                                this_item.find(".service_price").val(selling_price);
                            }
                            else if(doc_name()=="bill"){
                                this_item.find(".service_price").val(purchase_price);
                            }

                            this_item.find(".service_vat").val(vat);
                            this_item.find(".item_id").val(data_id);

                            var image_path=path+"assets/img/images/"+image;
                            this_item.find(".invoice-service-thumb").attr("src",image_path);
                            this_item.find(".fancy-box-item").attr("href",image_path);
                            altair_md.update_input($(this_item.find("input")));

                        }
                    };

                    $('.add_item_field').lightAutocomplete(options);
                    return service_id;
                };



                $('#invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    var service_number=append_service();
                    $(".service-item[data-service-number='"+service_number+"']").find("input").val("");
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();
                var va=$(".form_switcher_area select").val();
                $('.form-type').val(va);

            };

            // show invoice form on animation complete
            var show_form = function() {
                $offline_invoice_card.css({
                    'height': ''
                });
                $offline_invoice_form.show();
                $offline_invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $("#invoice_preview").on('click','#offline_invoice_edit', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($offline_invoice_card,insert_form,show_form,undefined);


                if(doc_name()!="reciept"){
                    var d;
                    if(doc_name()=="invoice"){
                        var user_type="customer";
                    }
                    else if(doc_name()=="bill"){
                        var user_type="vendor";
                    }
                    function user_data(){
                        var data=custom_object_filter(users_obj,"user_type",user_type);
                        return data;
                    }

                    var data=user_data();
                    var options = {
                        sourceData: function(search, success) {
                            dataResponse = [];
                            data.forEach(function(el, i) {
                                dataResponse.push({
                                    label: el.label,
                                    value: el.value
                                });
                            });
                            success(dataResponse);
                        },
                        minChar: 3,
                        onClick: function(item) {
                            $('.to_info').val(item.label);
                            var user_id=item.value;

                            var reciver=custom_filter(data,"row_id",user_id);
                            $(".reciver").val(reciver.user_id);
                            $("#invoice_to_address_1").val(reciver.address_line_1);
                            $("#invoice_to_address_2").val(reciver.address_line_2);
                            $("#to_email").val(reciver.email);
                            $("#to_phone").val(reciver.phone);
                            altair_md.update_input($("#invoice_to_address_1"));
                            altair_md.update_input($("#invoice_to_address_2"));
                            altair_md.update_input($("#to_email"));
                            altair_md.update_input($("#to_phone"));

                        },
                        onPressEnter: function(item) {
                            $('.to_info').val(item.label);
                            var user_id=item.value;

                            var reciver=custom_filter(data,"row_id",user_id);
                            $(".reciver").val(reciver.user_id);
                            $("#invoice_to_address_1").val(reciver.address_line_1);
                            $("#invoice_to_address_2").val(reciver.address_line_2);
                            $("#to_email").val(reciver.email);
                            $("#to_phone").val(reciver.phone);
                            altair_md.update_input($("#invoice_to_address_1"));
                            altair_md.update_input($("#invoice_to_address_2"));
                            altair_md.update_input($("#to_email"));
                            altair_md.update_input($("#to_phone"));

                        }


                    };

                    $('.to_info').lightAutocomplete(options);


                altair_form_file_dropify.init();
                $("a#example2").fancybox();
                altair_forms.select_elements();
                //invoice form discount control by discount type
                    $(".discount_type input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var val=event.target.value;

                        total=0;
                        var discount_type=val;
                        $("#offline_invoice_form .service-item").each(function(){
                            total += Number($(".service_total",this).val());
                        });


                        if(discount_type=='fixed'){
                           var discount=Number($(".discount").val());
                           }
                        else if(discount_type=='persent'){
                            var discount_val=Number($(".discount").val());
                            var discount=total*discount_val/100;
                                }
//                        var last_total=total-discount;
//                        $(".all-total").val(last_total);
                        $(".discount_view").html(discount);

//                        altair_md.update_input($(".service_total"));


                    });
                altair_form_validation.init();
                //end invoice form discount control by discount type
                }else{
                    altair_forms.select_elements();
                    altair_form_file_dropify.init();

                    $categories=$(".income_expense_category").selectize({
                        valueField: 'user_id',
                        labelField: 'category',
                        onDropdownOpen: function($dropdown) {
                            $dropdown
                                .hide()
                                .velocity('slideDown', {
                                begin: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top':'0'})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            })
                        },
                        onDropdownClose: function($dropdown) {
                            $dropdown
                                .show()
                                .velocity('slideUp', {
                                complete: function() {
                                    if (typeof thisPosBottom !== 'undefined') {
                                        $dropdown.css({'margin-top': ''})
                                    }
                                },
                                duration: 200,
                                easing: easing_swiftOut
                            });
                        }
                    });

                    //income expense category changes
                    $(".income-expense-category input").iCheck({
                        checkboxClass: "icheckbox_md",
                        radioClass: "iradio_md",
                        increaseArea: "20%"
                    }).on("ifChecked", function (event) {
                        var type=event.target.value;

                        var control=$categories[0].selectize;
                        control.clearOptions();

                        var data=custom_object_filter(income_expense_obj,"type",type);
                        for(item in data){
                            control.addOption(data[item]);

                        }
                    });
                    altair_form_validation.init();
                }
            })

        }
    },

    open_invoice: function() {

        var show_invoice = function(element) {
            var $this = element,
                $invoice_template = $(doc_type()+'#offline_invoice_template');

            var template = $invoice_template.html(),
                template_compiled = Handlebars.compile(template);


            // ajax function to get invoices
            var invoice_id = parseInt($this.attr('offline-data-invoice-id'));
            var docs=localStorage.getItem(default_doc_name());
            if(docs!==null){
                var docs_object=JSON.parse(docs);
                var data_object=document_object_filter(docs_object,"int_number",invoice_id);
                var response={};
                for(x in data_object){
                    var row_data=data_object[x];

                    if(default_doc_name()!="reciept"){
                        var invoice_services=[];
                        var services=row_data.services;
                        var total_amount=0;
                        for(x in services){
                                var vat=Number(services[x].vat);
                                var rate=Number(services[x].rate);
                                var quantity=Number(services[x].quantity);
                                var persent_amount=(vat/100)*rate*quantity;
                                var service_total=rate*quantity+persent_amount;
                                total_amount+=service_total;
                            var obj={
                                invoice:row_data.int_number,
                                service_description:services[x].description,
                                service_name:services[x].name,
                                service_quantity:services[x].quantity,
                                service_rate:services[x].rate,
                                service_total:service_total,
                                service_vat:services[x].vat,
                            };
                            invoice_services.push(obj);
                        }
                        var discount=row_data.discount;
                        if(row_data.discount_type=="persent"){
                            discount=total_amount*Number(discount)/100;
                        }
                        var due_date=next_date(row_data.issue_date,row_data.due_date);
                        var new_object={
                            invoice_date:row_data.issue_date,
                            invoice_due_date:due_date,
                            invoice_from_address_1:company_obj.address_line_1,
                            invoice_from_address_2:company_obj.address_line_2,
                            invoice_from_company:company_obj.name,
                            invoice_id:row_data.int_number,
                            invoice_number:row_data.int_number,
                            invoice_services:invoice_services,
                            invoice_to_address_1:row_data.address_line_1,
                            invoice_to_address_2:row_data.address_line_2,
                            invoice_to_company:row_data.name,
                            invoice_status:"Unpaid",
                            invoice_status_color:"danger",
                            invoice_total_value:total_amount,
                            invoice_type:document_name(),
                            invoice_vat_value:discount,
                            note:row_data.note,
                        };

                    }
                    else{
                        var expense_status=0;
                        var income_status=0;
                        if(row_data.type=="income"){
                            income_status=1;
                        }
                        else{
                            expense_status=1;
                        }

                        var this_category=custom_filter(income_expense_obj,"user_id",row_data.category);
                        var category_name=this_category.category;

                        var services=row_data.services;
                        var invoice_services=[];
                        var total_amount=0;
                        var sl=1;
                        var x;
                        for(x in services){
                                var vat=services[x].description;
                                var service_total=Number(services[x].amount);
                                total_amount+=service_total;
                            var obj={
                                service_id:row_data.int_number,
                                service_description:services[x].description,
                                service_sl:sl,
                                service_total:service_total,
                            };
                            invoice_services.push(obj);
                            sl++;
                        }
                        var this_team_member=custom_filter(team_members_obj,"user_id",row_data.team_member);

                        var new_object={
                            category:row_data.category,
                            category_name:category_name,
                            expense_status:expense_status,
                            income_status:income_status,
                            invoice_date:row_data.date,
                            invoice_id:row_data.int_number,
                            invoice_number:row_data.int_number,
                            invoice_services:invoice_services,
                            invoice_to_company:company_obj.name,
                            invoice_total_value:total_amount,
                            note:row_data.note,
                            team_member:row_data.team_member,
                            team_member_name:this_team_member.name,
                        };
                    }
                    var response={
                        invoice:new_object
                    };

                }
                var theCompiledHtml = template_compiled(response);
                $offline_invoice_preview.html(theCompiledHtml);
                $offline_invoice_form.html('');
                $window.resize();
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },290);
                altair_forms.select_elements();

                //confirm logistic status

                $(".logistic_order_status select").selectize({
                    onDropdownOpen: function($dropdown) {
                        $dropdown
                            .hide()
                            .velocity('slideDown', {
                            begin: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top':'0'})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                    },
                    onDropdownClose: function($dropdown) {
                        $dropdown
                            .show()
                            .velocity('slideUp', {
                            complete: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top': ''})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });
                    },
                    onChange: function(value) {

                    }
                })

            }


        };

        $(offline_invoice_list_class)
            .on('click','a[offline-data-invoice-id]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                // toggle card and show invoice
                altair_md.card_show_hide($offline_invoice_card,undefined,show_invoice,$(this));
                // set active class
                $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');
            });

//        if($(offline_invoice_list_class).find('a').length) {
//            // open first invoice
//            $(offline_invoice_list_class).find('a').eq(0).click();
//        } else {
//            // open form
//            $offline_invoice_add_btn.trigger('click');
//        }

    },
    print_invoice: function() {
        $body.on('click','#invoice_print',function(e) {
            e.preventDefault();
            UIkit.modal.confirm('Do you want to print this?', function () {
                // hide sidebar
                altair_main_sidebar.hide_sidebar();
                // wait for dialog to fully hide
                setTimeout(function () {
                    window.print();
                }, 300)
            }, {
                labels: {
                    'Ok': 'print'
                }
            });
        })
    },
    copy_list_sidebar: function() {
        // hide secondary sidebar toggle btn for large screens
        $sidebar_secondary_toggle.addClass('uk-hidden-large');

        var invoices_list_sidebar = $offline_invoices_list_main.clone();

        invoices_list_sidebar.attr('id','invoices_list_sidebar');

        $sidebar_secondary
            .find('.sidebar_secondary_wrapper').html(invoices_list_sidebar)
            .end();

    }
};
