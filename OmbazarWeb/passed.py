from OmbazarBackEnd .passed import Passed
from .helper import *


class WebPassed(Passed):
    def coupon_unlock(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            prize_id = coupon_unlock_doc = lucky_id_maker = lucky_coupon_doc = estimated_ids = ""
            common = self.common
            round_model = RoundBlock()
            unlock_model = UnlockCoupon()
            lucky_model = Lucky_Coupon()
            coupon_model = Coupon()

            this_request = request.POST
            coupon_number = this_request['coupon_number']
            mobile_number = this_request['mobile_number']
            if not len(coupon_number):
                messages['empty_coupon'] = "Enter a coupon number"

            if not len(mobile_number):
                messages['empty_mobile'] = "Enter a mobile number"

            if return_object['error'] or return_object['message']:
                return return_object

            round_id = this_request['round_id']

            block_id = this_request['block_id']
            es_id = this_request['es_id']

            # Check availability in this coupon number
            query = {"coupon_number": coupon_number}
            find = common.collection_data_one("coupons", query)
            # errors['find'] = find
            # errors['find_q'] = query
            if not find:
                messages['invalid_coupon'] = "Invalid coupon number!"
                return return_object
            else:
                status = find['status']
                if status:
                    messages['used'] = "This coupon already used!"
                    # show the coupon user
                    try:
                        unlock_query = {"coupon_number": coupon_number}
                        unlock_info = common.collection_data_one("unlock_coupons",  unlock_query)
                        if unlock_info:
                            messages['unlock_user'] = "His mobile number is " + unlock_info['mobile_number']
                        else:
                            messages['unlock_user_found'] = "Not found unlocker user this time!"
                    except Exception:
                        messages['unlock_user_exception'] = "Sorry unable to found now"

                    return return_object
            # Check coupon prize availability
            block_query = {"round_id": round_id, "block_id": block_id}
            find = common.collection_data_one("round_blocks", block_query)

            if find:
                try:
                    estimated_ids = find['estimated_ids']
                    id_info = estimated_ids[int(es_id)]
                    prize_id = id_info['prize_id']
                    status = id_info['status']
                    if status:
                        return_object['expect_message'] = "Sorry this block crached!"
                        return return_object
                    # update this block es id's status which is open now
                    id_info['status'] = 1
                    estimated_ids[int(es_id)] = id_info
                    # document unlock coupon
                    coupon_unlock_doc = {
                        "coupon_number": coupon_number,
                        "block_id": block_id,
                        "es_id": es_id,
                        "mobile_number": mobile_number,
                        "time": time,
                    }

                    # compare
                    u_compare = unlock_model.compare(coupon_unlock_doc)
                    if not u_compare:
                        errors['unlock_coupon_compare'] = "Unlock coupon compare fail!"

                    if prize_id:
                        try:
                            p_query = {"prize_id": prize_id}
                            p_doc = common.collection_data_one("prizes", p_query)
                            if not p_doc:
                                errors['prize_info'] = "Prize information not found!"
                            else:
                                prize_info = p_doc
                                prize_name = prize_info['prize_name']
                                p_message = "You have lucky for a <b>" + prize_name + "</b>"
                                p_message += '<i class="md-icon material-icons prize-details" data-coupon-number="'+coupon_number+'">print</i>'
                                return_object['expect_message'] = p_message

                                # lucky id making for a single lucky prize information
                                lucky_id_maker = common.IdMaker("lucky_coupon")
                                lucky_id = lucky_id_maker.user_id
                                if not lucky_id:
                                    errors['lucky_id_error'] = lucky_id_maker.error()

                                # Lucky coupon record doc
                                lucky_coupon_doc = coupon_unlock_doc
                                lucky_coupon_doc["prize_id"] = prize_id
                                lucky_coupon_doc['prize_name'] = prize_name
                                lucky_coupon_doc['active'] = 0
                                lucky_coupon_doc['lucky_id'] = lucky_id

                                # compare
                                l_compare = lucky_model.compare(lucky_coupon_doc)
                                if not l_compare:
                                    errors['lucky_coupon_compare'] = "Lucky coupon compare fail"
                                else:
                                    lucky_update = lucky_id_maker.update()
                                    if not lucky_update.status:
                                        errors['lucky_id_update'] = "lucky id update fail"
                        except Exception as e:
                            errors['prize_info'] = "prize information problem"
                            errors['prize_info_exception'] = str(e)
                    else:
                        p_message = "Sorry try another!"
                        # p_message += '<i class="md-icon material-icons sad-info">pageview</i>'
                        return_object['expect_message'] = p_message

                    # If any error not exist total unlock amount will be update
                    if not return_object['error'] and not return_object['message']:
                        try:
                            total_unlock = common.LastUpdate("total_coupon_unlock", round_id)
                        except:
                            messages['unknown_error'] = "Unknown error!"
                            errors['total_unlock'] = "Total unlock amount failed"
                        else:
                            total_unlock_update = total_unlock.update()
                            if not total_unlock_update.status:
                                errors['total_unlock'] = "Total coupon unlock failed!"

                    # If any error not exist find the next distribuited prize
                    if not return_object['error'] and not return_object['message']:
                        unlock_total_amount = common.total_amount("total_coupon_unlock", round_id)
                        next_prize_query = {
                            "unlock_amount": unlock_total_amount
                        }
                        find_next_prize = common.collection_data_one("prizes", next_prize_query)
                        if find_next_prize:
                            try:
                                prize_id = find_next_prize['prize_id']
                            except:
                                pass
                            else:
                                try:
                                    pipeline = [
                                        {
                                            "$project": {
                                                "block_id": 1,
                                                "round_id": 1,
                                                "estimated_ids": 1,
                                                "total": {
                                                    "$size": {
                                                        "$filter": {
                                                            "input": "$estimated_ids",
                                                            "cond": {"$eq": ["$$this.status", 0]}
                                                        }
                                                    }
                                                },
                                                "total_prize": {
                                                    "$size": {
                                                        "$filter": {
                                                            "input": "$estimated_ids",
                                                            "cond": {"$not": {
                                                                "$eq": ["$$this.prize_id", ""]}
                                                            }
                                                        }
                                                    }
                                                },

                                                "_id": 0,

                                            }
                                        },
                                        {
                                            "$match": {
                                                "total_prize": {"$lte": 1},
                                                "total": {"$ne": 0},
                                                "round_id": round_id
                                            }

                                        }
                                    ]
                                    find_data = common.aggregate_collection_data("round_blocks", pipeline, 10)
                                    lists = find_data['find_data']
                                    return_object['find'] = lists
                                    if lists:
                                        lucky_block = random.choice(lists)
                                        estimate_ids = []
                                        lucky_estimated_ids = lucky_block['estimated_ids']
                                        block_id = lucky_block['block_id']
                                        for x in lucky_estimated_ids:
                                            status = x['status']
                                            es_prize_id = x['prize_id']
                                            es_id = x['id']

                                            if es_prize_id == "" and status == 0:
                                                estimate_ids.append(es_id)
                                        lucky_es_id = random.choice(estimate_ids)
                                        lucky_es_object = {
                                            "id": lucky_es_id,
                                            "prize_id": prize_id,
                                            "status": 0,
                                        }
                                        lucky_estimated_ids[lucky_es_id] = lucky_es_object
                                        # distribute update
                                        lucky_where = {"block_id": block_id, "round_id": round_id}
                                        lucky_which = {"$set": {"estimated_ids": lucky_estimated_ids}}
                                        round_collection = round_model.collection_name()
                                        round_collection.update_one(lucky_where, lucky_which)
                                except:
                                    errors['prize_distribute'] = "Prize distribute error"
                                    errors['error_info'] = str(common.error_info())
                                # else:
                                #     messages['prize_distribute'] = "Prize distributed block id" + block_id + "dfdfd"

                except Exception as e:
                    errors['wrong_info'] = "Wrong information"
                    errors['prize_check_exception'] = str(e)
            else:
                errors['something_wrong'] = "Something wrong"

            # If any error not exist Lucky coupon id initiated
            if not return_object['error'] and not return_object['message']:
                # check lucky coupon id info available
                if lucky_coupon_doc:
                    lucky_update = lucky_id_maker.update()
                    if not lucky_update.status:
                        errors['lucky_coupon_update'] = lucky_update.error()

        except Exception as e:
            errors['coupon_unlock_exception'] = str(e)
            errors['error_info'] = str(common.error_info())
        else:
            # First check any error available# First check any error available
            if not return_object['error'] and not return_object['message']:
                # Coupon unlock record collection
                try:
                    # unlock coupon info pass
                    unlock_collection = unlock_model.collection_name()
                    unlock_collection.insert_one(coupon_unlock_doc)

                    # Lucky coupon doc info pass
                    if lucky_coupon_doc:
                        lucky_coupon_collection = lucky_model.collection_name()
                        lucky_coupon_collection.insert_one(lucky_coupon_doc)

                    # update round block
                    round_collection = round_model.collection_name()
                    where = {"block_id": block_id, "round_id": round_id}
                    which = {"$set": {"estimated_ids": estimated_ids}}
                    round_collection.update_one(where, which)

                    # update coupon status
                    coupon_collection = coupon_model.collection_name()
                    where = {"coupon_number": coupon_number}
                    which = {"$set": {"status": 1}}
                    coupon_collection.update_one(where, which)
                except Exception as e:
                    errors['last_unlock_exception'] = str(e)
                else:
                    return_object['status'] = 1

        return return_object
