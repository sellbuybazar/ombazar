let base_path = '/base/';
let template_dir = base_path+"OmbazarWeb/templates/OmbazarWeb/resource/";
let static_dir = "/static/OmbazarWeb/assets/";
let default_dir = base_path+"media/default/";
let app = angular.module("ombazar",["ngRoute"]);

/// url key for find location / keyword like /home and page is html file for which is content of this page
let urls = {
    home:{"url":"/","page":"home"},
    alter_home:{"url":"/home","page":"home"},
    profile:{"url":"/profile/:user_name?","page":"profile"},
    logout:{"url":"/logout","page":"logout"},
    not_found:{"url":"/404","page":"404"},
    login:{"url":"/login","page":"login"},
    coupon_unloker:{"url":"/coupon-unlocker","page":"coupon-unlocker"},
    prize_details:{"url":"/prize-details/:coupon_number","page":"prize-details"},
    no_prize_info:{"url":"/no-prize-info","page":"no-prize-info"},
    corporates:{"url":"/corporates","page":"corporates"},
    corporate:{"url":"/corporate/:corporate_id","page":"corporate"},
    registration:{"url":"/registration","page":"registration"},
    products:{"url":"/products/:category_name?","page":"products"},
    order:{"url":"/order/:order_id","page":"order"},
    orders:{"url":"/orders","page":"orders"},
    item:{"url":"/item/:store_item_id","page":"item"},
    tree:{"url":"/tree/:tree_user_id?","page":"tree"},
    forgot_password:{"url":"/forgot-password","page":"forgot-password"},
    top_up_requests:{"url":"/requests/:request_type","page":"requests"},
    edit_profile:{"url":"/edit-profile","page":"edit-profile"},
    buy_coupon:{"url":"/buy-coupon","page":"buy-coupon"},
    brand:{"url":"/brand/:brand_id","page":"brand"},
};


app.config(['$interpolateProvider','$routeProvider','$locationProvider','$httpProvider',function($interpolateProvider,$routeProvider,$locationProvider,$httpProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $locationProvider.html5Mode(true);

    //provider for csft token security reason
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    // single page development
    for (let url_info in urls){
        let url = urls[url_info].url;
        let page = urls[url_info].page;

        $routeProvider
        .when(url, {
            templateUrl : template_dir+page+".html",
        })
    }
    $routeProvider
    .otherwise(urls.not_found.url,{
        redirect: urls.not_found.page
    })

}]);

app.run(function ($rootScope, $location) {
    $rootScope.currency = "৳";
    $rootScope.error_page = function(){
        let url = "/404";
        $location.path(url);
    };
    $rootScope.site_logo = "/base/media/default/om-bazar-logo.png";
    $rootScope.rows = [];
    $rootScope.products = [];
    /// Breadcrumbs path
    $rootScope.path = $location.path();
    $rootScope.breadcrumbs = [];
    $rootScope.require = function(){};
    $rootScope.basic_info = {};
    // basic info scopes
    // ***************************
    //** id_types, logged_user, tree_info, logged_user_id, point_rules,
    // ***************************

    $rootScope.registration_complete = 0;
    $rootScope.table_pagination_path = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/parts/table-pagination.html';
    $rootScope.cart_path = '/base/OmbazarWeb/templates/OmbazarWeb/resource/parts/cart.html';
    $rootScope.brand_url = '/base/OmbazarWeb/templates/OmbazarWeb/resource/parts/brands.html';
    $rootScope.bag_switch = function(){
        $(".cart").toggleClass("cart-hidden");
    };
    $rootScope.selected_tree_info = {};
    $rootScope.product_search = function () {
        $location.path("/products");
        // $scope.search();
    }
});
app.filter('point', function($rootScope) {
  return function(input,quantity=0) {
      return  OmbazarFn.point_maker($rootScope,input,quantity);
  };
});

app.filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (let i=0; i<total; i++) {
        if (i>0){
            input.push(i);
        }

    }

    return input;
  };
});
app.filter('user_tooltip', function() {
  return function(input) {
      return OmbazarFn.user_tooltip(input);
  };
});

app.filter('if', function() {
  return function(input, result, comparism='') {
      if (comparism !== ""){
          if (input===comparism) {
              return result;
          }
      }
      else{
          if(input){
              return result;
          }
      }
  };
});

app.filter('default', function() {
  return function(input, result) {
      if (input === undefined){
          return result
      }
  };
});
app.filter('object_exist', function() {
  return function(input, objects,key_name, result = 0,else_result='not found') {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if (item !== undefined){
          if (result){
              return result;
          }
          else{
              return item;
          }
      }
      else{
          return else_result;
      }

  };
});

app.filter('line_total', function() {
  return function(input, objects,key_name) {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if(item !== undefined){
          let price = 0;
          if(item.sale){
              price = item.sale*item.quantity;
          }
          else{
              price = item.price*item.quantity;
          }
          return price;
      }
      else{
          return 0;
      }

  };
});

app.filter('line_quantity', function() {
  return function(input, objects,key_name) {
      let item = OmbazarFn.custom_filter(objects,key_name,input);
      if(item !== undefined){
          return item.quantity;
      }
      else{
          return 0;
      }

  };
});
app.filter('object_line_total', function() {
  return function(input) {
      let item = input;
      let price = item.price;
      let sale = item.sale;
      let vat = item.vat;
      let quantity = item.quantity;

      //for gather rice grand total
      let line_total = 0;
      if (item.sale){
          line_total = item.sale*item.quantity;
      }
      else{
          line_total = item.price*item.quantity;
      }
      let line_vat = line_total*(vat/100);
      let line_total_with_vat = line_total+line_vat;
      return line_total_with_vat;

  };
});


app.service('service',function ($rootScope) {
    /// logged user information wise  view
    this.logged_user = {};
    this.set_logged_user = function (user_data) {
        this.logged_user = user_data;
        $rootScope.$emit("set_logged_user")
    };
    this.get_logged_user = function () {
        return this.logged_user;
    };
    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };
    // for main menu handle access have or not
    this.menus = {};
    this.set_menus = function (menus) {
        this.menus = menus;
        $rootScope.$emit("set_menus")
    };
    this.get_menus = function () {
        return this.menus;
    };

    // for category chose tree
    this.category = {};
    this.set_category = function (new_category) {
        this.category = new_category;
        $rootScope.$emit("set_category")
    };
    this.get_category = function () {
        return this.category;
    };


});

// Page header to footer basic controller
app.controller("basic",['$scope','$http','$rootScope','service','$timeout','$route','$routeParams',function ($scope,$http,$rootScope,service,$timeout,$route,$routeParams) {
    // Retrive basic info for initiate access
    let function_name = "basic_information(request)";
    let extra_data = OmbazarFn.retrive_request_object(function_name);

    let form_data = OmbazarFn.form_data_maker("",extra_data);
    $scope.route_reload = function(){
        $route.reload();
    };
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(response);
        if(response.status){
            $rootScope.basic_info = response;
            OmbazarFn.id_types = $rootScope.basic_info.id_types;

            service.set_basic_info();

            if ($rootScope.basic_info.logged_user){
                service.set_logged_user();
            }
        }
        $timeout(function () {
            OmbazarFn.altair_init();
            //main sidebar
            altair_main_sidebar.init(true);

        })
    });

}]);



// for prize details
app.controller("home",['$scope','$http','$timeout','$routeParams','$rootScope','$location',function ($scope,$http,$timeout,$routeParams,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);

}]);

// Page header to
app.controller("coupon_unlocker",['$scope','$http','$rootScope','service','$timeout','$location',function ($scope,$http,$rootScope,service,$timeout,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    let function_name = "slider_coupons()";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.sliders = response.find_data;
        $timeout(function () {
            OmbazarFn.init_requirement();
            $scope.not_found = "Currently not available!"
        });

    });

    $("body").on("click",".crach-bar",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".mobile-number-bar").removeClass("hidden");
    });
    $("body").on("click",".coupon-next",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".input-bar").removeClass("hidden");
    });
    $("body").on("click",".coupon-close",function () {
        let item = $(this).closest(".single-coupon");
        item.find(".coupon-bar").addClass("hidden");
        item.find(".crach-bar").removeClass("hidden");
    });
    $scope.unlock = function (es_id,event) {
        let target_elem = $(event.target);
        let item = target_elem.closest(".single-coupon");
        let coupon_number = item.find(".input-bar .coupon-input-field input").val();

        let this_form = item.find(".coupon-unlock");
        let function_name = "coupon_unlock(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['es_id'] = es_id;
        let form_data = OmbazarFn.form_data_maker(this_form,extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            // console.log(response);
            if(response.status){
                let index_number = OmbazarFn.index_number($scope.my_coupon_list,"coupon_number",coupon_number);
                if (index_number !== undefined){
                    $scope.my_coupon_list.splice(index_number, 1);
                }
                if (typeof (response.expect_message) !== "undefined"){
                    item.find(".coupon-bar").addClass("hidden");
                    item.find(".coupon-message").removeClass("hidden").find(".used-text").html(response.expect_message);

                }
            }
        });
    };
    $scope.my_coupons = function (event) {
        OmbazarFn.get_my_coupons($http,$scope,function () {
        })
    };
    $scope.past_coupon = function(coupon_number,event){
        // console.log(coupon_number);
        let target = $(event.target);
        let parent = target.closest(".coupon-bar");
        let field = parent.find(".coupon-input-field").find("input");
        field.val(coupon_number);
        OmbazarFn.update_inputs();
    };
    /// SHow the prize information
    $("body").on("click",".prize-details",function () {
        let coupon_number = $(this).attr("data-coupon-number");
        let url ="/prize-details/"+coupon_number;
        $scope.$apply(function () {
            $location.path(url)
        })
    });

}]);

// for prize details
app.controller("prize_details",['$scope','$http','$timeout','$routeParams','$rootScope','$location',function ($scope,$http,$timeout,$routeParams,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.ombazar_logo = "/media/default/om-bazar-logo.png/";
    $scope.prize_image = "/media/default/prize.png";
    let coupon_number = $routeParams.coupon_number;
    let function_name = "prize_details('"+coupon_number+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.prize_details = response;
        $timeout(function () {
            OmbazarFn.init_requirement();
        });
    });
}]);

// login using this controller
app.controller("login",['$scope','$http','$location','service','$rootScope',function ($scope,$http,$location,service,$rootScope) {
    $rootScope.registration_complete = 0;
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    $scope.login = function(){
        let function_name = "login(request)";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".login-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $rootScope.basic_info['logged_user_id'] = response.user_info.user_id;
                $rootScope.basic_info['logged_user'] = response.user_info;
                $rootScope.basic_info['tree_info'] = response.tree_info;
                service.set_logged_user(response.user_info);
                let url = "/profile";
                $location.path(url);
            }
        });
    }

}]);

/// Corporates
app.controller("corporates",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.collection_name = "corporates";
    $scope.data_query = {};
    $scope.search_name = "";
    $scope.search_location = "";
    $scope.search_tags = "";
    $scope.query_option = {};
    $scope.page_title = "Corporates";
    $scope.switch = 1;
    $scope.search = function () {
        $scope.data_query['company_name'] = {"$regex": ""+$scope.search_name};
        $scope.data_query['location'] = {"$regex": ""+$scope.search_location};
        $scope.data_query['tags'] = {"$regex": ""+$scope.search_tags};
        $rootScope.rows_load();
    };
    $timeout(function () {
        OmbazarFn.init_requirement();
    });

}]);

/// view Corporate
app.controller("corporate",['$scope','$http','$routeParams','$timeout','$rootScope','$location',function ($scope,$http,$routeParams,$timeout,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    let corporate_id = $routeParams.corporate_id;
    let function_name = "corporate('"+corporate_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.corporate = response;
        $timeout(function () {
            // altair_md.init();
        })

    });
}]);

// Add a new controller
app.controller("add_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    $timeout(function () {
        OmbazarFn.init_requirement();
    });
    $scope.name = '';
    $scope.add_new_label = '';
    let controller_type = 'user';
    $scope.controller_type = controller_type;

    //check controller type and fix add new form information
    // if(controller_type === 'admin' ){
    //     $scope.add_new_label = "Add new admin";
    //     $scope.name = "Admin name";
    // }

    if(controller_type === 'user' ){
        $scope.add_new_label = "New registration";
        $scope.name = "User name";
    }

    OmbazarFn.countries_init($http,function () {
        // Any event fire after success
    });
    $scope.default_image = default_dir+"profile.png";

    $scope.submit = function () {
        //add an admin
        let function_name = "add_controller(request,'user')";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
            // console.log(extra_data);
        let form_data = OmbazarFn.form_data_maker(".add-controller",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $rootScope.registration_complete = 1;
                let user_name = response.user_name;
                let url = "/profile/"+user_name;
                OmbazarFn.add_action(response,$location,url);
            }
        });
    };

}]);

// Profile page view using this controller
app.controller("profile",['$scope','$http','$location','$routeParams','$rootScope','service','$timeout',function ($scope,$http,$location,$routeParams,$rootScope,service,$timeout) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    $scope.top_up_switch = 0;
    $scope.exchange_point_switch = 0;
    $scope.withdraw_switch = 0;
    $scope.balance_action = 0;
    // Pending switch
    $scope.top_up_pending_switch = 0;
    $scope.withdraw_pending_switch = 0;

    OmbazarFn.get_basic_info($rootScope,service,function () {
        let user_name = $routeParams.user_name;
        if (user_name === undefined){
            $scope.balance_action = 1;
        }
        else{
            $scope.balance_action = 0;
        }
        let function_name = '';
        if(user_name !== undefined){
            function_name = "controller_info('"+user_name+"',True)";
        }
        else{
            function_name = "logged_controller(request)";
        }

        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            // console.log(response);
            if (response.pending !== undefined){
                // Top up pending possibility
                let top_up_pending = OmbazarFn.custom_filter(response.pending,"type","top_up",true);
                if (top_up_pending !== undefined){
                    if (top_up_pending.length){
                        $scope.top_up_pending_switch = 1;
                    }
                }
                // Withdraw pending possibility
                let withdraw_pending = OmbazarFn.custom_filter(response.pending,"type","user_withdraw",true);
                if (withdraw_pending !== undefined){
                    if (withdraw_pending.length){
                        $scope.withdraw_pending_switch = 1;
                    }
                }

            }
            altair_md.fab_speed_dial($scope);
            $scope.user = response;
            if(response.user_id === undefined){
                $rootScope.error_page();
            }
            $scope.tree_info = $rootScope.basic_info.tree_info;
            $timeout(function () {
                OmbazarFn.init_requirement();
            });

        });
        // exchange point
        $scope.exchange_point = function () {
            function_name = "point_exchange(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".point-exchange",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Balance added");
                    $scope.tree_info.account_balance = response.account_balance;
                    $scope.tree_info.point_balance = response.point_balance;
                    OmbazarFn.clean("form");
                }
            });
        };

        // Top up
        $scope.top_up = function () {
            function_name = "top_up(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".top-up",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.top_up_pending_switch = 1;
                }
            });
        };
        // with draw
        $scope.withdraw = function () {
            function_name = "withdraw(request,'user_withdraw')";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".withdraw",extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                if (response.status){
                    OmbazarFn.notify("Request send.");
                    OmbazarFn.clean("form");
                    $scope.withdraw_pending_switch = 1;
                }
            });
        };

        $scope.open_top_up = function () {
            $scope.fab_close();
            $scope.top_up_switch = 1;
        };
        $scope.open_exchange = function () {
            $scope.fab_close();
            $scope.exchange_point_switch = 1;
        };
        $scope.open_withdraw = function () {
            $scope.fab_close();
            $scope.withdraw_switch = 1;
        };

        $scope.fab_close = function () {
            $scope.top_up_switch = 0;
            $scope.exchange_point_switch = 0;
            $scope.withdraw_switch = 0;
        };
        $("body").on("click",".md-fab-action-close",function () {
            $scope.$apply(function () {
               $scope.fab_close();
            });
        });
    });


}]);
// Logout using this controller
app.controller("logout",['$scope','$http','$location','$rootScope','service',function ($scope,$http,$location,$rootScope,service) {
    //page requirement
    let function_name = "logout(request)";

    let extra_data = OmbazarFn.common_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        if(response.logout !== undefined){
            $rootScope.basic_info.logged_user = {};
            $rootScope.basic_info.logged_user_id = 0;
            service.set_logged_user();
            let url = "";
            $location.path(url);
        }
        else{
            console.log(response);
        }

    });
}]);

// cart controller
app.controller("cart",['$scope','$http','$rootScope','service','$timeout','$location',function ($scope,$http,$rootScope,service,$timeout,$location) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.products = $rootScope.products;
        $scope.cart = [];
        let before_item = [];
        let cookies = Cookies.getJSON();
        for (let x in cookies){
            if(x !== 'csrftoken'){
                before_item.push(x);
            }
        }
        let c_query = {
            "store_item_id": {"$in": before_item}
        };
        let parameters = ["request"];
        OmbazarFn.retrieve_data($http,"cart_items",c_query,function (response,status) {
           let finds = response.find_data;
           for (let x in finds){
               let target = finds[x];
               let store_item_id = target.store_item_id;
               let cart_ob = cookies[store_item_id];
               target.quantity = cart_ob.quantity;
               let point = OmbazarFn.point_maker($rootScope,target);
               target.point = point;
               $scope.cart.push(target);
           }

           $scope.calculate();
        },{},true,parameters);

        $scope.add_to_cart = function(store_item_id,$event=0){
            if ($event){
                $event.stopImmediatePropagation();
            }
            $scope.products = $rootScope.products;
            OmbazarFn.add_to_cart($rootScope,$scope, store_item_id);
            // console.log($scope.products);
        };
        $scope.delete_cart_item = function($index,store_item_id){
            $scope.cart.splice($index, 1);
            Cookies.remove(store_item_id);
            $scope.calculate();
        };
        $scope.left_to_cart = function(store_item_id,$event){
            $event.stopImmediatePropagation();
            let index = OmbazarFn.index_number($scope.cart,"store_item_id",store_item_id);
            let item = $scope.cart[index];
            let before_quantity = item.quantity;
            let net_quantity = Number(before_quantity)-1;
                if(net_quantity){
                    OmbazarFn.add_to_cart($rootScope,$scope, store_item_id,net_quantity,true);
                }
                else{
                    $scope.delete_cart_item(index,store_item_id);
                }

        };

        $scope.sub_total = 0;
        $scope.vat = 0;
        $scope.discount = 0;
        $scope.grand_total = 0;
        $scope.total_discount = 0;
        $scope.total_item = 0;
        $scope.calculate = function(){
            let cart_items = $scope.cart;
            let total_info = OmbazarFn.order_total_info(cart_items,$scope.discount);
            $scope.sub_total = total_info.sub_total;
            $scope.vat = total_info.vat;
            $scope.point = total_info.point;
            $scope.grand_total = total_info.grand_total;
            $scope.total_discount = total_info.total_discount;
            $scope.total_item = total_info.total_item;
            $('.odometer').html($scope.grand_total);
        };
        $scope.clear_cart = function(){
            let cookies = Cookies.getJSON();
            for (let x in cookies){
                if(x !== 'csrftoken'){
                    Cookies.remove(x);
                }
            }
            $scope.cart = [];
            $scope.calculate();
        };
        $("body").on("change keyup",".quantity",function () {
            let quantity = Number($(this).val());
            let store_item_id = $(this).closest("tr").attr("data-id");

            $scope.$apply(function () {
                OmbazarFn.add_to_cart($rootScope,$scope, store_item_id,quantity,true);
            });
        });
        $("body").on("keyup",".cart-discount-field",function () {

            let discount = Number($(this).val());
            $scope.$apply(function () {
                $scope.discount = discount;
                $scope.calculate();
            });
        });
        $scope.order = function(){
            let data = JSON.stringify($scope.cart);
            let method = "order(request)";
            let extra_data = OmbazarFn.passed_request_object(method);
                extra_data['order_type'] = "web";
                extra_data['discount'] = $scope.discount;
                extra_data['data'] = data;
                extra_data['customer_id'] = $rootScope.basic_info.logged_user_id;
            let form_data = OmbazarFn.form_data_maker("", extra_data);
            OmbazarFn.$http($http,form_data,function (response,status) {
                console.log(response);
                if(response.status){
                    let order_id = response.order_id;
                    let url = "/order/"+order_id;
                    $scope.discount = 0;
                    $scope.clear_cart();
                    $location.path(url);
                    $rootScope.bag_switch();
                }
            })
        };
        $timeout(function () {
            OmbazarFn.init_requirement();
        },100);
    });


}]);


/// Pos for sell
app.controller("products",['$scope','$http','$location','$timeout','$rootScope','service','$routeParams',function ($scope,$http,$location,$timeout,$rootScope,service,$routeParams) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.limit = '10';
        $scope.total = 0;
        $scope.skip = 0;
        $scope.collection_name = "products";
        $scope.data_query = {};
        $scope.query_option = {};
        $scope.page_title = "Pos";
        $scope.switch = 1;
        $scope.rows = [];
        // $scope.products = $scope.rows;
        $scope.custom_function = {
            function_name: "products",
            parameteres: ['request'],
        };
        OmbazarFn.get_categories($http,function (categories) {
            let category_name = $routeParams.category_name;
            // For brand
            let brand_id = $routeParams.brand_id;
            if (category_name!==undefined){
                let category_info = OmbazarFn.custom_filter(categories,"category_name",category_name);
                let category_id = category_info.category_id;
                if(category_id !== undefined){
                    $timeout(function () {
                        // $scope.rows = [];
                    });

                    $scope.data_query['category_id'] = category_id;
                }
                else{
                    // console.log('category not found');
                }

            }
            if (brand_id !== undefined){
                $scope.data_query.store_id = brand_id;
            }
            let logged_user = $rootScope.basic_info.logged_user;
            if (logged_user !== undefined){
                $scope.query_option['post_code'] = logged_user.post_code;
            }

            $scope.pos_load = function(event=function () {}){
                OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                    console.log(response);

                    for (let item in $scope.rows){
                        let item_info = new Object();
                        Object.assign(item_info,$scope.rows[item]);
                        let point = OmbazarFn.point_maker($rootScope,item_info,1);
                        $scope.rows[item].point = point;
                    }
                        $timeout(function () {
                            $rootScope.products = $scope.rows;
                        });

                        // console.log($scope.products);
                        event(response,status);
                        altair_helpers.hierarchical_show();
                });
            };
            OmbazarFn.get_basic_info($rootScope,service,function () {
                OmbazarFn.total_counter($http,$scope,function (response, status) {
                    $scope.total = response.total;
                    $scope.pos_load()
                });
            });

            var element=window;
                $(element).on("scroll",function(e){
                    var scrollTop=$(this).scrollTop();
                    var element_offset_top=$(document).height()-$(window).height();
                    if(scrollTop==element_offset_top){
                        $scope.$apply(function () {
                            $scope.pos_load()
                        });
                    }
                });
            $scope.search = function(){
                // console.log($scope.search_name);
                let search_name = $scope.search_name;
                $scope.rows = [];
                $scope.skip = 0;
                // $scope.data_query = {};
                $scope.data_query['item_name'] = {"$regex": $scope.search_name};
                OmbazarFn.request_data_load($http,$scope,$rootScope,$timeout,function (response,status) {
                    console.log(response);
                    altair_helpers.hierarchical_show();
                });


            };
            $rootScope.product_search = function () {
                // console.log("df");
                // $location.path("/products");
                $scope.search();
            }
        });
    });


}]);

/// view order
app.controller("order",['$scope','$http','$routeParams','$timeout','$rootScope','$location',function ($scope,$http,$routeParams,$timeout,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    let order_id = $routeParams.order_id;
    let function_name = "order(request,'"+order_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(order_id);
        if (response.find_data.length){
            let order = response.find_data[0];
            $scope.order = order;

            order = OmbazarFn.order_info_filter(order,$rootScope);
            // console.log(order);
            $timeout(function () {
                // altair_md.init();
            });
            OmbazarFn.order_status_selectize(order.status,1);
        }
        else{
            console.log(response);
        }

    });
}]);

/// Corporates
app.controller("orders",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.collection_name = "orders";
    $scope.data_query = {};
    $scope.search_name = "";
    $scope.search_location = "";
    $scope.search_tags = "";
    $scope.query_option = {};
    $scope.page_title = "Orders";
    $scope.switch = 1;
    $scope.data_query['customer_id'] = $rootScope.basic_info.logged_user_id;
    $timeout(function () {
        OmbazarFn.init_requirement();
    });

}]);

/// view item using store item id
app.controller("item",['$scope','$http','$routeParams','$rootScope','$location',function ($scope,$http,$routeParams,$rootScope,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    OmbazarFn.init_requirement();
    let item_id = $routeParams.store_item_id;
    let function_name = "store_item(request,'"+item_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.item = response;
        console.log(response);
        let parent_id = response.category_id;
        $scope.item = response;
        $scope.item.parent_name = "Uncategorized";
        if(!parent_id || parent_id!=='0'){
            if(OmbazarFn.categories.length){
                let categories = OmbazarFn.categories;
                let parent = OmbazarFn.custom_filter(categories,"category_id",parent_id);
                $scope.item.parent_name = parent.category_name;

            }
            else{
                let function_name = "category('"+parent_id+"')";
                let extra_data = OmbazarFn.retrive_request_object(function_name);
                let form_data = OmbazarFn.form_data_maker("",extra_data);
                OmbazarFn.$http($http,form_data,function (response) {
                    let category_name = response.category_name;
                    $scope.item.parent_name = category_name;
                })
            }
        }
    });
}]);



/// add category
app.controller("categories",['$scope','$http','$timeout','$location','service',function ($scope,$http,$timeout,$location,service) {
    // Access category fancy tree initiate
    OmbazarFn.access_category_init($http,$scope,$timeout,function () {
        /// any event call when access catgory initiate

    });

}]);
app.directive("subCategories",function () {
    let template  = ''+
            '<ul ng-if="category.categories.length>0">\n' +
            '<li id="((category.category_id))" ng-repeat="category in category.categories " >\n' +
            '<a href="products/((category.category_name))">\n'+
            '<span class="menu_title">((category.category_name))</span>\n'+
            '</a>\n'+
            '<!-- directive: sub-categories -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// Tree
app.controller("tree",['$scope','$http','$routeParams','$route','$rootScope','service','$timeout','$location',function ($scope,$http,$routeParams,$route,$rootScope,service,$timeout,$location) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.exchange = true;
    OmbazarFn.get_basic_info($rootScope,service,function () {
        OmbazarFn.init_requirement();
        $scope.self_tree_info = $rootScope.basic_info.tree_info;
        $scope.top_tree_info = new Object();
        Object.assign($scope.top_tree_info,$scope.self_tree_info);
        $scope.more_data = {
            "limit": 0,
            "status": "Paid"
        };
        let user_id = $routeParams.tree_user_id;
        // For when select any other user tree view

        if (user_id !== undefined){
            $scope.selected_tree_info = $rootScope.selected_tree_info[user_id];
            if ($scope.selected_tree_info === undefined){
                $location.path("/tree");
                return false;
            }
            $scope.more_data.parent_id = user_id;
            $scope.top_tree_info = $scope.selected_tree_info;
        }
        else{
            user_id = $scope.top_tree_info.user_id;
            $rootScope.selected_tree_info = {};
        }

        // console.log(user_id);
        let free_tree,main_tree = undefined;
        $scope.load_tree = function(){
            OmbazarFn.get_tree($http,$scope.top_tree_info,function (tree,response) {
                $scope.member_tree_view = function(member_id){
                    let tree_data = response.tree_data.find_data;
                    let selected_user = OmbazarFn.custom_filter(tree_data,"user_id",member_id);
                    $rootScope.selected_tree_info[member_id] = selected_user;
                };
                let data = response.exchange_child_count.total;
                if (data>2){
                    $scope.exchange = false;
                }
                if ($rootScope.basic_info.logged_user_id !== $scope.top_tree_info.user_id){
                    tree.child = 1;
                }
                $scope.tree = tree;
                $timeout(function(){
                    main_tree = OmbazarFn.tree(".network",{
                        'class_name': 'network',
                        'exchange_option': $scope.exchange,
                        'drag_disabled': true,
                        'drag_start': function (e) {
                            // console.log(e);
                        },
                        'before_drop': function (this_element,tree,ui,event=function () {}) {
                            let dropped_parent = $(this_element).closest(".tree");
                            let draggable_item_amount = ui.draggable.closest("li").children("ul").children("li").length;
                            let draggable_item = ui.draggable.closest("li");
                            let draggable_item_id = draggable_item.attr("id");
                            let available_child=$(this_element).closest("li").children("ul").children("li").length;

                            if ($(this_element).hasClass('check_div')) {
                                OmbazarFn.notify('Cant Move on Child Element.',"top-center",2000);
                                event(false);
                            }
                            else if(available_child>=2 && !dropped_parent.hasClass("free-tree")){
                                OmbazarFn.notify('Tow hand close.',"top-center",2000);
                                event(false);
                            }
                            else if(draggable_item_amount>0){
                                OmbazarFn.notify("Group not allow! chose a single person");
                                event(false);
                            }
                            else if(draggable_item_id===$rootScope.basic_info.logged_user_id){
                                OmbazarFn.notify("Not adjustable!");
                                event(false);
                            }

                            else{

                                let dropped_element = $(this_element).closest("li");
                                let dropped_element_user_id = dropped_element.attr("id");
                                let drooped_user_info = OmbazarFn.custom_filter(response.tree_data.find_data,'user_id',dropped_element_user_id);
                                if (drooped_user_info !== undefined){
                                    let total_child = drooped_user_info.total_child;
                                    if (total_child >= 2){
                                        OmbazarFn.notify('Members already exist!',"top-center",2000);
                                        event(false);
                                    }
                                    else{
                                        let new_child = $(ui.draggable).closest("li").attr('id');
                                        let up_line_referer = dropped_element.attr('id');
                                        // console.log(up_line_referer);
                                        let referer = $rootScope.basic_info.logged_user_id;
                                        let available_child=$(this_element).closest("li").children("ul").children("li").length;
                                        let position = "Left";
                                        if (available_child ===1){
                                            position = "Right";
                                        }
                                        let new_position_object = {
                                            new_child:new_child,
                                            up_line_referer: up_line_referer,
                                            referer: referer,
                                            position: position,
                                        };
                                        OmbazarFn.set_tree_position($http,new_position_object,function (response,status) {
                                            // console.log(response);
                                            if (response.status){
                                                event(true);
                                            }
                                            else{
                                                event(false);
                                            }
                                        });
                                    }
                                }
                                else{
                                    console.log("Up line referer info not found");
                                }

                            }
                        },
                        'drop': function (element,tree,ui) {
                            if (free_tree !== undefined){
                                free_tree[0].call_structure();
                            }
                            let available_child=$(element).closest("li").children("ul").children("li").length;
                            // console.log(available_child);
                            let new_item_id = $(ui.draggable).closest("li").attr('id');
                            let new_item = $(".network #"+new_item_id);
                            if ($scope.free_tree !== undefined){
                                let members = $scope.free_tree[0].tree;
                                if (members !== undefined){
                                    let new_member = OmbazarFn.custom_filter(members,'user_id',new_item_id);
                                        new_member.status = "Paid";
                                        let position = "Left";
                                        if (available_child === 2){
                                            position = "Right";
                                        }
                                        new_member.position = position;
                                    let user_tooltip = OmbazarFn.user_tooltip(new_member);
                                    new_item.children(".tree-user").children("a").attr("title",user_tooltip).attr("data-position",position)
                                    .attr("href","tree/"+new_item_id);
                                }
                            }
                        },
                        on_exchange:function (this_parent) {
                            let childes = this_parent.children("ul").children("li");
                            let first_user_id = $(childes[0]).attr("id");
                            let second_user_id = $(childes[1]).attr("id");
                            if(childes.length === 2){
                                let update_list = [
                                    {
                                        "user_id": first_user_id,
                                        "position": "Left"
                                    },
                                    {
                                        "user_id": second_user_id,
                                        "position": "Right"
                                    }
                                ];
                                let data_object = {
                                    "data": JSON.stringify(update_list)
                                };
                                OmbazarFn.exchange_position($http,data_object,function (response) {
                                    $scope.route_reload();
                                });
                            }
                        }
                    });

                });
            },$scope.more_data);
        };

        $scope.load_tree();

        $scope.load_free_tree = function () {
            $scope.more_data.status = "Free";
            $scope.more_data.limit = 0;
            OmbazarFn.get_tree($http,$scope.self_tree_info,function (tree,response) {
                $scope.free_tree = tree;
                $timeout(function(){
                    free_tree = OmbazarFn.tree(".free-tree",{
                        'class_name': 'free-tree',
                        'exchange_option': false,
                        'edit_option':true,
                        before_edit: function (that,e) {
                            let item = $(that).closest("li");
                            let item_id = item.attr("id");
                            let item_info = OmbazarFn.custom_filter(response.find_data,'user_id',item_id);
                            let form = item.children(".tree-user").find("form");
                            if (item_info !== undefined){
                                let position = item_info.position;
                                form.children("select").val(position);
                            }
                            form.children("select").change(function () {
                                let value = $(this).val();
                                let where = {
                                    "user_id": item_id
                                };
                                let which = {
                                    "position": value
                                };
                                OmbazarFn.document_update($http,"tree",where,which,function (response) {
                                    if (response.status){
                                        item_info.position = value;
                                    }
                                });

                            });
                        }
                    });
                    $scope.control_free_tree = function () {
                        $(".tree-area").toggleClass("tree-overflow");
                        free_tree[0].call_structure();
                        main_tree[0].call_structure();
                    };
                });
            },$scope.more_data,false);
        };
        $scope.load_free_tree();


    });
}]);


app.directive("stalks",function () {
    let not_condition = 'stalk.status !== \"Paid\" ';
    let condition = 'stalk.status == \"Paid\" ';
    let template  = ""+
            "<ul>\n" +
            "<li class='((stalk.child | if : \"child\"))' id='((stalk.user_id))' ng-repeat='stalk in stalk.tree' > \n" +
            "<div class='tree-user'> "+
            "<a ng-click='member_tree_view( ((stalk.user_id)) )' ng-if='"+condition+"' class='' data-position='((stalk.position))' href='tree/((stalk.user_info.user_id))' data-uk-tooltip='{pos:\'right\'}' title='((stalk | user_tooltip))'>\n"+
            "<img src='((stalk.user_info.image))'>"+
            "<span>((stalk.user_info.name))</span>\n"+
            "</a>\n"+
            ""+
            "<a ng-if='"+not_condition+"' class='' data-position='((stalk.position))'  data-uk-tooltip='{pos:'right'}' title='((stalk | user_tooltip))'>\n"+
            "<img src='((stalk.user_info.image))'>"+
            "<span>((stalk.user_info.name))</span>\n"+
            "</a>\n"+
            "</div>"+
            "<!-- directive: stalks -->" +
            "</li>\n" +
            "</ul>\n";
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// forgot password
app.controller("forgot_password",['$scope','$http','$routeParams','$rootScope','$location','$timeout',function ($scope,$http,$routeParams,$rootScope,$location,$timeout) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    $scope.all_off = function(){
        $scope.find_form_switch = 0;
        $scope.confirmation_switch = 0;
        $scope.find_list_switch = 0;
        $scope.new_password_switch = 0;
        $scope.not_found_switch = 0;
    };
    $scope.all_off();
    $scope.find_form_switch = 1;

    $timeout(function () {
        OmbazarFn.init_requirement();
    });


    $scope.find_forgots = function () {
        let function_name = "find_users(request)";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".find-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.find_data !== undefined){
                if (response.find_data.length){
                    $scope.find_list = response.find_data;
                    $scope.all_off();
                    $scope.find_list_switch = 1;
                }
                else {
                    $scope.all_off();
                    $scope.not_found_switch = 1;
                }
            }
        })
    };
    $scope.user_select = function (user_id, user_email, mobile) {
        let function_name = "send_code_forgot_user(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
            extra_data['email'] = user_email;
            extra_data['user_id'] = user_id;
            extra_data['mobile'] = mobile;
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                $scope.code_sender_email = user_email;
                $scope.all_off();
                $scope.confirmation_switch = 1;
                $timeout(function () {
                    OmbazarFn.init_requirement();
                })
            }
        })
    };
    $scope.confirm_code = function () {
        let function_name = "confirm_code(request)";
        let extra_data = OmbazarFn.passed_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".confirm-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.new_password_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.new_password = function () {
        let function_name = "new_password(request)";
        let extra_data = OmbazarFn.modify_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker(".new-password-form",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if (response.status){
                $scope.all_off();
                $scope.done_switch = 1;
            }
            $timeout(function () {
                OmbazarFn.init_requirement();
            })
        })
    };
    $scope.back_to_find = function () {
        $scope.all_off();
        $scope.find_form_switch = 1;
        $timeout(function () {
            OmbazarFn.init_requirement();
        })
    }

}]);
// Requests
app.controller("requests",['$scope','$http','$location','$routeParams','$timeout','$rootScope','service',function ($scope,$http,$location,$routeParams,$timeout,$rootScope,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        $scope.user_type = OmbazarFn.logged_user_type($rootScope);
        let request_type = $routeParams.request_type;
        $scope.page_title = "";
        $scope.type = "";
        if (request_type === "top-up"){
            $scope.page_title = "Top up requests";
            $scope.type = "top_up";
        }
        else if(request_type === "user-withdraw"){
            $scope.page_title = "Withdraws";
            $scope.type = "user_withdraw";
        }
        else if(request_type === "exchange-point"){
            $scope.page_title = "Exchange point";
            $scope.type = "point_exchange";
        }

        $scope.collection_name = "transactions";
        $scope.data_query = {
            "type": $scope.type,
            "user_id": $rootScope.basic_info.logged_user_id
        };
        $scope.query_option = {};
        $scope.switch = 1;
        $scope.custom_function = {
            function_name: "transactions",
            parameteres: ['request']
        };
    });


}]);

app.controller("edit_controller",['$scope','$http','$location','$interpolate','$routeParams','$timeout','service','$rootScope',function ($scope,$http,$location,$interpolate,$routeParams,$timeout,service,$rootScope) {
    $rootScope.breadcrumbs = OmbazarFn.route_info($rootScope,$location);
    //page requirement
    OmbazarFn.get_basic_info($rootScope,service,function () {
        // for access stores
        $scope.stores_init = 1;
        $scope.edit_label = "Edit profile";
        $scope.controller_type = "user";
        //check controller type and fix add new form information

        let user_id = $rootScope.basic_info.logged_user_id;
        let function_name = "controller_info('"+user_id+"')";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            $scope.controller = response;
            $scope.name = $scope.controller.name;
            $timeout(function () {
                OmbazarFn.init_requirement();
                OmbazarFn.countries_init($http,function (selectize) {
                    // Any event fire after success
                    let instance = selectize;
                    instance.setValue($scope.controller.calling_code);
                });

            });
        });
    });

    $scope.submit = function () {
        //edit controller
        function_name = "edit_controller(request)";
        extra_data = OmbazarFn.modify_request_object(function_name);
        extra_data['nationality'] = JSON.stringify(OmbazarFn.selected_country);
        extra_data['user_id'] = $scope.controller.user_id;
        extra_data['before_password'] = $scope.controller.password;
        // extra_data['password'] = "open1234";
        extra_data['before_image'] = $scope.controller.image;
        extra_data['edit'] = 1;

        form_data = OmbazarFn.form_data_maker(".edit-controller",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            if(response.status){
                let url = "/profile/"+$scope.controller.user_name;
                OmbazarFn.add_action(response,$location,url);
            }
        });

    }
}]);


/// Prize manager
app.controller("buy_coupon",['$scope','$http','$location','$timeout','$rootScope','$compile','$interpolate','service',function ($scope,$http,$location,$timeout,$rootScope,$compile,$interpolate,service) {
    OmbazarFn.get_basic_info($rootScope,service,function () {
        OmbazarFn.init_requirement();
        $scope.collection_name = "my_coupons";
        $scope.data_query = {};
        $scope.query_option = {
            "user_id": $rootScope.basic_info.user_id
        };
        $scope.custom_function = {
            function_name: "my_coupons",
            parameteres: ['request']
        };
        $scope.page_title = "Buy coupon";
        $scope.switch = 1;
        $scope.submit = function(){
            let function_name = "buy_coupon(request)";
            let extra_data = OmbazarFn.passed_request_object(function_name);
            let form_data = OmbazarFn.form_data_maker(".buy-coupon",extra_data);

            OmbazarFn.$http($http,form_data,function (response,status) {
                // console.log(response);
                if(response.status){
                    OmbazarFn.add_action(response,$location);
                    $rootScope.rows_load();
                }
            });
        };
    });
}]);


/// Brand manager
app.controller("brand_slides",['$scope','$http','$location','$timeout','$rootScope',function ($scope,$http,$location,$timeout,$rootScope) {
    $scope.brands_init = 0;
    $scope.slide = function () {
        OmbazarFn.get_brand_slides($http,$scope,function () {
            if ($scope.brands_init){
                $scope.brands_init = 0;
            }
            else{
                $scope.brands_init = 1;
            }
        });

    };

}]);

/// Brand manager
app.controller("brand",['$scope','$http','$location','$timeout','$rootScope','$routeParams',function ($scope,$http,$location,$timeout,$rootScope,$routeParams) {
    let brand_id = $routeParams.brand_id;
    let function_name = "store('"+brand_id+"')";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        $scope.brand = response;
    });
}]);



app.controller("data_table_handler",['$scope','$http','$timeout','$rootScope',function ($scope,$http,$timeout,$rootScope) {

    $scope.limit = '10';
    $scope.filtered = 0;
    $scope.start_row = 0;
    $scope.total = 0;
    $scope.page = '1';
    $scope.prev_switch = 1;
    $scope.next_switch = 1;

    OmbazarFn.total_counter($http,$scope,function (response, status) {
        // console.log(response);
        $scope.total = response.total;
        $scope.pages = Math.ceil($scope.total/$scope.limit);


        OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

        });
        $scope.next = function () {
            $scope.page = Number($scope.page)+1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.prev = function () {
            $scope.page = $scope.page-1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {
            });
        };
        $scope.reload = function () {
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $scope.first = function () {
            $scope.page = 1;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $scope.last = function () {
            let last_page = $scope.pages;
            $scope.page = last_page;
            OmbazarFn.data_load($http,$scope,$rootScope,$timeout,function (response,status) {

            });
        };
        $rootScope.rows_load = function () {
            OmbazarFn.total_counter($http,$scope,function (response,status) {
                $scope.total = response.total;
                $scope.pages = Math.ceil($scope.total/$scope.limit);
                $scope.reload();
            });
        }
    });
}]);



