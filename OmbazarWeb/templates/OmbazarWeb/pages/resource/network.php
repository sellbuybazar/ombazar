<div id="page_content">
    <div id="top_bar">
        <ul id="breadcrumbs">
            <li><a href="<?php echo SITE_URL;?>home" class="no_load">Home</a></li>
            <li><a href="<?php echo SITE_URL;?>network" class="no_load">Network</a></li>
            <li><span>Tree</span></li>
        </ul>
    </div>
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content">
                <div class="overflow  tree-overflow tree-area">
                    <button data-uk-tooltip="{pos:'left'}" title="Free tree" class="md-btn md-btn-default md-btn-mini md-btn-wave-light waves-effect waves-button waves-light"><i class="material-icons">tab</i></button>
                    <div class="uk-width-medium-8-10 uk-float-left">
                        <ul class="tree">
                            <?php
                                $tree=$retrive->tree();
                                echo $tree['data'];
                            ?>
                        </ul>
                        
                    </div>
                    <div class="uk-width-medium-2-10 uk-float-left">
                        <ul class="tree">
                            <?php
                                $tree=$retrive->tree();
                                echo $tree['data'];
                            ?>
                        </ul>
                        
                    </div>
                    
                </div>

            </div>
        </div>

    </div>
</div>
