import json
import re
import random
import hashlib
import pymongo
import barcode
import sys, os
import rstr
import bson
import sendgrid
from openpyxl import Workbook
from django.conf import settings
from .models import *
from .helper import *
from django.core.files.storage import FileSystemStorage
from barcode.writer import ImageWriter
# Assign a line for all requirement in this availability
from sendgrid.helpers.mail import *

class Common:
    def __init__(self):
        self.db = settings.DB
        self.base_bir = settings.BASE_PATH + "OmbazarBackEnd/"
        self.template_dir = self.base_bir + "templates/"

    def shuffle(self, string):
        s = string
        r = ''.join(random.sample(s, len(s)))
        return r

    def random_string(self):
        return rstr.xeger(r'[A-Z]\d[A-Z]\d[A-Z]\d')

    def custom_filter(self, obj, key_name, finder_object, negative=False):
        def fil_func(item):
            if negative:
                if item[key_name] in finder_object:
                    return False
                else:
                    return True
            else:
                if item[key_name] in finder_object:
                    return True
                else:
                    return False

        net_data = list(filter(fil_func, obj))

        return net_data

    def countries(self):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages,
        }
        try:
            url = self.template_dir+"OmbazarBackEnd/async/countries.json"
            country_list = open(url, "r", encoding='utf-8')
            result = country_list.read()
            result = json.loads(result)
        except Exception:
            errors['data_reading'] = "Data reading problem"

        else:
            return_object['status'] = 1
            return_object['data'] = result

        return return_object

    def reverse(self, array):
        arr = array
        reverse = {v: k for k, v in arr.items()}
        return reverse

    def id_types(self, type=''):
        types = {
            "cs": "000",
            "admin": "001",
            "vendor": "002",
            "user": "003",
            "store": "004",
            "employee": "006",
            "order": "007",
            "category": "008",
            "item": "009",
            "round": "010",
            "prize": "011",
            "lucky_coupon": "012",
            "corporate": "013",
            "store_item": "014",
            "transaction": "015",
            "cron": "016",
        }
        if type:
            return types[type]
        else:
            return types

    def id_info(self, user_id, info_name='type'):
        try:
            type = user_id[0:3]
            sl = user_id[3:18]
            option = user_id[18:36]
            arr = {"type": type, "sl": sl, "option": option}
            result = arr[info_name]
        except Exception:
            return False
        else:
            return result

    def id_type(self, type_name):
        try:
            arr = self.id_types()
            type = arr[type_name]
        except Exception:
            return False
        else:
            return type

    def id_type_name(self, user_id):
        try:
            id_type = self.id_info(user_id, 'type')
        except Exception:
            return False

        try:
            reverse_arr = self.reverse(self.id_types())
        except Exception:
            return False

        try:
            type_name = reverse_arr[id_type]
        except Exception:
            return False
        else:
            return type_name

    def transaction_type(self, type):
        types = {
            "point_exchange": "1",
            "member_position": "2",
            "recharge": "3",
            "product_sale": "4",
            "top_up": "5",
            "vendor_withdraw": "6",
            "user_withdraw": "7",
            "vendor_top_up": "8",
            "product_buy": "9",
        }
        return types[type]

    def transaction_maker(self, request, tr_type, more_data={}, update=False):
        try:
            logged_user_id = request.session['logged_user_id']
            type_code = self.transaction_type(tr_type)
            model = Transaction()
            collection = model.collection_name()
            id_maker = self.IdMaker("transaction", type_code)
            transaction_id = id_maker.user_id
            if not transaction_id:
                return False
            request_data = {
                "transaction_id": transaction_id,
                "user_id": logged_user_id,
                "status": 0,
                "time": time,
                "type": tr_type,
            }
            request_data.update(more_data)
            send_transaction_data = collection.insert_one(request_data)
            if update:
                if send_transaction_data:
                    update_last = id_maker.update()
                    if not update_last.status:
                        return False
                else:
                    return False
        except:
            return False
        else:
            return id_maker

    # ****************** Id maker nested class start *****************
    class IdMaker:
        def __init__(self, type_name, option=''):
            self.type_name = type_name
            self.option = option
            self.parent = Common()
            self.user_id = ''
            return_object = {
                "status": 0,
                "module_name": "IdMaker"
            }
            total_amount_count = 0
            try:
                parent = self.parent
                total_amount_count = parent.total_amount(type_name, '') + 1

                # Initiate total counter for next use
                self.total_controller = total_amount_count

                str_total_amount = str(total_amount_count)
                first_part = parent.id_type(type_name)
                second_part = str_total_amount.zfill(15)
                if option:
                    if len(option) == 36:
                        id_type = parent.id_info(option, "type")
                        id_sl = parent.id_info(option, "sl")
                        option = id_type+id_sl
                third_part = option.zfill(18)
                user_id = ''.join([first_part, second_part, third_part])

                # Initiate user id for result
                self.user_id = user_id

            except Exception as e:
                return_object['total_' + type_name] = total_amount_count
                return_object['expect_error'] = str(e)
                return_object['error'] = "User id not generated"
            else:
                return_object['user_id'] = user_id
                return_object['status'] = 1

                # Initiate error object and update availability
            self.return_object = return_object
            self.status = return_object['status']

        def error(self):
            return self.return_object

        def update(self):
            type_name = self.type_name
            option = self.option
            total_amount_count = self.total_controller
            last_update_amount = self.parent.Update(type_name, '', total_amount_count)
            return last_update_amount

    class Update:
        def __init__(self, type_name, option, now_total):
            self.type_name = type_name
            self.option = option
            self.parent = Common()
            self.total_controller = now_total
            return_object = {
                "status": 0,
                "module_name": "Update"
            }
            try:
                parent = self.parent
                controller_type = self.type_name
                now_total = self.total_controller
                update_data = {
                    "type": controller_type,
                    "amount": now_total,
                    "option": option
                }
                update_last_amount = parent.AssignLastUpdate(update_data)
                if update_last_amount.status:
                    return_object['status'] = 1
                else:
                    return_object['error'] = 'Last update fail'
                    return_object['status'] = 0
                    return_object['last_update_data'] = update_last_amount
            except Exception as e:
                return_object['last_update_error'] = str(e)

            # Initiate error
            # Initiate status for result
            self.status = return_object['status']
            self.return_object = return_object

        def error(self):
            return self.return_object

    def last_update(self, info_type, option=""):
        model = LastUpdate()
        collection = model.collection_name()
        query = {"type": info_type}
        if option:
            query["option"] = option
        data = collection.find_one(query, {"_id": 0})
        return data

    def total_amount(self, info_type, option=''):
        count = 0
        data = self.last_update(info_type, option)
        if data:
            count = data['amount']
        return count

    class UserNameMaker:
        def __init__(self, name):
            self.name = name
            self.parent = Common()
            self.user_name = ''
            self.keyword = ''
            self.now_amount = 0
            # username is 8-30 characters long
            # no _ or . at the beginning
            # no __ or _. or ._ or .. inside
            # allowed characters [a-zA-Z0-9]
            # no _ or . at the end
            return_object = {
                "status": 0,
                "module_name": "UserNameMaker"
            }
            info_type = "user_name"
            self.info_type = info_type
            parent = self.parent
            try:

                split = name.split(' ')
                valid_parts = []
                for part in split:
                    legal_characters = '^(?=.{0,30}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$'
                    match = re.match(legal_characters, part)
                    if match:
                        valid_parts.append(part)
                        user_name = '-'.join(valid_parts)
                        find = parent.last_update(info_type, user_name)
                        if not find:
                            break

                user_name = '-'.join(valid_parts)
            except Exception as e:
                return_object['error'] = 'User name maker'
                return_object['exception'] = str(e)
            else:
                # Check the valid part available or not
                if user_name:
                    amount = 1
                    find = parent.last_update(info_type, user_name)
                    self.keyword = user_name
                    if find:
                        amount = find['amount']+1
                        user_name = user_name+str(amount)
                    self.now_amount = amount
                else:
                    user_name = parent.shuffle(time_unique_id)
                    self.keyword = user_name
                return_object['status'] = 1
                self.user_name = user_name

            # Initiate status and error object
            self.status = return_object['status']
            self.return_object = return_object

        def error(self):
            return self.return_object

        def update(self):
            info_type = self.info_type
            keyword = self.keyword
            now_amount = self.now_amount
            assign_document = {"type": info_type, "option": keyword, "amount": now_amount}
            assign_username_keyword = self.parent.AssignLastUpdate(assign_document)
            return assign_username_keyword

    class AssignLastUpdate:
        def __init__(self, document):
            self.document = document
            return_object = {
                "status": 0,
                "module_name": "AssignLastUpdate"
            }
            model = LastUpdate()
            collection_name = model.collection_name()
            try:
                query = {"type": document['type'], "option": document['option']}
                compare = model.compare(document)
            except Exception as e:
                return_object['error'] = "Compare error in assign last update"
                return_object['exception'] = str(e)
            else:
                if compare:
                    try:
                        update = collection_name.update_one(query, {"$set": document}, upsert=True)
                    except Exception as e:
                        return_object['error'] = "last update assign problem"
                        return_object['exception'] = str(e)
                    else:
                        return_object['status'] = 1
                        return_object['message'] = "Last update document updated."

            # Initiate status and error object for next user
            self.return_object = return_object
            self.status = return_object['status']

        def error(self):
            return self.return_object

    def assign_last_update(self, info_type, option, more_info={}):
        document = {"type": info_type, "option": option}
        if more_info:
            document.update(more_info)
        update = self.AssignLastUpdate(document)
        update_status = update.status
        if update_status:
            return True
        else:
            return False

    class LastUpdate:
        def __init__(self, info_type, option=''):
            self.info_type = info_type
            self.option = option
            self.parent = Common()
            common = self.parent
            update_info = common.last_update(info_type, option)
            amount = 0
            if update_info:
                amount = update_info['amount']
            self.amount = amount+1
            self.info = update_info

        def update(self):
            info_type = self.info_type
            option = self.option
            now_amount = self.amount
            assign_document = {"type": info_type, "option": option, "amount": now_amount}
            assign = self.parent.AssignLastUpdate(assign_document)
            return assign

    def assign_install_collection(self, document):
        return_object = {
            "status": 0
        }
        model = InstalledCollection()
        collection_name = model.collection_name()
        query = {"collection_name": document['collection_name']}
        try:
            compare = model.compare(document)
        except Exception as e:
            return_object['error'] = "Compare error in assign install collection"
            return_object['exception'] = str(e)
        else:
            if compare:
                try:
                    update = collection_name.update_one(query, {"$set": document}, upsert=True)
                except Exception as e:
                    return_object['error'] = "assign install collection not updated"
                    return_object['exception'] = str(e)
                else:
                    return_object['status'] = 1
                    return_object['message'] = "Assign install collection updated"
        return return_object

    def user_id_update(self, return_object, id_maker):
        errors = {}
        # First check any error available else update
        if not return_object['error'] and not return_object['message']:

            # First update total amount controller. Because its safe for id generate.
            # (if insert fail but generate id not duplicate anyway)(if last update fail and insert new data,
            # generate id duplicate possible.)
            try:
                total_user_update = id_maker.update()
                status = total_user_update.status
                if not status:
                    errors['user_total_last_update_errors'] = total_user_update.error()
            except Exception as e:
                errors['total_user_last_update'] = "Last update of " + id_maker.type_name + " fail!"
                errors['total_user_last_update_exception'] = str(e)
        return errors

    def file_upload(self, request, field_name, multiple=False):
        images = []
        if field_name in request.FILES:
            try:
                if multiple:
                    files = request.FILES.getlist(field_name)
                    for file in files:
                        my_file = file
                        fs = FileSystemStorage()
                        location = 'images/' + time_folder
                        filename = fs.save(location + my_file.name, my_file)
                        uploaded_file_url = fs.url(filename)
                        images.append(uploaded_file_url)
                    return images
                else:
                    my_file = request.FILES[field_name]
                    fs = FileSystemStorage()
                    location = 'images/'+time_folder
                    filename = fs.save(location + my_file.name, my_file)
                    uploaded_file_url = fs.url(filename)
            except Exception:
                if multiple:
                    return images
                else:
                    return False
        else:
            if multiple:
                return images
            else:
                return False

        return uploaded_file_url

    def exist(self, collection_name, document):
        collection = self.db[collection_name]
        find = collection.find_one(document)
        return find

    def logout(self, request):
        return_object = {}
        try:
            request.session.clear()
        except Exception:
            return_object['error'] = "Logout problem"
        else:
            return_object['logout'] = "Cookie clear user id"

        return return_object

    def power_undo(self, request):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        logged_user_id = vendor_id = store_id = ""
        try:
            logged_user_id = request.session['logged_user_id']
        except:
            pass
        try:
            vendor_id = request.session['vendor_id']
        except:
            pass
        try:
            store_id = request.session['store_id']
        except:
            pass

        try:
            request.session.clear()
        except Exception:
            return_object['error'] = "Power undo problem"
        else:
            logged_user_type = self.id_type_name(logged_user_id)
            if logged_user_type == "employee":
                try:
                    request.session['vendor_id'] = vendor_id
                except:
                    errors['store_error'] = "Vendor id should be removed"
            try:
                request.session['logged_user_id'] = logged_user_id
            except:
                errors['session_error'] = "Cookie problem"
            else:
                return_object['status'] = 1
            return_object['power_undo'] = "Cookie clear "

        return return_object

    def md5(self, string):
        hp = hashlib.md5(string.encode('utf-8'))
        string = hp.hexdigest()
        return string

    def json_find_data(self, data):
        json_array = []
        find_data = {"find_data": json_array}
        for item in data:
            json_array.append(item)
        return find_data

    def collection_data(self, collection_name, qurey={}, option={}, limit=0, skip=0, count=False):
        collection = self.db[collection_name]
        option["_id"] = 0
        if count:
            find = self.documents_counter(collection_name, qurey)

        else:
            find = collection.find(qurey, option).limit(limit).skip(skip)
            find = self.json_find_data(find)
        return find

    def documents_counter(self, collection_name, qurey={}):
        return_data = {}
        collection = self.db[collection_name]
        count = collection.count_documents(qurey)
        return_data['total'] = count
        return return_data

    def collection_data_one(self, collection_name, qurey={}, option={}):
        collection = self.db[collection_name]
        option["_id"] = 0
        find = collection.find_one(qurey, option)
        return find

    def aggregate_collection_data(self, collection_name, pipeline, limit=0, skip=0, count=False):
        collection = self.db[collection_name]
        if count:
            count_object = {
                "$group": {
                    "_id": "null",
                    "count": {"$sum": 1}
                }
            }
            pipeline.append(count_object)
        else:
            if limit:
                pipeline.append({"$skip": skip})
                pipeline.append({"$limit": limit})

        find = collection.aggregate(pipeline)
        clean_data = self.json_find_data(find)
        if count:
            return_data = {}
            count = 0
            if clean_data['find_data']:
                count = clean_data['find_data'][0]['count']
            return_data['total'] = count
            return return_data
        return clean_data

    def aggregate_collection_data_count(self, collection_name, pipeline, limit):
        collection = self.db[collection_name]
        count_object = {
            "$group": {
                "_id": "null",
                "count": {"$sum": 1}
            }
        }
        pipeline.append(count_object)
        if limit:
            pipeline.append({"$limit": limit})

        find = collection.aggregate(pipeline)
        clean_data = self.json_find_data(find)
        return_data = {}
        count = 0
        if clean_data['find_data']:
            count = clean_data['find_data'][0]['count']
        return_data['total'] = count
        return return_data

    def aggregate_collection_more_data(self, collection_name, pipe_line):
        self.preview(pipe_line)
        collection = self.db[collection_name]
        find = collection.aggregate(collection_name, pipe_line)
        clean_data = self.json_find_data(find)
        return clean_data

    def document_delete_one(self, collection_name, query):
        collection = self.db[collection_name]
        delete = collection.delete_one(query)
        return delete

    def document_delete(self, collection_name, query):
        collection = self.db[collection_name]
        delete = collection.delete_many(query)
        return delete

    def preview(self, content):
        print("********************************** Preview data ******************************************")
        print("    *******************************************************************************       ")
        print("        ************************************************************************       ")
        print(content)
        print("        ************************************************************************       ")
        print("    *******************************************************************************       ")
        print("************************************** End ***********************************************")

    def file_delete(self, image_list):
        errors = {}
        try:
            for del_image in image_list:
                del_image = del_image[1:len(del_image)]
                # Check this file default or not
                split_path = del_image.split('/')
                root_name = split_path[1]
                if root_name != "default":
                    file_path = base_bir + del_image
                    if os.path.exists(file_path):
                        os.remove(del_image)

        except Exception as e:
            errors['image_delete_exception'] = str(e)
            errors['image delete'] = "Image delete problem"
        return errors

    def activate(self, request, activate_id):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages
        }
        try:
            logged_user_id = ''
            try:
                logged_user_id = request.session['logged_user_id']
            except:
                messages['login_required'] = "Login first"
            id_info = {}
            id_type_name = self.id_type_name(activate_id)
            if id_type_name == 'vendor':
                request.session['vendor_id'] = activate_id
                # last activities
                last_activities = self.last_activities(request)
                return_object.update(last_activities)
                try:
                    del request.session['store_id']
                except:
                    pass

                id_info = self.collection_data_one("controllers", {"user_id": activate_id})
            elif id_type_name == 'store':
                try:
                    logged_user_id = request.session['logged_user_id']
                except:
                    errors['login_required'] = "Login first"
                else:
                    logged_id_type_name = self.id_type_name(logged_user_id)

                    if logged_id_type_name != "vendor":
                        try:
                            vendor_id = request.session['vendor_id']
                        except Exception as e:
                            messages['undefined_vendor'] = "Chose a vendor first!"
                            return return_object
                    request.session['store_id'] = activate_id
                    self.assign_last_update("last_store", logged_user_id, {"value": activate_id})
                    id_info = self.collection_data_one("stores", {"store_id": activate_id})

            elif id_type_name == "employee":
                request.session['employee_id'] = activate_id
                id_info = self.collection_data_one("controllers", {"user_id": activate_id})
            if id_type_name == "vendor" or id_type_name == "employee":
                stores = id_info['stores']
                store_details = self.collection_data("stores", {"store_id": {"$in": stores}}, {"store_name": 1, "store_id": 1})
                if store_details['find_data']:
                    stores = store_details['find_data']
                    return_object['stores'] = stores

        except Exception as e:
            errors['activate_exception'] = str(e)
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1
                return_object['id_type'] = id_type_name
                return_object['id_info'] = id_info
                return_object['activate_id'] = activate_id
        return return_object

    def barcode_generator(self, bar_code):
        errors = {}
        messages = {}
        return_object = {
            "status": 0,
            "error": errors,
            "message": messages,
            "bar_code_image": ''
        }
        try:
            code = barcode.get_barcode_class('code128')
            code_128 = code(u'"'+bar_code+'"', writer=ImageWriter())
            path = "/bar-codes/"+time_folder
            save_path = settings.MEDIA_ROOT+path
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            i = 1
            image_name = bar_code
            while i > 0:
                file_path = save_path + image_name+".png"
                if not os.path.exists(file_path):
                    break
                else:
                    new_str = self.random_string()
                    image_name += "_"+new_str
                i -= 1

            code_128.save(save_path + image_name)
            barcode_image = "/media"+path+image_name+".png"

        except Exception as e:
            errors['barcode_generator_exception'] = str(e)
        else:
            # First check any error available
            if not return_object['error'] and not return_object['message']:
                return_object['status'] = 1
                return_object['bar_code'] = {
                    "code": bar_code,
                    "image": barcode_image
                }
                return_object['bar_code_image'] = barcode_image
        return return_object

    def error_info(self):
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        error_object = {
            "file_name": fname,
            "error_line_number": exc_tb.tb_lineno,
            "error_type": str(exc_type),
            "error_name": str(exc_obj)
        }
        return error_object

    def data_export_to_excel(self, object_list, file_name, sheet_name="Sheet 1"):
        file_info = {}
        if object_list:
            try:
                wb = Workbook()
                ws = wb.active
                i = 0
                first_object = object_list[0]
                key_names = {}
                for key, value in first_object.items():
                    key_names[key] = key.replace("_", " ").capitalize()
                object_list.insert(0, key_names)

                for x in object_list:
                    j = 0
                    columns = []
                    for value in x.values():
                        columns.append(value)
                        j += 1
                    # insert single row
                    ws.append(columns)
                    i += 1
                #
                save_path_info = self.save_path_ready("exports/", file_name, True)
                if save_path_info:
                    save_path = save_path_info['save_path']
                    wb.save(save_path)
                    file_info = save_path_info
            except:
                self.preview(self.error_info())
                pass
        return file_info

    def save_path_ready(self, path, file_name, replace=False):
        file_info = {}
        try:
            file_path = ""
            path = "/" + path + time_folder
            save_path = settings.MEDIA_ROOT + path
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            i = 1
            file = file_name
            while i > 0:
                file_path = save_path + file
                # If replace permission false
                if not replace:
                    if not os.path.exists(file_path):
                        break
                    else:
                        new_str = "_"+self.random_string()
                        # Split the extension
                        split = file.split(".")
                        just_name = ""
                        name_range = len(split)-1
                        extension = split[len(split)-name_range]
                        for x in range(name_range):
                            just_name += split[x]
                        new_name = just_name + new_str + "." + extension
                        file = new_name
                    i -= 1
                else:
                    break
            just_path = "/media" + path
            view_path = just_path + file
            file_path = save_path + file
            file_info = {
                "file_name": file,
                "path": just_path,
                "view_path": view_path,
                "save_path": file_path
            }
        except:
            self.preview(self.error_info())
            pass
        return file_info

    def last_activities(self, request):
        return_object = {}
        logged_user_id = ""
        try:
            logged_user_id = request.session['logged_user_id']
        except:
            pass
            return return_object
        try:
            vendor_id = ""
            # Check before used store id and if available save automatic this store activate
            last_store = self.last_update("last_store", logged_user_id)
            if last_store:
                store_id = last_store['value']
                query = {"store_id": store_id}
                logged_id_type_name = self.id_type_name(logged_user_id)
                if logged_id_type_name == "admin":
                    try:
                        vendor_id = request.session['vendor_id']
                    except:
                        pass
                    query["vendor_id"] = vendor_id
                store_info = self.collection_data_one("stores", query)
                if store_info:
                    return_object['store_id'] = store_id
                    return_object['store_info'] = store_info
                    request.session['store_id'] = store_id
        except:
            pass

        return return_object

    def login_validation(self, request):
        messages = {}
        try:
            logged_user_id = request.session['logged_user_id']
        except:
            messages['login_required'] = "login first"

        return messages

    def point_maker(self, item, quantity=0):
        sale = item['sale']
        price = item['price']
        purchase = item['purchase']
        sale_rule = self.sale_point_rule()
        exact_price = rule_point = 0
        if sale_rule:
            rule_point = sale_rule['amount']
            exact_price = 0
            if sale:
                exact_price = sale
            else:
                exact_price = price
            if quantity:
                quantity = 1
            else:
                quantity = item['quantity']

            profit = exact_price * quantity - purchase * quantity
            point = profit * rule_point
        else:
            point = 0
        return point

    def sale_point_rule(self):
        sale_rule = self.collection_data_one("point_rules", {"id": "1"})
        return sale_rule

    def line_total(self, item):
        price = item['price']
        sale = item['sale']
        vat = item['vat']
        quantity = item['quantity']
        # for gather rice grand total
        line_total = 0
        if item['sale']:
            line_total = item['sale'] * item['quantity']
        else:
            line_total = item['price'] * item['quantity']
        line_vat = line_total * (vat / 100)
        line_total_with_vat = line_total + line_vat
        return line_total_with_vat

    def object_size(self, ob):
        size = len(bson.BSON.encode(ob)) / 1000000
        return size

    def total_tree(self, pipeline):
        total_document = self.aggregate_collection_data("tree", pipeline, 0, 0, True)
        return total_document['total']

    def user_point_add(self, user_id, amount):
        try:
            model = Tree()
            collection = model.collection_name()
            user_info = self.collection_data_one("tree", {"user_id": user_id})
            if not user_info:
                return True
            else:
                active_expire = user_info['active_expire']
                position_expire = user_info['expire']
                if active_expire < time:
                    return True
                elif position_expire < time:
                    return True
            try:
                amount = int(amount)
            except:
                return False
            else:
                where = {
                    "user_id": user_id
                }
                which = {
                    "$inc": {
                        "distributed_point": amount,
                        "point_balance": amount
                    }

                }
                update = collection.update_one(where, which)
                if not update:
                    return False
        except:
            # self.preview(self.error_info())
            return False
        else:
            return True

    def item_stock_update(self, item_id, amount, update_type = "in"):
        try:
            store_item_model = StoreItem()
            collection = store_item_model.collection_name()
            where = {
                "store_item_id"
            }
            which = {
                "$inc": {
                    "quantity": amount
                }
            }
            collection.update_one(where, which)

        except:
            return False
        else:
            return True

    def point_add_validity(self, user_id):
        try:
            find = self.collection_data_one("tree", {"user_id": user_id})
            if find:
                return True
            else:
                return False
        except:
            return False

    def order_total_info(self, items, extra_discount=0):
        sub_total = 0
        total = 0
        vat = 0
        point = 0
        discounts = []

        grand_total = 0
        store_wise_info = {}
        total_item = len(items)
        for x in items:
            item = x['item_info']
            # Store info
            store_id = x['store_id']
            if store_id not in store_wise_info:
                store_wise_info[store_id] = {
                    'grand_total': 0,
                    'sub_total': 0,
                    'vat': 0,
                    'extra_discount': 0,
                    'total_discount': 0,
                    'total_item': 0,
                    'discounts': [],
                    'point': 0,
                }
            price = item['price']
            item_id = item['item_id']
            sale = item['sale']
            item_vat = item['vat']
            quantity = item['quantity']
            item_point = item['point']
            # for gather rice grand total
            line_total = 0
            if sale:
                line_total = sale * quantity

            else:
                line_total = price * quantity

            line_vat = line_total * (item_vat / 100)
            vat += line_vat
            sub_total += line_total
            point += item_point
            # put the current info into store wise total info
            store_sub_total = store_wise_info[store_id]['sub_total']
            store_wise_info[store_id]['sub_total'] = store_sub_total + line_total

            store_vat = store_wise_info[store_id]['vat']
            store_wise_info[store_id]['vat'] = store_vat + item_vat

            store_point = store_wise_info[store_id]['point']
            store_wise_info[store_id]['point'] = store_point + item_point

            store_total_item = store_wise_info[store_id]['total_item']
            store_wise_info[store_id]['total_item'] = store_total_item + 1

            now_store_sub_total = store_wise_info[store_id]['sub_total']
            now_store_vat_total = store_wise_info[store_id]['vat']
            store_wise_info[store_id]['grand_total'] = now_store_sub_total + now_store_vat_total

            if sale:
                price_line_total = price * quantity
                price_line_vat = price_line_total * (item_vat / 100)
                this_discount = (price_line_total+price_line_vat)-(line_total+line_vat)
                item_in_ob = {
                    "item_name": item['item_name'],
                    "discount": this_discount,
                    "item_id": item_id,
                }
                discounts.append(item_in_ob)
                store_discounts = store_wise_info[store_id]['discounts']
                store_discounts.append(item_in_ob)
        total = sub_total + vat - extra_discount
        return_object = {
            'grand_total': total,
            'extra_discount': extra_discount,
            'sub_total': sub_total,
            'vat': vat,
            'point': point,
        }
        total_item_discount = 0
        for x in discounts:
            total_item_discount += x['discount']

        return_object['total_discount'] = total_item_discount + extra_discount
        return_object['total_item'] = total_item
        return_object['discounts'] = discounts
        return_object['stores_total'] = store_wise_info

        return return_object

    def user_expire_manager(self, customer_id, grand_total):
        try:
            tree_info = self.collection_data_one("tree", {"user_id": customer_id})
            if tree_info:
                position = tree_info['position']
                if position != "Top":
                    current_expire = tree_info['expire']
                    # update tree information expire time for this customer
                    expire_rule = self.estimate_expire_rule(grand_total)

                    if expire_rule:
                        expire_rule = expire_rule['value']['expire']
                        rule_day = expire_rule['day']
                        rule_hour = expire_rule['hour']
                        rule_minute = expire_rule['minute']
                        next_expire = after_time(rule_day, rule_hour, rule_minute)

                        if next_expire > current_expire:
                            tree_model = Tree()
                            tree_collection = tree_model.collection_name()
                            tree_where = {"user_id": customer_id}
                            tree_which = {
                                "$set": {
                                    "expire": next_expire
                                }
                            }
                            tree_collection.update_one(tree_where, tree_which)
        except:
            return False
        else:
            return True

    def estimate_expire_rule(self, amount):
        pipeline = [
            {
                "$project": {
                    "_id": 0,
                }
            },
            {
                "$match": {
                    "type": "expire_date_rules"
                }
            },
            {
                "$unwind": "$value"
            },
            {
                "$match": {
                    "value.amount": {
                        "$lte": amount
                    }
                }
            },
            {
                "$project": {
                    "option": 0,
                    "type": 0,
                }
            }
        ]
        find = self.aggregate_collection_data("last_updates", pipeline)
        if find['find_data']:
            find = find['find_data'][0]
        else:
            find = {}
        return find

    def required(self, v_type, data):
        if v_type:
            if not data and data != 0:
                return False
            else:
                return True
        return True

    def validation(self, v_type, data, mobiles=[]):
        return_object = {}
        if v_type:
            if v_type == "int":
                try:
                    data = int(data)
                except:
                    return_object['message'] = "Enter a valid numeric amount"
            elif v_type == "time":
                pass
            elif v_type == "position":
                if data != "Left" and data != "Right" and data != "Top":
                    return_object['message'] = "Position format not valid enter..(Left,Right,Top)"
            elif v_type == "status":
                if data != "Free" and data != "Paid" and data != "":
                    return_object['message'] = "Status format not valid enter..(Free,Paid)"
            elif v_type == "referer":
                if data:
                    if data not in mobiles:
                        return_object['message'] = "Referer not found"
            elif v_type == "up_line_referer":
                if data:
                    if data not in mobiles:
                        return_object['message'] = "Up line referer not found"
        return return_object

    def new_user_id(self, total_amount_count, id_type='user'):
        str_total_amount = str(total_amount_count)
        first_part = self.id_type(id_type)
        second_part = str_total_amount.zfill(15)
        option = ""
        third_part = option.zfill(18)
        user_id = ''.join([first_part, second_part, third_part])
        return user_id

    def send_mail(self, from_email, to_email, subject, message):
        sg = sendgrid.SendGridAPIClient(apikey="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")
        from_email = Email(from_email)
        to_email = Email(to_email)
        subject = subject
        m_content = Content("text/html", message)
        m_mail = Mail(from_email, subject, to_email, m_content)
        response = sg.client.mail.send.post(request_body=m_mail.get())
        return response


