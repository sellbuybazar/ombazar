<div id="page_content">

    <?php
    if(!isset($data)){
        $pars_url=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }
    else{
        $pars_url=$data;
    }
    $divi=explode("/",$pars_url);
    if(isset($divi['3'])){
        $id=$divi['3'];
        $info=$retrive->store_item_info($id,true);

    ?>
            <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
                <h1><?php echo $info["item_name"];?></h1>
                <span class="uk-text-muted uk-text-upper uk-text-small"><?php echo $info['item_code']?></span>
            </div>
            <div id="page_content_inner">

                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Photos
                                </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-slidenav-position" data-uk-slider="{infinite:false}">

                                    <div class="uk-slider-container">
                                        <ul class="uk-slider uk-grid uk-grid-small uk-grid-width-medium-1-1 uk-grid-width-small-1-1">
                                            <?php
                                                $images=$info['images'];
                                                foreach ($images as $image_info){
                                            ?>
                                            <li style="max-height: 200px"><img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $image_info['image']?>" alt=""></li>
                                            <?php } ?>
                                        </ul>
                                    </div>

                                    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
                                    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Details
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium">
                                    <div class="uk-width-large-1-2">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">Product Name</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle"><a href="<?php echo SITE_URL;?>product_details/<?php echo $info['item_name']?>/<?php echo $info['id']?>"><?php echo $info['item_name']?></a></span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small">Price</span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <span class="uk-text-large uk-text-middle"><?php echo $info['price']?> <?php echo $retrive->currency_symbol()?></span>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <?php
                                            $specifications=$info['specifications'];
                                            foreach ($specifications as $specification){
                                        ?>
                                        <div class="uk-grid uk-grid-small">
                                            <div class="uk-width-large-1-3">
                                                <span class="uk-text-muted uk-text-small"><?php echo $specification['specification_name']?></span>
                                            </div>
                                            <div class="uk-width-large-2-3">
                                                <?php echo $specification['val']?>
                                            </div>
                                        </div>
                                        <hr class="uk-grid-divider">
                                        <?php } ?>
                                    </div>
                                    <div class="uk-width-large-1-2">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Short description</span>
                                            <?php echo $info['short_description'];?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Description
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <?php echo $info['description'];?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>




    <?php }?>

</div>
