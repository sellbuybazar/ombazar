<div id="jas-content">
    <div class="jas-container">
        <div class="jas-row jas-page">
            <div class="jas-col-md-12 jas-col-xs-12 mt__60 mb__60" role="main" itemscope="itemscope"
                 itemtype="http://schema.org/CreativeWork">
                <div class="woocommerce">
                    <div class="jas-container login-form">


                        <div class="jas-row u-columns col2-set" id="customer_login">


                            <div class="tabs">
                                <ul class="tabs__items">
                                    <li class="tabs__item tabs_active">Login</li>
                                    <li class="tabs__item">Registration</li>
                                </ul>
                                <div class="tabs__content-wrapper">
                                    <div class="tabs__content tabs_active">
                                        <div class="jas-col-md-6 jas-col-sm-6 jas-col-xs-12 u-column1 col-1">

                                            <div class="form-login">
<!--                                                <h2>Login</h2>-->

                                                <form method="post" class="woocommerce-form woocommerce-form-login login">


                                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                        <label for="username">Username or Mobile number <span
                                                                    class="required">*</span></label>
                                                        <input class="woocommerce-Input woocommerce-Input--text input-text"
                                                               name="username" id="username" autocomplete="username" value=""
                                                               type="text"></p>
                                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                        <label for="password">Password <span class="required">*</span></label>
                                                        <input class="woocommerce-Input woocommerce-Input--text input-text"
                                                               name="password" id="password" autocomplete="current-password"
                                                               type="password">
                                                    </p>


                                                    <p class="form-row">
                                                        <input id="_wpnonce" name="_wpnonce" value="ed4c651438" type="hidden"><input
                                                                name="_wp_http_referer" value="/e-theme/my-account/" type="hidden">
                                                        <button type="submit" class="woocommerce-Button button fr" name="login"
                                                                value="Log in">Log in
                                                        </button>

                                                        <span class="inline"><span class="style-checkbox">
                                            <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" id="rememberme" value="forever" type="checkbox"> <span>Remember me</span>
                                            </span>
                                            </span>
                                                    </p>
                                                    <p class="woocommerce-LostPassword lost_password">
                                                        <a href="http://localhost/e-theme/my-account/lost-password/">Lost your
                                                            password?</a>
                                                    </p>


                                                </form>
                                            </div>


                                        </div>

                                    </div>
                                    <div class="tabs__content">
                                        <div class="jas-col-md-6 jas-col-sm-6 jas-col-xs-12 u-column2 col-2">

<!--                                            <h2>Register</h2>-->

                                            <form method="post" class="register">

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_email">Mobile number <span class="required">*</span></label>
                                                    <input class="woocommerce-Input woocommerce-Input--text input-text" name="email"
                                                           id="reg_email" autocomplete="email" value="" type="email"></p>
                                                <div class="jas-row">
                                                    <div class="jas-col-md-6">
                                                        <label for="country">Country <span class="required">*</span></label>
                                                        <select class="woocommerce-input woocommerce-Input--text input-text" id="country">
                                                            <option value="">Select country</option>
                                                            <option value="Bangladesh">Bangladesh</option>
                                                            <option value="America">America</option>
                                                        </select>
                                                    </div>
                                                    <div class="jas-col-md-6">
                                                        <label for="zip">Zip code <span class="required">*</span></label>
                                                        <input class="woocommerce-Input  woocommerce-Input--text input-text" id="zip" name="email" value="" type="text">
                                                    </div>
                                                </div>
                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="referel">Referel id <span class="required"></span></label>
                                                    <input class="woocommerce-Input input-text" name="referel"
                                                           id="referel"  value="" type="text"></p>

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_password">Password <span class="required">*</span></label>
                                                    <input class="woocommerce-Input woocommerce-Input--text input-text"
                                                           name="password" id="reg_password" type="password">
                                                </p>


                                                <!-- Spam Trap -->
                                                <div style="left: -999em; position: absolute;"><label
                                                            for="trap">Anti-spam</label><input name="email_2" id="trap"
                                                                                               tabindex="-1" type="text"></div>

                                                <div class="woocommerce-privacy-policy-text"></div>
                                                <p class="woocommerce-FormRow form-row">
                                                    <input id="woocommerce-register-nonce" name="woocommerce-register-nonce"
                                                           value="1e5647447e" type="hidden"><input name="_wp_http_referer"
                                                                                                   value="/e-theme/my-account/"
                                                                                                   type="hidden">
                                                    <button type="submit" class="woocommerce-Button button fr" name="register"
                                                            value="Register">Register
                                                    </button>
                                                </p>


                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <!-- $classes -->

        </div>
        <!-- .jas-row -->
    </div>
</div>
