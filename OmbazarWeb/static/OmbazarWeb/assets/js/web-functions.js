// OmbazarFn = {}; not open this this is functions object name you can get any function into the object.

OmbazarFn.$http = function($http,data,event,loading = true){
    if(loading){
        OmbazarFn.loading_on();
    }
    // When any form submit first disabled all button in form element
    $("form button").addClass("uk-disabled");
    $http({
        method : "POST",
        url : "/request/",
        data : data,
        headers: { 'Content-Type': undefined}
    }).then(
        function (response) {
            let data = response.data;
            let status = response.status;
            if (loading){
                OmbazarFn.loading_off();
            }

            OmbazarFn.informer(data);
            event(data,status);
            $("form button").removeClass("uk-disabled");
        },
        function(response) {
            if (loading){
                OmbazarFn.loading_off();
            }
              console.log(response);
            $("form button").removeClass("uk-disabled");
        })
};

OmbazarFn.route_info = function ($rootScope,$location) {
    $rootScope.path = $location.path();
    let path = $rootScope.path;
    let path_text = path.substr(1, path.length);
    let split = path_text.split("/");

    let info = [
        {
            name: "Home",
            url: "",
            anchor: true
        },
    ];
    if(split !== undefined && split.length){
        for (let x in split){
            let first_char = path_text.substr(0,1).toUpperCase();
            let path_name = first_char + path_text.substr(1, path_text.length);
                path_name = split[x];
                path_name = path_name.replace(/-/g," ");

            info.push({
                name: path_name,
                url: path_text,
                anchor: true
            });
        }

    }
    return info;
};

OmbazarFn.tree_maker = function(user_id, tree_data,key_name="up_line_referer"){
    let parent_tree_data = OmbazarFn.custom_filter(tree_data,key_name,user_id,true);
    let left_child = OmbazarFn.custom_filter(parent_tree_data,"position","Left");
    let right_child = OmbazarFn.custom_filter(parent_tree_data,"position","Right");
    parent_tree_data = [];
    if (left_child !== undefined){
        parent_tree_data.push(left_child);
    }
    if (right_child !== undefined){
        parent_tree_data.push(right_child);
    }

    let tree_data_list = [];
    for (let x in parent_tree_data){
        let item = parent_tree_data[x];
        let user_id = item.user_id;
        let new_tree = OmbazarFn.tree_maker(user_id,tree_data,key_name);
        // if(new_categories.length){
            item['tree'] = new_tree;
            item['child'] = 1;
            tree_data_list.push(item);
        // }
    }
    return tree_data_list;
};


OmbazarFn.set_tree_position = function ($http,data_object,event) {
    let function_name = "set_tree_position(request)";
    let extra_data = OmbazarFn.passed_request_object(function_name);
        extra_data = OmbazarFn.merge(extra_data,data_object);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
         event(response,status);
    });
};
OmbazarFn.exchange_position = function($http,data_object,event){
    let function_name = "exchange_tree_position(request)";
    let extra_data = OmbazarFn.modify_request_object(function_name);
        extra_data = OmbazarFn.merge(extra_data,data_object);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
         event(response,status);
    });
};
OmbazarFn.get_tree = function($http,parent_tree_info,event,more_data={},tree_model=true){
    let function_name = "tree(request)";
    let extra_data = OmbazarFn.retrive_request_object(function_name);
        extra_data = OmbazarFn.merge(extra_data,more_data);
    let form_data = OmbazarFn.form_data_maker("",extra_data);
    OmbazarFn.$http($http,form_data,function (response,status) {
        // console.log(response);
        let tree_data = response.tree_data.find_data;
        let parnet_user_id = parent_tree_info.user_id;
        let tree = new Object();
            Object.assign(tree,parent_tree_info);
        if (tree_model){
            tree_data.push(parent_tree_info);
            tree.tree = OmbazarFn.tree_maker(parnet_user_id, tree_data);

        }
        else{
            tree.tree = tree_data;
        }
        let tree_list = [];
        tree_list.push(tree);
        event(tree_list,response);
    })
};
OmbazarFn.tree = function (element,options={}) {
    let defaults = {
        'add_option': false,
        'edit_option': false,
        'delete_option': false,
        'confirm_before_delete': false,
        'animate_option': false,
        'fullwidth_option': false,
        'align_option': 'center',
        'drag_disabled': false,
    };
    $.extend(defaults,options);
    return $(element).tree_structure(defaults);
};
OmbazarFn.add_to_cart = function ($rootScope,$scope, store_item_id, amount = 1,replace = false) {
    let item = Cookies.getJSON(store_item_id);
        amount = Number(amount);
    if (item){
        let new_amount = 0;
        if (replace){
            new_amount = amount;
        }
        else{
            new_amount = amount+item.quantity;
        }

        Cookies.set(store_item_id,{
            "quantity": new_amount
        });
        let index = OmbazarFn.index_number($scope.cart,"store_item_id",store_item_id);
        $scope.cart[index].quantity = new_amount;
        let before_info = $scope.cart[index];
        let point = OmbazarFn.point_maker($rootScope,before_info);
        $scope.cart[index].point = point;
    }
    else{
        let item_info = OmbazarFn.custom_filter($scope.products,"store_item_id",store_item_id);
            // item_info.quantity = amount;
        Cookies.set(store_item_id,{
            "quantity": amount
        });
        let new_object = {
            "price": item_info.price,
            "sale": item_info.sale,
            "quantity": amount,
            "purchase": item_info.purchase,
            "item_id": item_info.item_id,
            "store_id": item_info.store_id,
            "store_item_id": item_info.store_item_id,
            "vat": item_info.vat,
            "vendor_id": item_info.vendor_id,
            "category_id": item_info.category_id,
            "description": item_info.description,
            "images": item_info.images,
            "short_description": item_info.short_description,
            "item_name": item_info.item_name
        };
        let point = OmbazarFn.point_maker($rootScope,new_object);
        new_object.point = point;
        $scope.cart.push(new_object);
    }
    $scope.calculate();

    // console.log(index);

};
OmbazarFn.user_tooltip = function (input) {
    let html = "<div class='uk-text-left'>";
        html += "<span class='uk-display-block'><b>Point balance :</b> "+input.point_balance+"</span>";
        html += "<span class='uk-display-block'><b>User id :</b> "+input.user_id+"</span>";
        html += "<span class='uk-display-block'><b>Level :</b> "+input.level+"</span>";
        html += "<span class='uk-display-block'><b>Position :</b> "+input.position+"</span>";
        html += "<span class='uk-display-block'><b>Mobile :</b> "+input.user_info.calling_code+input.user_info.mobile+"</span>";
        html += "<span class='uk-display-block'><b>Status :</b> "+input.status+"</span>";
        html += "<span class='uk-display-block'><b>Expire :</b> "+input.expire+"</span>";
        html += "</div>";
        return html;
};

OmbazarFn.get_my_coupons = function ($http, $scope, event) {
    let my_coupons = $scope.my_coupon_list;
    if (my_coupons === undefined || !my_coupons.length){
        let function_name = "my_coupons(request,10,0)";
        let extra_data = OmbazarFn.retrive_request_object(function_name);
        let form_data = OmbazarFn.form_data_maker("",extra_data);
        OmbazarFn.$http($http,form_data,function (response,status) {
            console.log(response);
            $scope.my_coupon_list = response.find_data;
            event();
        });
    }else{
        event();
    }

};
OmbazarFn.get_brand_slides = function ($http,$scope,event) {
    if ($scope.brand_slides === undefined){
        OmbazarFn.retrieve_data($http,"active_brands",{},function (data) {
            console.log(data);
            $scope.brand_slides = data.find_data;
            event();
        },{},true)
    }
    else{
        event();
    }

};