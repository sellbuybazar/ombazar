$(function() {
    // init invoices
    altair_invoices.init();
});

// variables
var $invoice_card = $('#invoice'),
    $invoice_preview = $('#invoice_preview'),
    $invoice_form = $('#invoice_form'),
    $invoices_list_main = $('#invoices_list'),
    invoice_list_class = '.invoices_list', // main/sidebar list
    $invoice_add_btn = $('#invoice_add'),
    $invoice_edit_btn = $('#reciept_edit');;

altair_invoices  = {
    init: function() {
        // copy list to sidebar
        altair_invoices.copy_list_sidebar();
        // add new invoice
        altair_invoices.add_new();
        // open invoice
        altair_invoices.open_invoice();
        altair_invoices.edit_reciept();
        // print invoice btn
        altair_invoices.print_invoice();
    },
    add_new: function() {
        if($invoice_add_btn) {

            var insert_form = function() {

                var $invoice_form_template = $('#invoice_form_template'),
                    card_height = $invoice_card.height(),
                    content_height = $invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $invoice_form_template.html();
                    template_compiled = Handlebars.compile(invoice_form_template_content);
                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_document_sl.php',
                        data: { type: "reciept" },
                        success: function(data,stat) {
                            var context = {
                                "doc_sl": data
                            };
                            $.ajax({
                                async:false,
                                type: 'post',
                                url: path+'ajax/get_default_banner.php',
                                data: { type: "reciept" },
                                dataType: 'json',
                                success: function(banner_data,stat) {
                                    context.default_image=banner_data.image;
                                    context.note=banner_data.note;
                                }

                            })
                            compiled_html=template_compiled(context);
                        }

                    })

                // remove "uk-active" class form invoices list
                $(invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $invoice_card.height(card_height);

                $invoice_form
                    .hide()
                    // add form to card
                    .html(compiled_html)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_service = function() {

                    var $invoice_form_template_services = $('#invoice_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_invoice_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id
                        },
                        theCompiledHtml = template_compiled(context);

                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                };

                // append first service to invoice form on init
                append_service();

                $('#invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    append_service();
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();

            };

            // show invoice form on animation complete
            var show_form = function() {
                $invoice_card.css({
                    'height': ''
                });
                $invoice_form.show();
                $invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $invoice_add_btn.on('click', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($invoice_card,insert_form,show_form,undefined);
                altair_forms.select_elements();
                altair_form_file_dropify.init();

                $categories=$(".income_expense_category").selectize({
                    valueField: 'user_id',
                    labelField: 'category',
                    onDropdownOpen: function($dropdown) {
                        $dropdown
                            .hide()
                            .velocity('slideDown', {
                            begin: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top':'0'})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                    },
                    onDropdownClose: function($dropdown) {
                        $dropdown
                            .show()
                            .velocity('slideUp', {
                            complete: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top': ''})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });
                    }
                });

                //income expense category changes
                $(".income-expense-category input").iCheck({
                    checkboxClass: "icheckbox_md",
                    radioClass: "iradio_md",
                    increaseArea: "20%"
                }).on("ifChecked", function (event) {
                    var type=event.target.value;

                    var control=$categories[0].selectize;
                    control.clearOptions();
                    $.ajax({
                        type: 'post',
                        url: path+'ajax/get_income_expense_category.php',
                        data: { type: type },
                        dataType: 'json',
                        success: function(data,stat) {
                            for(item in data){
                                control.addOption(data[item]);

                            }
                        }
                    })

                });
                altair_form_validation.init();






            })


        }
    },
    edit_reciept: function() {
        if($invoice_edit_btn) {

            var insert_form = function() {

                var $invoice_form_template = $('#edit_invoice_form_template'),
                    card_height = $invoice_card.height(),
                    content_height = $invoice_card.find('.md-card-content').innerHeight(),
                    invoice_form_template_content = $invoice_form_template.html();
                    template_compiled = Handlebars.compile(invoice_form_template_content);
                    var invoice_id = parseInt($("#reciept_edit").attr('data-invoice-id'));

                    $.ajax({
                        async:false,
                        type: 'post',
                        url: path+'ajax/get_reciept.php',
                        data: { invoice_id: invoice_id },
                        dataType: 'json',
                        success: function(response,stat) {

                            reciept_data=response;
                            services=response.invoice.invoice_services;
                            theCompiledHtml = template_compiled(response);
                        }
                    })
                // remove "uk-active" class form invoices list
                $(invoice_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $invoice_card.height(card_height);

                $invoice_form
                    .hide()
                    // add form to card
                    .html(theCompiledHtml)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // append services to invoice form
                var append_int_service = function() {

                    var $invoice_form_template_services = $('#edit_invoice_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_edit_invoice_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id,services
                        },
                        theCompiledHtml = template_compiled(context);

                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();

                };

                // append first service to invoice form on init
                append_int_service();

                // append services to invoice form
                var append_service = function() {

                    var $invoice_form_template_services = $('#invoice_blank_form_template_services'),
                        $invoice_services = $invoice_form.find('#form_edit_invoice_services');

                    var template = $invoice_form_template_services.html(),
                        template_compiled = Handlebars.compile(template);

                    var service_id = (!$invoice_services.children().length) ? 1 : parseInt($invoice_services.children('.uk-grid:last').attr('data-service-number')) + 1,
                        context = {
                            "invoice_service_id": service_id
                        },
                        theCompiledHtml = template_compiled(context);

                    $invoice_services.append(theCompiledHtml);

                    // invoice md inputs
                    altair_md.inputs();
                    // invoice textarea autosize
                    altair_forms.textarea_autosize();

                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    return service_id;

                };




                $('#invoice_form_append_service_btn').on('click', function (e) {
                    e.preventDefault();
                    // append service to invoice form
                    var service_number=append_service();
                    $(".service-item[data-service-number='"+service_number+"']").find("input").val("");
                });

                // invoice due-date radio boxes
                altair_md.checkbox_radio();

            };

            // show invoice form on animation complete
            var show_form = function() {
                $invoice_card.css({
                    'height': ''
                });
                $invoice_form.show();
                $invoice_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $("#invoice_preview").on('click','#reciept_edit', function (e) {
                e.preventDefault();
                altair_md.card_show_hide($invoice_card,insert_form,show_form,undefined);
                altair_forms.select_elements();
                altair_form_file_dropify.init();

                $team_member=$(".team_member").selectize({
                    valueField: 'user_id',
                    labelField: 'team_member',
                    onDropdownOpen: function($dropdown) {
                        $dropdown
                            .hide()
                            .velocity('slideDown', {
                            begin: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top':'0'})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                    },
                    onDropdownClose: function($dropdown) {
                        $dropdown
                            .show()
                            .velocity('slideUp', {
                            complete: function() {
                                if (typeof thisPosBottom !== 'undefined') {
                                    $dropdown.css({'margin-top': ''})
                                }
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });
                    }
                });


//                var team_member = $team_member[0].selectize;
//                team_member.setValue(reciept_data.invoice.team_member);

                altair_form_validation.init();







            })

        }
    },

    open_invoice: function() {

        var show_invoice = function(element) {
            var $this = element,
                $invoice_template = $('#invoice_template');

            var template = $invoice_template.html(),
                template_compiled = Handlebars.compile(template);




            var invoice_id = parseInt($this.attr('data-invoice-id'));

            $.ajax({
                type: 'post',
                url: path+'ajax/get_reciept.php',
                data: { invoice_id: invoice_id },
                dataType: 'json',
                success: function(response,stat) {
//                    console.log(response);
                    var theCompiledHtml = template_compiled(response);
                    $invoice_preview.html(theCompiledHtml);
                    $invoice_form.html('');
                    $window.resize();
                    setTimeout(function() {
                        // reinitialize uikit margin
                        altair_uikit.reinitialize_grid_margin();
                    },290);
                    altair_forms.select_elements();
                }
            })


        };

        $(invoice_list_class)
            .on('click','a[data-invoice-id]', function(e) {
                e.preventDefault();
                e.stopPropagation();
                // toggle card and show invoice
                altair_md.card_show_hide($invoice_card,undefined,show_invoice,$(this));
                // set active class
                $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');
            });

        if($(invoice_list_class).find('a[data-invoice-id]').length) {
            // open first invoice
            $(invoice_list_class).find('a[data-invoice-id]').eq(0).click();
        } else {
            // open form
            $invoice_add_btn.trigger('click');
        }

    },

    print_invoice: function() {
        $body.on('click','#invoice_print',function(e) {
            e.preventDefault();
            UIkit.modal.confirm('Do you want to print this?', function () {
                // hide sidebar
                altair_main_sidebar.hide_sidebar();
                // wait for dialog to fully hide
                setTimeout(function () {
                    window.print();
                }, 300)
            }, {
                labels: {
                    'Ok': 'print'
                }
            });
        })
    },
    copy_list_sidebar: function() {
        // hide secondary sidebar toggle btn for large screens
        $sidebar_secondary_toggle.addClass('uk-hidden-large');

        var invoices_list_sidebar = $invoices_list_main.clone();

        invoices_list_sidebar.attr('id','invoices_list_sidebar');

        $sidebar_secondary
            .find('.sidebar_secondary_wrapper').html(invoices_list_sidebar)
            .end();

    }
};
