    <div id="page_content">
        <div id="page_content_inner">
            <form method="post" class="category-add" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Add new category</h4>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Category name</label>
                                            <input class="md-input" type="text" name="category_name" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row uk-margin-top">
                                            <label>Description</label>
                                            <textarea class="md-input" name="description" ></textarea>
                                        </div>
                                        <div class="uk-width-large-1-1">
                                            <div class="uk-grid form_section" id="d_form_row" data-uk-grid-margin>
                                                <div class="uk-width-medium-1-2">
                                                        <label>Specification name</label>
                                                        <input type="text" class="md-input" name="specification_name[]">

                                                </div>
                                                <div class="uk-width-medium-1-2">
                                                    <div class="uk-input-group">
                                                        <input class="tags md-input" id="tags" type="text" name="specification_tags[]"  placeholder="Separated by coma ">
                                                        <span class="uk-input-group-addon">
                                                            <a href="#" class="spbtnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <div class="uk-grid" uk-grid-margin>
                                        <label for="">Category image</label>
                                        <div class="uk-width-large-1-1 ">
                                            <input type="file" id="input-file-a" class="dropify " name="image" >
                                        </div>
                                    </div>
                                    <div class="access_area uk-margin-bottom uk-margin-top">
                                        <label for="">Access</label>
                                        <?php include_once("Src/inc/category-tree.php");?>
                                    </div>

                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save & New</button>
                                    <button class="md-btn md-btn-success" button-name="save">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
