
<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">

    <title>Ombazar</title>
    <!-- uikit -->
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>assets/css/main.min.css" media="all">
    <style>
        .hidden{
            display: none
        }
    </style>




</head>

<body class=" sidebar_main_open sidebar_main_swipe ">
    <section class="stickyHeaderBag animated hidden">
        <div class="itemCount">
            <p>
                <span class="bag"><i class="material-icons">shopping_basket</i></span>
                <span class="item-count"></span> ITEMS</p>
        </div>
        <div class="total">
            <div class="odometer">0</div>
            <span><?php echo $retrive->currency_symbol();?> </span>
        </div>
    </section>
    <div class="uk-width-medium-1-3 ">
        <div class="pos-selected-items-part hidden cart cart-hidden" >
            <div class="md-card">
                <div class="cart-close-tools">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-7-10">
                            <h4 class="total-item-heading"><i class="material-icons">shopping_basket</i> <span class="item-count"></span> Items</h4>
                        </div>

                        <div class="uk-width-medium-2-10 uk-text-center">
                            <button  class="search-pos-item md-fab" >
                                <i class="material-icons">close</i>
                            </button>
                        </div>
                    </div>
                    <hr class="uk-margin-bottom-remove uk-margin-top-remove"/>
                </div>
                <div class="  pos-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <table class="uk-table uk-margin-bottom-remove">
                                <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Price</th>
                                    <th>Qnt</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>

                            <!-- hidden content for cart element manage -->
                            <script class="cart-item-template" type="text/x-handlebars-template">
                                {{#each rows}}
                                {{#if item_id}}
                                <tr class="cart-item" data-id="{{id}}" >
                                    <td class=" ">
                                        <img class="img_thumb" src="{{image_path}}" alt="">
                                        <div class="inline-block"  data-uk-tooltip="{pos:'top'}" title="{{item_name}}Point : {{point}}">
                                         {{item_name}}
                                            {{#if sale}}
                                            <span class="block"><del class="">{{price}}</del> <?php echo $retrive->currency_symbol();?></span>
                                            <span class="">{{sale}}</span> <?php echo $retrive->currency_symbol();?>
                                            {{else}}
                                            <span class="">{{price}}</span> <?php echo $retrive->currency_symbol();?>
                                            {{/if}}
                                        </div>
                                    </td>
                                    <td class="" >
                                        {{#if sale}}
                                            <span class="uk-block"><del class=" del-cart-line-total">{{equation price '*' quantity}}</del> <?php echo $retrive->currency_symbol();?></span>
                                            <span class="cart-line-total">{{equation sale '*' quantity}}</span> <?php echo $retrive->currency_symbol();?>
                                        {{else}}
                                        <span class="cart-line-total">{{equation price '*' quantity}}</span> <?php echo $retrive->currency_symbol();?>
                                        {{/if}}
                                    </td>
                                    <td>
                                        <div class="quantity-manager">
                                            <input type="number" min="0" class="quantity hover" value="{{quantity}}">
                                        </div>
                                    </td>
                                    <td class=" cart-line-total-field"><a href="#" class="cart-delete-button"><i class="material-icons " style="color:md-color-cyan-700">close</i></a>
                                    </td>
                                </tr>
                                {{/if}}
                                {{/each}}
                            </script>

                            <div class="uk-overflow-container">
                                <div class="cart-message"></div>
                                <table class="uk-table cart-table">
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="calculation uk-margin-top">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-10-10 uk-text-right">
                                <div class="sub-total">
                                    Sub total= <span class="cart-sub-total"></span> <?php echo $retrive->currency_symbol();?>
                                </div>
                                <div class="vat-total">
                                    Vat= <span></span> <?php echo $retrive->currency_symbol();?>
                                </div>
                                <div class="discount">
                                    Discount= <span class="cart-discount"></span> <?php echo $retrive->currency_symbol();?>
                                </div>
                            </div>
                        </div>


                    </div>
                    <hr class="hr">
                    <div class="procceed-buttons uk-text-center uk-margin-top uk-padding-bottom">
                        <button  class="md-btn md-btn-flat md-btn-wave waves-effect waves-button order-button">Order</button>
                        <button class="md-btn md-btn-success uk-margin-small-top "> <span class="total-amount"></span> <?php echo $retrive->currency_symbol();?></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php
        if(isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $id_type_name=$common->id_type_name($user_id);
            $logger_info=$retrive->controller_info($user_id);
            $referel_id_info=$retrive->referrel_info($user_id);
        }
    ?>
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>

                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <?php if(isset($_SESSION['user_id'])){?>
                        <li data-uk-tooltip="{pos:'top'}" title="Orders"> <a href="<?php echo SITE_URL;?>orders" class="no_load"><i class="material-icons md-24 md-light">child_friendly</i></a></li>
                        <?php } ?>
                        <?php if(isset($_SESSION['user_id'])){?>
                        <li data-uk-tooltip="{pos:'top'}" title="Your Friends"> <a href="<?php echo SITE_URL;?>network" class="no_load"><i class="material-icons md-24 md-light">device_hub</i></a></li>
                        <?php } ?>

                        <li data-uk-tooltip="{pos:'top'}" title="Coupon unlock"> <a href="<?php echo SITE_URL;?>coupon-unlocker" class="no_load"><i class="material-icons md-24 md-light">card_giftcard</i></a></li>
                        <li data-uk-tooltip="{pos:'top'}" title="Corporates"> <a href="<?php echo SITE_URL;?>corporates" class="no_load"><i class="material-icons md-24 md-light">business</i></a></li>
                        <li data-uk-tooltip="{pos:'top'}" title="Fullscreen"><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>



<!--                        <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li>-->
<!---->
<!--                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">-->
<!--                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>-->
<!--                            <div class="uk-dropdown uk-dropdown-xlarge">-->
<!--                                <div class="md-card-content">-->
<!--                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">-->
<!--                                        <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>-->
<!--                                        <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>-->
<!--                                    </ul>-->
<!--                                    <ul id="header_alerts" class="uk-switcher uk-margin">-->
<!--                                        <li>-->
<!--                                            <ul class="md-list md-list-addon">-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <span class="md-user-letters md-bg-cyan">we</span>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Aliquam ea.</a></span>-->
<!--                                                        <span class="uk-text-small uk-text-muted">Est autem ratione et voluptas vel aut amet inventore.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Veniam earum harum.</a></span>-->
<!--                                                        <span class="uk-text-small uk-text-muted">Enim sapiente sint id quod praesentium est.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <span class="md-user-letters md-bg-light-green">mb</span>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Iste maiores incidunt.</a></span>-->
<!--                                                        <span class="uk-text-small uk-text-muted">Sed quidem quas quaerat magnam animi officia sit doloribus delectus.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Qui eveniet.</a></span>-->
<!--                                                        <span class="uk-text-small uk-text-muted">Beatae quia distinctio et error animi quam nisi esse esse.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading"><a href="pages_mailbox.html">Nam laborum autem.</a></span>-->
<!--                                                        <span class="uk-text-small uk-text-muted">Veniam blanditiis eligendi consequuntur non laudantium voluptatem error soluta fugit impedit illo.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                            </ul>-->
<!--                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">-->
<!--                                                <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>-->
<!--                                            </div>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <ul class="md-list md-list-addon">-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading">Voluptate magni et.</span>-->
<!--                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Aut possimus debitis exercitationem sed ducimus.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading">Est et.</span>-->
<!--                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Aspernatur et eligendi accusantium et sit.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading">Cumque occaecati provident.</span>-->
<!--                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Perferendis sint enim aut quod occaecati sequi non.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                <li>-->
<!--                                                    <div class="md-list-addon-element">-->
<!--                                                        <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>-->
<!--                                                    </div>-->
<!--                                                    <div class="md-list-content">-->
<!--                                                        <span class="md-list-heading">Error sed.</span>-->
<!--                                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Soluta odio voluptatem quidem tenetur repellendus commodi quaerat voluptas.</span>-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                            </ul>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </li>-->
                        <?php
                            if(isset($_SESSION['user_id'])){
                        ?>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="<?php echo SITE_URL;?>assets/img/images/<?php echo $logger_info[0]['image'];?>" alt=""/></a>

                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a class="no_load" href="<?php echo SITE_URL;?>profile/<?php echo $referel_id_info;?>"><i class="material-icons ">assignment_ind</i> Profile</a></li>
                                    <li><a href="<?php echo SITE_URL;?>views/logout"><i class="material-icons ">exit_to_app</i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <?php } else{?>
                                <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                    <a href="#" class="user_action_image"><i class="material-icons md-24 md-light">lock_open</i></a>

                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav js-uk-prevent">
                                            <li><a class="no_load" href="<?php echo SITE_URL;?>login"><i class="material-icons md-24 ">keyboard_tab</i> Login</a></li>
                                            <li><a class="no_load" href="<?php echo SITE_URL;?>registration"><i class="material-icons md-24 ">add</i> Registration</a></li>
                                        </ul>
                                    </div>
                                </li>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
                <script type="text/autocomplete">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">
                        {{~items}}
                        <li data-value="{{ $item.value }}">
                            <a href="{{ $item.url }}">
                                {{ $item.value }}<br>
                                <span class="uk-text-muted uk-text-small">{{{ $item.text }}}</span>
                            </a>
                        </li>
                        {{/items}}
                    </ul>
                </script>
            </form>
        </div>

    </header><!-- main header end -->


    <!-- main sidebar -->
    <aside id="sidebar_main">

        <div class="sidebar_main_header">
            <div class="sidebar_logo">

                <a href="<?php echo SITE_URL;?>home" class="sSidebar_hide sidebar_logo_large no_load">
                    <img class="logo_regular" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                    <img class="logo_light" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                </a>
                <a href="<?php echo SITE_URL;?>home" class="sSidebar_show sidebar_logo_small no_load">
                    <img class="logo_regular" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                    <img class="logo_light" src="<?php echo SITE_URL;?>assets/img/default/om-bazar-logo.png" alt="" height="" width=""/>
                </a>
            </div>

            <div class="sidebar_actions">

            </div>
        </div>

        <div class="menu_section">
<!--            <pre>-->
            <?php echo $retrive->main_menu_of_categories()?>
        </div>
    </aside><!-- main sidebar end -->

