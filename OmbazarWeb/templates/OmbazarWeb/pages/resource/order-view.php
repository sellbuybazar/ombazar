<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content">
                <?php
                    $order_id=$_REQUEST['order'];
                    $data=$retrive->order_info($order_id,false);
                    foreach($data as $result){
                        $time=$common->simple_date($result['time'],"D M Y H:i A")
                ?>
                <h3 class="inline"><?php echo $result['order_id']?></h3> <span class="bold"> Total : <?php echo $result['grand_total']?></span> /  <?php echo $result['customer_name']?> / <?php echo $time;?>
                <hr/>
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-7-10">
                        <table class="uk-table">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Vat</th>
                                    <th>Total</th>
                                </tr>

                            </thead>
                            <tbody>
                            <?php
                                $items=$result['orders'];
                                foreach($items as $item){
                            ?>
                                <tr>
                                    <td>
                                        <div class="inline-block view-order-item">
                                            <img src="<?php echo SITE_URL;?>assets/img/images/<?php echo $item['image']?>">
                                        </div>
                                        <div class="inline-block">
                                            <span><?php echo $item['name'];?></span><br>
                                            <span>Code: <?php echo $item['item_code']?></span><br>
<!--                                            <span>Point: 1500</span>-->
                                        </div>

                                    </td>
                                    <td><?php echo $item['price']?></td>
                                    <td><?php echo $item['quantity']?></td>
                                    <td><?php echo $item['vat']?>%</td>
                                    <td><?php echo $item['sub_total']?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div>
                            <h3 class="uk-text-right">Total</h3>
                            <table class="uk-table">
                                <tr>
                                    <td class="uk-text-right">Sub total:</td>
                                    <td class="uk-text-right"><?php echo $result['sub_total']?></td>
                                </tr>
                                <tr>
                                    <td class="uk-text-right">Vat:</td>
                                    <td class="uk-text-right"><?php echo $result['vat_total']?></td>
                                </tr>
                                <?php if($result['type']=="pos"){?>
                                <tr>
                                    <td class="uk-text-right">Discount:</td>
                                    <td class="uk-text-right"><?php echo $result['discount']?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td class="uk-text-right">Grand total:</td>
                                    <td class="uk-text-right"><?php echo $result['grand_total']?></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <div class="uk-width-medium-3-10">
                        <div class="uk-row-first">
                            <div class="md-card">
                                <div class="md-card-content uk-grid" data-uk-grid-margin>
                                    <div  class="uk-width-medium-4-10">
                                        <p class="uk-margin-top">Order status</p>
                                    </div>

                                    <div class="uk-width-medium-6-10">
                                        <select class="order-status" disabled selected-id="<?php echo $result['order_status']?>" <?php if($result['order_status']==1){echo "disabled";}?>>
                                            <option value="0">Pending</option>
                                            <option value="1">Done</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="md-card md-card-primary">
                                <div class="md-card-content">
                                    <h3>User Information</h3>
                                    <?php

                                        foreach($result['user_info'] as $user_info){
                                    ?>
                                    <p>
                                        <span>Name: </span> <?php echo $user_info['name'] ?><br>
                                        <span>Mobile: </span> <?php echo $user_info['mobile'] ?><br>
                                        <span>Email: </span> <?php echo $user_info['email'] ?><br>
<!--                                        <span>Country: </span> --><?php //echo $user_info['country'] ?>
                                    </p>
                                    <?php } ?>
                                </div>

                            </div>
                            <div class="md-card md-card-success">
                                <div class="md-card-content">
                                    <h3>Store Information</h3>
                                    <ol class="order-stores">
                                    <?php

                                        foreach($result['stores_info'] as $store_info){
                                    ?>

                                        <li><?php echo $store_info['store_name'] ?></li>

                                    <?php } ?>
                                    </ol>
                                </div>

                            </div>


                        </div>

                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
