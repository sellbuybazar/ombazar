
    <div id="page_content">
        <div id="page_content_inner">
            <?php
                $item_id=$_REQUEST['item'];
                $array=array();
                $info=$retrive->store_item_info($item_id);
                $array[]=$info;
                foreach($array as $result){
            ?>
            <form method="post" class="my-item-edit" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Edit Information of ( <?php echo $result['item_name']?> )</h4>
                            <input type="hidden" name="item_id" value="<?php echo $item_id;?>">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Item code</label>
                                            <input class="md-input item-code-field" type="text" <?php if($result['item_code']==""){?> name="item_code"<?php } else{?> disabled <?php } ?> value="<?php echo $result['item_code']?>"  >
                                        </div>
                                        <?php if($result['item_code']!==""){?>
                                            <input type="hidden" value="<?php echo $result['item_code'];?>" name="item_code">
                                        <?php } ?>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Purchase</label>
                                            <input class="md-input item-purchase-field" type="text" name="purchase" value="<?php echo $result['purchase']?>" required >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Price</label>
                                            <input class="md-input item-price-field" type="text" name="price" value="<?php echo $result['price']?>" required>
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Sale</label>
                                            <input class="md-input item-sale-field" type="text" name="sale" value="<?php echo $result['sale']?>" >
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Vat</label>
                                            <input class="md-input item-vat-field" type="text" name="vat" value="<?php echo $result['vat']?>" >
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </form>
            <?php } ?>
        </div>
    </div>
