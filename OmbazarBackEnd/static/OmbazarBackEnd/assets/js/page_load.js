function get_page_data(){
    var url_info=location.pathname;
    var q_search=location.search;
    var get_page=url_info.split("/");
    var this_page_name=get_page[1];
    if(this_page_name===undefined){
        this_page_name="home";
    }
    else if(this_page_name===""){
        this_page_name="home";
    }
    loading_on();
    $.ajax({
        url:path+"ajax/page-data-loader.php"+q_search,
        data:{url_info:url_info},
        type:"POST",
        success:function(data,status){
            loading_off();
             $("body").click();
            $("#page_content:not('.footer')").remove();
            $("#sidebar_main").after(data);
            script_loader(this_page_name);
//                setTimeout(function () {
//                    page_requirement(this_page_name);
//                },1000)
            page_requirement(this_page_name);


        }
    });

}
function new_page_load(url,title,obj={}){
    var href=url;
    var push_object=obj;
    history.pushState(push_object,title,href);
    get_page_data();
}


$("body").on("click","a.no_load",function (e) {
    e.preventDefault();
    var href=$(this).attr("href");
    var title=$(this).text();
    var push_object={};
    history.pushState(push_object,title,href);
    get_page_data();

});
window.addEventListener('popstate', e => {
    get_page_data();
});