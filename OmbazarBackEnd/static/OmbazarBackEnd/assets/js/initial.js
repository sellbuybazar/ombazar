var path_name=location.pathname;
var divide_path_name=path_name.split("/");
var page_name=divide_path_name[1];

var script_requirement_array={
    recaptcha:["login","registration"],
    components_preloaders:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    functions:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    uikit_custom:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    altair_admin_common:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    jquery_ui:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","network"],
    jquery_fancytree:["home","corporates","corporate","prize-details","coupon-unlocker","products"],
    plugins_tree:["home","corporates","corporate","prize-details","coupon-unlocker","products"],
    handlebars:["home","corporates","corporate","prize-details","coupon-unlocker","products","product-details","login","registration","orders","order-view","profile","edit-profile","network"],
    handlebars_helpers:["home","corporates","corporate","prize-details","coupon-unlocker","products","product-details","login","registration","orders","order-view","profile","edit-profile","network"],
    parsley_validatoin_config:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","edit-profile"],
    parsley:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","edit-profile"],
    forms_validation:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","edit-profile"],
    page_user_edit:["home","corporates","corporate","prize-details","coupon-unlocker","registration","edit-profile"],
    jquery_tree:["network"],
    jquery_fullscreen:["home","corporates","corporate","prize-details","coupon-unlocker","network"],
    // odometer:["home","corporates","corporate","prize-details","coupon-unlocker","products"],
    push:["home","corporates","corporate","prize-details","coupon-unlocker"],
    printThis:["home","corporates","corporate","prize-details","coupon-unlocker","products","orders","order-view"],
    custom:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    high_density:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    style_switcher:["home","corporates","corporate","prize-details","coupon-unlocker","products","product-details","orders","order-view","profile","edit-profile","network"],

}


// console.log(script_requirement_array);

var script_information_array={
    parsley_validatoin_config:{
        src:path+"assets/js/parsley-validation-config.js",
        id:"parsley_validatoin_config-js",
        type:"text-javascript"
    },
    components_preloaders:{
        src:path+"assets/js/pages/components_preloaders.js",
        id:"parsley_validatoin_config-js",
        type:"text-javascript"
    },
    
    high_density:{
        src:path+"assets/js/high-density.js",
        id:"high_density-js",
        type:"text-javascript"
    },

    style_switcher:{
        src:path+"assets/js/style-switcher.js",
        id:"style_switcher-js",
        type:"text-javascript"
    },

    printThis:{
        src:path+"assets/js/plugins/jQuery-Plugin-To-Print-Specified-Elements-Of-Webpage-printThis/lib/jquery.printThis.js",
        id:"printThis-js",
        type:"text-javascript"
    },

    push:{
        src:path+"bower_components/push.js/push.min.js",
        id:"push-js",
        type:"text-javascript"
    },

    odometer:{
        src:path+"assets/js/plugins/odometer-master/odometer.js",
        id:"odometer-js",
        type:"text-javascript"
    },

    jquery_fullscreen:{
        src:path+"assets/js/plugins/Easy-Elements-Fullscreen-Plugin-with-jQuery/release/jquery.fullscreen.js",
        id:"jquery_fullscreen-js",
        type:"text-javascript"
    },

    jquery_tree:{
        src:path+"assets/js/plugins/h-tree/js/jquery.tree.js",
        id:"jquery_tree-js",
        type:"text-javascript"
    },

    page_user_edit:{
        src:path+"assets/js/pages/page_user_edit.min.js",
        id:"page_user_edit-js",
        type:"text-javascript"
    },

    forms_validation:{
        src:path+"assets/js/pages/forms_validation.js",
        id:"forms_validation-js",
        type:"text-javascript"
    },

    parsley:{
        src:path+"bower_components/parsleyjs/dist/parsley.min.js",
        id:"parsley-js",
        type:"text-javascript"
    },

    handlebars:{
        src:path+"bower_components/handlebars/handlebars.min.js",
        id:"handlebars-js",
        type:"text-javascript"
    },

    handlebars_helpers:{
        src:path+"assets/js/custom/handlebars_helpers.min.js",
        id:"handlebars_helpers-js",
        type:"text-javascript"
    },

    plugins_tree:{
        src:path+"assets/js/pages/plugins_tree.min.js",
        id:"plugins_tree-js",
        type:"text-javascript"
    },

    jquery_fancytree:{
        src:path+"bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js",
        id:"jquery_fancytree-js",
        type:"text-javascript"
    },

    jquery_ui:{
        src:path+"bower_components/jquery-ui/jquery-ui.min.js",
        id:"jquery_ui-js",
        type:"text-javascript"
    },

    altair_admin_common:{
        src:path+"assets/js/altair_admin_common.min.js",
        id:"altair_admin_common-js",
        type:"text-javascript"
    },

    recaptcha:{
        src:"https://www.google.com/recaptcha/api.js",
        id:"recaptcha-js",
        type:"text-javascript"
    },

    uikit_custom:{
        src:path+"assets/js/uikit_custom.js",
        id:"uikit_custom-js",
        type:"text-javascript"
    },
    functions:{
        src:path+"assets/js/functions.js",
        id:"functions-js",
        type:"text-javascript"
    },
    custom:{
        src:path+"userEnd/assets/js/custom.js",
        id:"custom-js",
        type:"text-javascript"
    },

}


// console.log(script_requirement_array);


var css_requirement_array={
    fancytree_ui:["home","corporates","corporate","prize-details","coupon-unlocker","products"],
    style_switcher:["home","corporates","corporate","prize-details","coupon-unlocker","products","product-details","product-details","orders","order-view","profile","edit-profile","network"],
    error_page:["home","corporates","corporate","prize-details","coupon-unlocker",],
    themes_combined:["home","corporates","corporate","prize-details","coupon-unlocker","products","product-details","orders","order-view","profile","edit-profile","network"],
    login_page:["login"],
    odometer:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    animate:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],
    h_tree:["network"],
    custom:["home","corporates","corporate","prize-details","coupon-unlocker","login","registration","products","product-details","orders","order-view","profile","edit-profile","network"],

}

var css_information_array={
    login_page:{
        href:path+"assets/css/login_page.min.css",
        id:"login_page-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    animate:{
        href:path+"assets/css/animate.css",
        id:"animate-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },

    themes_combined:{
        href:path+"assets/css/themes/themes_combined.min.css",
        id:"themes_combined-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    fancytree_ui:{
        href:path+"assets/skins/jquery.fancytree/ui.fancytree.min.css",
        id:"fancytree-ui-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    custom:{
        href:path+"assets/css/custom.css",
        id:"custom-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    style_switcher:{
        href:path+"assets/css/style_switcher.min.css",
        id:"style_switcher-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    error_page:{
        href:path+"assets/css/error_page.min.css",
        id:"error_page-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    odometer:{
        href:path+"assets/js/plugins/odometer-master/themes/odometer-theme-default.css",
        id:"odometer-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },
    h_tree:{
        href:path+"assets/js/plugins/h-tree/style.css",
        id:"h_tree-css",
        type:"text-css",
        media:"all",
        rel:"stylesheet",
    },


}

if(!page_name){
    page_name="home";
}
function script_loader(new_page_name=""){
    if(new_page_name!=""){
        page_name=new_page_name;
    }
    //generate css 's
    for(x in css_requirement_array){
        var page_names=css_requirement_array[x];

        var index_of=page_names.indexOf(page_name);
        if(index_of>=0){
            var css_href=css_information_array[x].href;
            var type=css_information_array[x].type;
            var id=css_information_array[x].id;
            var media=css_information_array[x].media;
            var rel=css_information_array[x].rel;

            var check=$("#"+id).length;
            if(!check){
                var imported = document.createElement('link');
                imported.href = css_href;
                imported.id = id;
                imported.media = media;
                imported.rel = rel;
                document.head.appendChild(imported);
            }
        }
    }
    //generate js 's
    for(x in script_requirement_array){
        var page_names=script_requirement_array[x];
        var index_of=page_names.indexOf(page_name);

        if(index_of>=0){
            var script_src=script_information_array[x].src;
            var type=script_information_array[x].type;
            var id=script_information_array[x].id;

            var check=$("#"+id).length;
            if(!check){
                var imported = document.createElement('script');
                imported.src = script_src;
                imported.id = id;
                imported.async = false;
                document.body.appendChild(imported);
            }
        }

    }
    function script_load(script_requirement_array) {
        var page_names=script_requirement_array[0];
        var index_of=page_names.indexOf(page_name);
        if(index_of>=0){
            var script_src=script_information_array[x].src;
            var type=script_information_array[x].type;
            var id=script_information_array[x].id;
            var check=$("#"+id).length;

            if(!check){
                var imported = document.createElement('script');
                imported.src = script_src;
                imported.id = id;
                imported.async = false;
                document.body.appendChild(imported);
            }
        }
    }


}

// $(window).load(function () {
//     script_loader();
// })
script_loader();


