    <?php
        $store_id=$_REQUEST['store'];
        $data=$retrive->store_info($store_id);
    ?>
    <div id="page_content">
        <div id="page_content_inner">
            <?php
                foreach($data as $result){
            ?>
            <form method="post" class="store-edit" id="form">
                <div class="uk-grid " data-uk-grid-margin data-uk-grid-match >
                    <div class="uk-width-large-1-11">
                        <div class="md-card user_content">
                            <h4 class="heading_c uk-margin-bottom ">Edit store</h4>
                            <input type="hidden" name="store_id" value="<?php echo $store_id;?>">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-large-1-2 parsley-row">
                                    <div class="uk-grid "  data-uk-grid-margin>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Store name</label>
                                            <input class="md-input" type="text" name="store_name" required value="<?php echo $result['store_name'];?>">
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Address line 1</label>
                                            <input class="md-input " type="text" name="address_line_1" required value="<?php echo $result['address_line_1'];?>">
                                        </div>
                                        <div class="uk-width-large-1-1 parsley-row">
                                            <label>Address line 2</label>
                                            <input class="md-input " type="text" name="address_line_2" value="<?php echo $result['address_line_2'];?>"/>
                                        </div>
                                        <div class="uk-width-1-1  parsley-row">
                                            <label>Post code</label>
                                            <input type="text" class="md-input" name="post_code" value="<?php echo $result['post_code']?>" required >
                                        </div>
                                        <div class="uk-width-1-1  parsley-row">
                                            <select class="full-name-country-code-selector" name="country" selected-id="<?php echo $result['country']?>">
                                                <option value="">Select country</option>

                                            </select>
                                        </div>
                                        <div class="uk-width-1-1  parsley-row">
<!--                                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14610.945298850498!2d90.4078518!3d23.721108299999997!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1528955757167" width="100%" height="250px" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                                        </div>

                                    </div>
                                </div>


                                <div class="uk-width-large-1-2 uk-margin-top">
                                    <input type="file" id="input-file-a" class="dropify " name="image" data-default-file="<?php echo SITE_URL?>assets/img/images/<?php echo $result['image']?>"/>
                                    <p>
                                        <label for="">Active</label>
                                        <input type="checkbox" data-switchery <?php if($result['active']==1){echo "checked";} ?> value="1" name="active"/>
                                    </p>
                                </div>




                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-large-1-2">
                                    <button class="md-btn md-btn-primary" button-name="save_and_new">Save</button>
                                    <button type="reset" class="md-btn md-btn-info">Cancel</button>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </form>
            <?php } ?>
        </div>
    </div>
